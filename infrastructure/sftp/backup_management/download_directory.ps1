﻿param(
    [Parameter(Mandatory=$true)][String]$HostName,
    [Parameter(Mandatory=$true)][int]$PortNumber,
    [Parameter(Mandatory=$true)][String]$SshHostKeyFingerprint,
    [Parameter(Mandatory=$true)][String]$UserName,
    [Parameter(Mandatory=$true)][Security.SecureString]$Password,
    [Parameter(Mandatory=$true)][String]$RemoteDirectory,
    [Parameter(Mandatory=$true)][String]$LocalDirectory
)

$ErrorActionPreference = "Stop"

# Constants
$OriginalDir = Get-Location
$BaseDir = Resolve-Path "$PSScriptRoot"
$ExecutionDate = (Get-Date).ToUniversalTime().ToString("yyyy-MM-ddTHH-mm-ss")

Function Run {
    try
    {
        # Load WinSCP .NET assembly
        Write-Message "Loading WinSCPnet assembly" -ForegroundColor "Cyan"
        Add-Type -Path "C:\Program Files (x86)\WinSCP\WinSCPnet.dll"
        Write-Message "OK" -ForegroundColor "Green"
     
        # Setup session
        Write-Message "Setup SFTP session" -ForegroundColor "Cyan"
        $SessionOptions = New-Object WinSCP.SessionOptions -Property @{
            Protocol = [WinSCP.Protocol]::Sftp
            HostName = $HostName
            PortNumber = $PortNumber
            SshHostKeyFingerprint = $SshHostKeyFingerprint
            UserName = $UserName
            SecurePassword = $Password
        }
        $Session = New-Object WinSCP.Session
        Write-Message "OK" -ForegroundColor "Green"
     
        try
        {
            # Connect
            Write-Message "Connecting to host '$HostName'" -ForegroundColor "Cyan"
            $Session.Open($SessionOptions)
            Write-Message "OK" -ForegroundColor "Green"
     
            # Download files
            Write-Message "Downloading files" -ForegroundColor "Cyan"
            $TransferOptions = New-Object WinSCP.TransferOptions
            $TransferOptions.TransferMode = [WinSCP.TransferMode]::Binary
            $RemotePath = "$RemoteDirectory/*";
            $LocalPath = "$LocalDirectory\*";
            $TransferResult = $Session.GetFiles($RemotePath, $LocalPath, $False, $TransferOptions)
            $TransferResult.Check()
            Write-Message "Transfered $($TransferResult.Transfers.Count) files successfully" -ForegroundColor "Green"
        }
        finally
        {
            # Disconnect, clean up
            $Session.Dispose()
        }
    } finally {
        Set-Location "$OriginalDir"
    }
}

Function Write-Message {
	param(
		[Parameter(Mandatory=$true)]
		[String]$Message,
		[Parameter(Mandatory=$true)]
		[ValidateSet("Green","Red","Cyan")]
		[String]$ForegroundColor
	)
	
	$CurrentTime = Get-Date -Format "yyyy-MM-dd HH:mm:ss"
	Write-Host "[$CurrentTime] $Message" -ForegroundColor $ForegroundColor
}

Run
