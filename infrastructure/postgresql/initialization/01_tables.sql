CREATE TABLE ip_log
(
    id uuid NOT NULL,
    ip_address text NOT NULL,
    action text NOT NULL,
    context text NULL,
    timestamp timestamptz NOT NULL,
    created_at timestamptz NOT NULL DEFAULT NOW(),
    updated_at timestamptz NOT NULL DEFAULT NOW(),
    CONSTRAINT ip_log_pkey PRIMARY KEY (id)
)
WITH (
    OIDS = FALSE
);

CREATE INDEX ip_log_ip_address_idx
    ON ip_log USING btree
    (ip_address ASC NULLS LAST);
CREATE INDEX ip_log_action_idx
    ON ip_log USING btree
    (action ASC NULLS LAST);
CREATE INDEX ip_log_timestamp_idx
    ON ip_log USING btree
    (timestamp ASC NULLS LAST);


CREATE TABLE application_user
(
    id uuid NOT NULL,
    email text NOT NULL,
    is_password_clear_text boolean NOT NULL,
    password text NOT NULL,
    is_admin boolean NOT NULL,
    last_name text NOT NULL,
    first_name text NOT NULL,
    created_at timestamptz NOT NULL DEFAULT NOW(),
    updated_at timestamptz NOT NULL DEFAULT NOW(),
    CONSTRAINT application_user_pkey PRIMARY KEY (id),
    CONSTRAINT application_user_email_key UNIQUE (email)
)
WITH (
    OIDS = FALSE
);


CREATE TABLE recipe_category
(
    id uuid NOT NULL,
    name text NOT NULL,
    position integer NOT NULL,
    created_at timestamptz NOT NULL DEFAULT NOW(),
    updated_at timestamptz NOT NULL DEFAULT NOW(),
    CONSTRAINT recipe_category_pkey PRIMARY KEY (id)
)
WITH
(
    OIDS = FALSE
);


CREATE TABLE recipe
(
    id uuid NOT NULL,
    category_id uuid NOT NULL,
    name text NOT NULL,
    nb_persons integer NOT NULL,
    preparation_time interval NOT NULL,
    total_time interval NOT NULL,
    ingredients text[] NOT NULL,
    tools text[] NOT NULL,
    steps text[] NOT NULL,
    created_at timestamptz NOT NULL DEFAULT NOW(),
    updated_at timestamptz NOT NULL DEFAULT NOW(),
    CONSTRAINT recipe_pkey PRIMARY KEY (id),
    CONSTRAINT recipe_category_id_fkey FOREIGN KEY (category_id)
        REFERENCES recipe_category (id) MATCH SIMPLE
        ON UPDATE CASCADE
        ON DELETE CASCADE
)
WITH
(
    OIDS = FALSE
);

CREATE INDEX recipe_category_id_idx
    ON recipe USING btree
    (category_id ASC NULLS LAST);
