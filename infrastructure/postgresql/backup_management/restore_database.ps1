﻿param(
    [Parameter(Mandatory=$true)][String]$DbHost,
    [Parameter(Mandatory=$true)][int]$DbPort,
    [Parameter(Mandatory=$true)][String]$DbName,
    [Parameter(Mandatory=$true)][String]$DbUsername,
    [Parameter(Mandatory=$true)][String]$DumpFilePath
)

$ErrorActionPreference = "Stop"

# Constants
$OriginalDir = Get-Location
$BaseDir = Resolve-Path "$PSScriptRoot"
$ExecutionDate = (Get-Date).ToUniversalTime().ToString("yyyy-MM-ddTHH-mm-ss")
$PgPassFile = if ([String]::IsNullOrEmpty($env:PGPASSFILE)) { "$env:APPDATA\postgresql\pgpass.conf" } else { "$env:PGPASSFILE" }

Function Run {
    try {
        if (-not (Test-Path $PgPassFile -PathType Leaf)) {
            throw "File '$PgPassFile' does not exist. See https://www.postgresql.org/docs/current/libpq-pgpass.html for guidance."
        }
        
        Write-Message "Restore local dump to target SQL database" -ForegroundColor "Cyan"
        Restore-DatabaseFromLocalBackup -DbHost $DbHost -DbPort $DbPort -DbName $DbName -DbUsername $DbUsername -DumpFilePath $DumpFilePath
        Write-Message "Done" -ForegroundColor "Green"
    } finally {
        Set-Location "$OriginalDir"
    }
}

Function Restore-DatabaseFromLocalBackup {
	param(
		[Parameter(Mandatory=$true)]
		[String]$DbHost,
		[Parameter(Mandatory=$true)]
		[String]$DbPort,
		[Parameter(Mandatory=$true)]
		[String]$DbName,
		[Parameter(Mandatory=$true)]
		[String]$DbUsername,
		[Parameter(Mandatory=$true)]
		[String]$DumpFilePath
	)
    
    exec {
        dropdb --host "$DbHost" --port "$DbPort" --username "$DbUsername" --no-password --if-exists "$DbName"
    } > $null
    exec {
        createdb --host "$DbHost" --port "$DbPort" --username "$DbUsername" --no-password --encoding "UTF8" --template "template0" "$DbName"
    } > $null
    exec {
        pg_restore --host "$DbHost" --port "$DbPort" --username "$DbUsername" --no-password --dbname $DbName --exit-on-error --no-owner --no-privileges """$DumpFilePath"""
    } > $null
}

Function script:exec {
    [CmdletBinding()]
	param(
		[Parameter(Position=0,Mandatory=1)][scriptblock]$cmd,
		[Parameter(Position=1,Mandatory=0)][string]$errorMessage = ("Error executing command: {0}" -f $cmd)
	)
    
	& $cmd
	if ($lastexitcode -ne 0)
	{
		throw $errorMessage
	}
}

Function Write-Message {
	param(
		[Parameter(Mandatory=$true)]
		[String]$Message,
		[Parameter(Mandatory=$true)]
		[ValidateSet("Green","Red","Cyan")]
		[String]$ForegroundColor
	)
	
	$CurrentTime = Get-Date -Format "yyyy-MM-dd HH:mm:ss"
	Write-Host "[$CurrentTime] $Message" -ForegroundColor $ForegroundColor
}

Run
