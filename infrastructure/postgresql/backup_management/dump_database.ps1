﻿param(
    [Parameter(Mandatory=$true)][String]$DbHost,
    [Parameter(Mandatory=$true)][int]$DbPort,
    [Parameter(Mandatory=$true)][String]$DbName,
    [Parameter(Mandatory=$true)][String]$DbUsername,
    [Parameter(Mandatory=$true)][String]$DumpFileDirectory
)

$ErrorActionPreference = "Stop"

# Constants
$OriginalDir = Get-Location
$BaseDir = Resolve-Path "$PSScriptRoot"
$ExecutionDate = (Get-Date).ToUniversalTime().ToString("yyyy-MM-ddTHH-mm-ss")
$PgPassFile = if ([String]::IsNullOrEmpty($env:PGPASSFILE)) { "$env:APPDATA\postgresql\pgpass.conf" } else { "$env:PGPASSFILE" }
$DumpFilePath = Join-Path -Path "$DumpFileDirectory" -ChildPath "$ExecutionDate.sql"

Function Run {
    try {
        if (-not (Test-Path $PgPassFile -PathType Leaf)) {
            throw "File '$PgPassFile' does not exist. See https://www.postgresql.org/docs/current/libpq-pgpass.html for guidance."
        }
        
        Write-Message "Dump SQL database to local file" -ForegroundColor "Cyan"
        Dump-DatabaseToLocalBackup -DbHost $DbHost -DbPort $DbPort -DbName $DbName -DbUsername $DbUsername -DumpFilePath $DumpFilePath
        Write-Message "Done (dumped to '$DumpFilePath')" -ForegroundColor "Green"
    } finally {
        Set-Location "$OriginalDir"
    }
}

Function Dump-DatabaseToLocalBackup {
	param(
		[Parameter(Mandatory=$true)]
		[String]$DbHost,
		[Parameter(Mandatory=$true)]
		[String]$DbPort,
		[Parameter(Mandatory=$true)]
		[String]$DbName,
		[Parameter(Mandatory=$true)]
		[String]$DbUsername,
		[Parameter(Mandatory=$true)]
		[String]$DumpFilePath
	)
    
    exec {
        pg_dump --host "$DbHost" --port "$DbPort" --username "$DbUsername" --no-password --encoding "UTF8" --file """$DumpFilePath""" --format "custom" --no-owner --no-privileges "$DbName"
    } > $null
}

Function script:exec {
    [CmdletBinding()]
	param(
		[Parameter(Position=0,Mandatory=1)][scriptblock]$cmd,
		[Parameter(Position=1,Mandatory=0)][string]$errorMessage = ("Error executing command: {0}" -f $cmd)
	)
    
	& $cmd
	if ($lastexitcode -ne 0)
	{
		throw $errorMessage
	}
}

Function Write-Message {
	param(
		[Parameter(Mandatory=$true)]
		[String]$Message,
		[Parameter(Mandatory=$true)]
		[ValidateSet("Green","Red","Cyan")]
		[String]$ForegroundColor
	)
	
	$CurrentTime = Get-Date -Format "yyyy-MM-dd HH:mm:ss"
	Write-Host "[$CurrentTime] $Message" -ForegroundColor $ForegroundColor
}

Run
