#!/bin/bash

# Exit when any command fails
set -e

# Script variables
REPOSITORY_ROOT=$(dirname "$(dirname "$(realpath -s "${BASH_SOURCE[0]/..}")")")
TEMP_PUBLISH_DIRECTORY="$REPOSITORY_ROOT/deployment/publish"
APPLICATION_DIRECTORY="/var/www/madmox-website"

echo "Publish application to temporary directory..."
rm -rf $TEMP_PUBLISH_DIRECTORY/*
dotnet publish "$REPOSITORY_ROOT/dotnetapp/Madmox.Website.Web" --configuration "Release" --output "$TEMP_PUBLISH_DIRECTORY"
echo

echo "Stop Kestrel service..."
service kestrel-madmox-website stop
echo

echo "Copy deployment package to application directory..."
rm -rf $APPLICATION_DIRECTORY/*
mv $TEMP_PUBLISH_DIRECTORY/* $APPLICATION_DIRECTORY
chown -R www-data:www-data $APPLICATION_DIRECTORY
echo

echo "Start Kestrel service..."
service kestrel-madmox-website start
echo
