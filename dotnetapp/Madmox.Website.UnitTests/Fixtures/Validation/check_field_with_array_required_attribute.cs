﻿using FluentAssertions;
using Madmox.Website.Core.Validation;
using NUnit.Framework;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace Madmox.Website.UnitTests.Fixtures.Validation
{
    public class check_field_with_array_required_attribute
    {
        [Test]
        public void validate_with_value_type_array()
        {
            // Arrange
            var value = new Foo<int>(new List<int>() { 0, 1 });

            // Act
            bool isValid = Validate(value);

            // Assert
            isValid.Should().BeTrue();
        }

        [Test]
        public void validate_with_reference_type_array()
        {
            // Arrange
            var value = new Foo<Bar>(new List<Bar>() { new Bar("bar 1"), new Bar("bar 2") });

            // Act
            bool isValid = Validate(value);

            // Assert
            isValid.Should().BeTrue();
        }

        [Test]
        public void fail_to_validate_with_null_array()
        {
            // Arrange
            var value = new Foo<int>(null);

            // Act
            bool isValid = Validate(value);

            // Assert
            isValid.Should().BeFalse();
        }

        [Test]
        public void fail_to_validate_with_null_reference_value()
        {
            // Arrange
            var value = new Foo<Bar>(new List<Bar>() { new Bar("bar 1"), null });

            // Act
            bool isValid = Validate(value);

            // Assert
            isValid.Should().BeFalse();
        }

        [Test]
        public void fail_to_validate_with_null_nullable_value_type_value()
        {
            // Arrange
            var value = new Foo<int?>(new List<int?>() { 0, null });

            // Act
            bool isValid = Validate(value);

            // Assert
            isValid.Should().BeFalse();
        }

        private static bool Validate(object value)
        {
            var context = new ValidationContext(value, serviceProvider: null, items: null);
            var validationResults = new List<ValidationResult>();
            return Validator.TryValidateObject(value, context, validationResults, true);
        }

        private class Foo<T>
        {
            [ArrayRequired]
            public List<T> Value { get; set; }

            public Foo(List<T> value)
            {
                this.Value = value;
            }
        }

        private class Bar
        {
            public string Value { get; set; }

            public Bar(string value)
            {
                this.Value = value;
            }
        }
    }
}
