﻿using FakeItEasy;
using FluentAssertions;
using Madmox.Website.BusinessComponents.Authentication;
using Madmox.Website.Core.Security;
using Madmox.Website.Core.Types;
using Madmox.Website.DataAccess.Database.Entities;
using Madmox.Website.DataAccess.Database.Repositories;
using NodaTime;
using NUnit.Framework;
using System;
using System.Threading.Tasks;

namespace Madmox.Website.UnitTests.Fixtures.Authentication
{
    public class verify_credentials
    {
        private IApplicationUserRepository _applicationUserRepository;
        private IIpLogRepository _ipLogRepository;
        private IAuthenticator _authenticator;

        [SetUp]
        public void Setup()
        {
            this._applicationUserRepository = A.Fake<IApplicationUserRepository>();
            this._ipLogRepository = A.Fake<IIpLogRepository>();
            this._authenticator = new Authenticator(
                this._applicationUserRepository,
                this._ipLogRepository,
                A.Fake<IPasswordHasher>()
            );
        }

        [Test]
        public async Task validate_if_they_are_valid()
        {
            // Arrange
            var user = new ApplicationUserEntity()
            {
                Id = Guid.NewGuid(),
                Email = "user@test.com",
                IsPasswordClearText = true,
                Password = "password"
            };
            A.CallTo(() => this._applicationUserRepository.GetByEmail(user.Email))
                .Returns(user);
            A.CallTo(() => this._ipLogRepository.CountByIpAddressSinceTimestamp(A<IpLogAction>.Ignored, A<string>.Ignored, A<Instant>.Ignored))
                .Returns(0);

            // Act
            ApplicationUserEntity result = await this._authenticator.VerifyCredentials(user.Email, user.Password, ipAddress: "127.0.0.1");

            // Assert
            result.Should().NotBeNull();
        }

        [Test]
        public async Task fail_to_validate_if_email_is_invalid()
        {
            // Arrange
            var user = new ApplicationUserEntity()
            {
                Id = Guid.NewGuid(),
                Email = "user@test.com",
                IsPasswordClearText = true,
                Password = "password"
            };
            A.CallTo(() => this._applicationUserRepository.GetByEmail(user.Email))
                .Returns(user);
            A.CallTo(() => this._ipLogRepository.CountByIpAddressSinceTimestamp(A<IpLogAction>.Ignored, A<string>.Ignored, A<Instant>.Ignored))
                .Returns(0);

            // Act
            ApplicationUserEntity result = await this._authenticator.VerifyCredentials("invalid@test.com", user.Password, ipAddress: "127.0.0.1");

            // Assert
            result.Should().BeNull();
        }

        [Test]
        public async Task fail_to_validate_if_password_is_invalid()
        {
            // Arrange
            var user = new ApplicationUserEntity()
            {
                Id = Guid.NewGuid(),
                Email = "user@test.com",
                IsPasswordClearText = true,
                Password = "password"
            };
            A.CallTo(() => this._applicationUserRepository.GetByEmail(user.Email))
                .Returns(user);
            A.CallTo(() => this._ipLogRepository.CountByIpAddressSinceTimestamp(A<IpLogAction>.Ignored, A<string>.Ignored, A<Instant>.Ignored))
                .Returns(0);

            // Act
            ApplicationUserEntity result = await this._authenticator.VerifyCredentials(user.Email, "invalid", ipAddress: "127.0.0.1");

            // Assert
            result.Should().BeNull();
        }

        [Test]
        public async Task fail_to_validate_if_too_many_failed_signin_attempts_for_ip_address()
        {
            // Arrange
            var user = new ApplicationUserEntity()
            {
                Id = Guid.NewGuid(),
                Email = "user@test.com",
                IsPasswordClearText = true,
                Password = "password"
            };
            A.CallTo(() => this._applicationUserRepository.GetByEmail(user.Email))
                .Returns(user);
            A.CallTo(() => this._ipLogRepository.CountByIpAddressSinceTimestamp(A<IpLogAction>.Ignored, "127.0.0.1", A<Instant>.Ignored))
                .Returns(SecurityConstants.FAILED_VERIFY_CREDENTIALS_THRESHOLD);

            // Act
            Func<Task<ApplicationUserEntity>> act = async () => await this._authenticator.VerifyCredentials(user.Email, user.Password, ipAddress: "127.0.0.1");

            // Assert
            await act.Should().ThrowAsync<TooManyFailedAttemptsException>();
        }
    }
}
