﻿using FluentAssertions;
using Madmox.Website.Core.Git;
using NUnit.Framework;

namespace Madmox.Website.UnitTests.Fixtures.Git
{
    public class git_service
    {
        [Test]
        public void get_git_revision_hash()
        {
            // Arrange
            IGitService git = new GitService();

            // Act
            string revisionHash = git.GetRevisionHash();

            // Assert
            revisionHash.Should().NotBeNullOrEmpty();
        }
    }
}
