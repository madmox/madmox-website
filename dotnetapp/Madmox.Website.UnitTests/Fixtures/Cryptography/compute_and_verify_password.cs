﻿using FluentAssertions;
using Madmox.Website.Core.Security;
using NUnit.Framework;

namespace Madmox.Website.UnitTests.Fixtures.Cryptography
{
    public class compute_and_verify_password
    {
        private IPasswordHasher _passwordHasher;

        [SetUp]
        public void Setup()
        {
            this._passwordHasher = new Pbkdf2PasswordHasher();
        }

        [Test]
        public void validate_roundtrip()
        {
            // Arrange
            string hashedPassword = this._passwordHasher.ComputePasswordHash("foo");

            // Act
            bool valid = this._passwordHasher.VerifyPassword("foo", hashedPassword);

            // Assert
            valid.Should().BeTrue();
        }

        [Test]
        public void fail_to_validate_roundtrip_with_invalid_password()
        {
            // Arrange
            string hashedPassword = this._passwordHasher.ComputePasswordHash("foo");

            // Act
            bool valid = this._passwordHasher.VerifyPassword("bar", hashedPassword);

            // Assert
            valid.Should().BeFalse();
        }
    }
}
