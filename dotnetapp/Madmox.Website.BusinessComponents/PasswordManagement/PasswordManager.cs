﻿using Madmox.Website.BusinessComponents.Authentication;
using Madmox.Website.Core.Identity;
using Madmox.Website.Core.Security;
using Madmox.Website.DataAccess.Database.Entities;
using Madmox.Website.DataAccess.Database.Repositories;
using System.Threading.Tasks;

namespace Madmox.Website.BusinessComponents.PasswordManagement
{
    public class PasswordManager : IPasswordManager
    {
        private readonly IAuthenticator _authenticator;
        private readonly IApplicationUserRepository _applicationUserRepository;
        private readonly IPasswordHasher _passwordHasher;
        private readonly IPasswordPolicyManager _passwordPolicyManager;

        public PasswordManager(
            IAuthenticator authenticator,
            IApplicationUserRepository applicationUserRepository,
            IPasswordHasher passwordHasher,
            IPasswordPolicyManager passwordPolicyManager
        )
        {
            this._authenticator = authenticator;
            this._applicationUserRepository = applicationUserRepository;
            this._passwordHasher = passwordHasher;
            this._passwordPolicyManager = passwordPolicyManager;
        }

        async Task IPasswordManager.Update(UserIdentity currentUser, string currentPassword, string newPassword, string ipAddress)
        {
            ApplicationUserEntity user = await this._authenticator.VerifyCredentials(currentUser.Id, currentPassword, ipAddress);
            if (user == null)
            {
                throw new InvalidCurrentPasswordException("Invalid current password");
            }

            this._passwordPolicyManager.VerifyPasswordPolicy(newPassword);

            user.Password = this._passwordHasher.ComputePasswordHash(newPassword);
            user.IsPasswordClearText = false;
            await this._applicationUserRepository.Update(user);
        }
    }
}
