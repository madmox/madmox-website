﻿using System;

namespace Madmox.Website.BusinessComponents.PasswordManagement
{
    public class InvalidCurrentPasswordException : Exception
    {
        public InvalidCurrentPasswordException(string message) : base(message)
        {
        }
    }
}
