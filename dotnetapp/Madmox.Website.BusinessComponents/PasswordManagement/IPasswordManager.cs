﻿using Madmox.Website.Core.Identity;
using System.Threading.Tasks;

namespace Madmox.Website.BusinessComponents.PasswordManagement
{
    public interface IPasswordManager
    {
        Task Update(UserIdentity currentUser, string currentPassword, string newPassword, string ipAddress);
    }
}
