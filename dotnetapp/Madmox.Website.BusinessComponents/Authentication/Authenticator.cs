﻿using Madmox.Website.Core.Security;
using Madmox.Website.Core.Types;
using Madmox.Website.DataAccess.Database.Entities;
using Madmox.Website.DataAccess.Database.Repositories;
using NodaTime;
using System;
using System.Threading.Tasks;

namespace Madmox.Website.BusinessComponents.Authentication
{
    public class Authenticator : IAuthenticator
    {
        private readonly IApplicationUserRepository _applicationUserRepository;
        private readonly IIpLogRepository _ipLogRepository;
        private readonly IPasswordHasher _passwordHasher;

        public Authenticator(
            IApplicationUserRepository applicationUserRepository,
            IIpLogRepository ipLogRepository,
            IPasswordHasher passwordHasher
        )
        {
            this._applicationUserRepository = applicationUserRepository;
            this._ipLogRepository = ipLogRepository;
            this._passwordHasher = passwordHasher;
        }

        async Task<ApplicationUserEntity> IAuthenticator.VerifyCredentials(string email, string password, string ipAddress)
        {
            ipAddress ??= "localhost";

            // Check the IP address is authorized to attempt to sign
            await this.AuthorizeAttempt(
                ipAddress,
                action: IpLogAction.VERIFY_CREDENTIALS,
                scanPeriod: SecurityConstants.FAILED_VERIFY_CREDENTIALS_SCAN_PERIOD,
                threshold: SecurityConstants.FAILED_VERIFY_CREDENTIALS_THRESHOLD
            );

            // Check the account exists
            ApplicationUserEntity user = await this._applicationUserRepository.GetByEmail(email);
            if (user == null)
            {
                await this.LogFailedAttempt(ipAddress, IpLogAction.VERIFY_CREDENTIALS, context: email);
                return null;
            }

            // Check credentials
            return await this.VerifyCredentials(user, password, ipAddress);
        }

        async Task<ApplicationUserEntity> IAuthenticator.VerifyCredentials(Guid applicationUserId, string password, string ipAddress)
        {
            ipAddress ??= "localhost";

            // Check the IP address is authorized to attempt to sign
            await this.AuthorizeAttempt(
                ipAddress,
                action: IpLogAction.VERIFY_CREDENTIALS,
                scanPeriod: SecurityConstants.FAILED_VERIFY_CREDENTIALS_SCAN_PERIOD,
                threshold: SecurityConstants.FAILED_VERIFY_CREDENTIALS_THRESHOLD
            );

            // Check the account exists
            ApplicationUserEntity user = await this._applicationUserRepository.Get(applicationUserId);
            if (user == null)
            {
                return null;
            }

            // Check credentials
            return await this.VerifyCredentials(user, password, ipAddress);
        }

        private async Task<ApplicationUserEntity> VerifyCredentials(ApplicationUserEntity user, string password, string ipAddress)
        {
            if (string.IsNullOrEmpty(user.Password) || password.Length > SecurityConstants.PASSWORD_MAX_LENGTH)
            {
                await this.LogFailedAttempt(ipAddress, IpLogAction.VERIFY_CREDENTIALS, context: user.Email);
                return null;
            }

            if (user.IsPasswordClearText)
            {
                bool valid = string.Equals(password, user.Password, StringComparison.Ordinal);
                if (valid)
                {
                    user.Password = this._passwordHasher.ComputePasswordHash(user.Password);
                    user.IsPasswordClearText = false;
                    await this._applicationUserRepository.Update(user);

                    return user;
                }
                else
                {
                    await this.LogFailedAttempt(ipAddress, IpLogAction.VERIFY_CREDENTIALS, context: user.Email);
                    return null;
                }
            }
            else
            {
                bool valid = this._passwordHasher.VerifyPassword(password, user.Password);
                if (valid)
                {
                    return user;
                }
                else
                {
                    await this.LogFailedAttempt(ipAddress, IpLogAction.VERIFY_CREDENTIALS, context: user.Email);
                    return null;
                }
            }
        }

        private async Task AuthorizeAttempt(string ipAddress, IpLogAction action, Duration scanPeriod, int threshold)
        {
            Instant now = SystemClock.Instance.GetCurrentInstant();

            int ipLogs = await this._ipLogRepository.CountByIpAddressSinceTimestamp(
                action,
                ipAddress,
                timestamp: now.Minus(scanPeriod)
            );

            if (ipLogs >= threshold)
            {
                throw new TooManyFailedAttemptsException($"Too many failed attempts. Please wait before retrying.");
            }
        }

        private async Task LogFailedAttempt(string ipAddress, IpLogAction action, string context)
        {
            Instant now = SystemClock.Instance.GetCurrentInstant();

            var attempt = new IpLogEntity()
            {
                Id = Guid.NewGuid(),
                IpAddress = ipAddress,
                Action = action,
                Context = context,
                Timestamp = now,
            };
            await this._ipLogRepository.Insert(attempt);
        }
    }
}
