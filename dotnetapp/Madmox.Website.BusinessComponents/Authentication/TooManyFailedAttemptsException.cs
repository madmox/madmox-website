﻿using System;

namespace Madmox.Website.BusinessComponents.Authentication
{
    public class TooManyFailedAttemptsException : Exception
    {
        public TooManyFailedAttemptsException(string message) : base(message)
        {
        }
    }
}
