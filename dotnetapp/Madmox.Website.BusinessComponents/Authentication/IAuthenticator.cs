﻿using Madmox.Website.DataAccess.Database.Entities;
using System;
using System.Threading.Tasks;

namespace Madmox.Website.BusinessComponents.Authentication
{
    public interface IAuthenticator
    {
        Task<ApplicationUserEntity> VerifyCredentials(string email, string password, string ipAddress);
        Task<ApplicationUserEntity> VerifyCredentials(Guid applicationUserId, string password, string ipAddress);
    }
}
