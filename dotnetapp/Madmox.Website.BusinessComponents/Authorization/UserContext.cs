﻿using Madmox.Website.Core.Identity;

namespace Madmox.Website.BusinessComponents.Authorization
{
    public class UserContext
    {
        public UserIdentity User { get; }
        public bool IsAdmin { get; }

        public UserContext(UserIdentity user, bool isAdmin)
        {
            this.User = user;
            this.IsAdmin = isAdmin;
        }
    }
}
