﻿using Madmox.Website.Core.Identity;
using Madmox.Website.DataAccess.Database.Entities;
using Madmox.Website.DataAccess.Database.Repositories;
using System;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace Madmox.Website.BusinessComponents.Authorization
{
    public class AuthorizerProvider : IAuthorizerProvider
    {
        private readonly Dictionary<Guid, IAuthorizer> _cache = new();
        private readonly IApplicationUserRepository _applicationUserRepository;
        private readonly IPermissionsProvider _permissionsProvider;

        public AuthorizerProvider(
            IApplicationUserRepository applicationUserRepository,
            IPermissionsProvider permissionsProvider
        )
        {
            this._applicationUserRepository = applicationUserRepository;
            this._permissionsProvider = permissionsProvider;
        }

        async Task<IAuthorizer> IAuthorizerProvider.GetAuthorizer(UserIdentity identity, bool reload)
        {
            if (reload)
            {
                this._cache.Clear();
            }

            if (this._cache.ContainsKey(identity.Id))
            {
                return this._cache[identity.Id];
            }

            bool isAdmin;
            if (!identity.IsAuthenticated)
            {
                isAdmin = false;
            }
            else
            {
                ApplicationUserEntity user = await this._applicationUserRepository.Get(identity.Id);
                isAdmin = user.IsAdmin;
            }

            var userContext = new UserContext(identity, isAdmin);
            var authorizer = new Authorizer(userContext, this._permissionsProvider);

            this._cache[identity.Id] = authorizer;
            return authorizer;
        }
    }
}
