﻿using Madmox.Website.Core.Identity;
using System.Collections.Generic;
using System.Linq;

namespace Madmox.Website.BusinessComponents.Authorization
{
    public class PermissionsProvider : IPermissionsProvider
    {
        private readonly IAdditionalPermissionsProvider _additionalPermissionsProvider;

        public PermissionsProvider(IAdditionalPermissionsProvider additionalPermissionsProvider)
        {
            this._additionalPermissionsProvider = additionalPermissionsProvider;
        }

        List<ApplicationPermission> IPermissionsProvider.ListApplicationPermissions(UserContext userContext)
        {
            // Consolidate all permissions
            var consolidatedPermissions = new List<ApplicationPermission>();
            if (userContext.IsAdmin)
            {
                consolidatedPermissions.AddRange(AdminApplicationPermissions.Table);
            }

            // Permissions injected for tests
            IEnumerable<ApplicationPermission> additionalPermissions = this._additionalPermissionsProvider.ListApplicationPermissions(userContext.User.Id);

            return consolidatedPermissions
                .Concat(additionalPermissions)
                .Distinct()
                .ToList();
        }
    }
}
