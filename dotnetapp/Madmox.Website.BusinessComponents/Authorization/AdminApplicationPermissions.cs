﻿using Madmox.Website.Core.Identity;
using System.Collections.Generic;

namespace Madmox.Website.BusinessComponents.Authorization
{
    internal static class AdminApplicationPermissions
    {
        public static List<ApplicationPermission> Table => new()
        {
            ApplicationPermission.ADMIN_ACCESS,
        };
    }
}
