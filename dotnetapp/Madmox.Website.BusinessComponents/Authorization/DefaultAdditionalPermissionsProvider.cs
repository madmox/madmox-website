﻿using Madmox.Website.Core.Identity;
using System;
using System.Collections.Generic;
using System.Linq;

namespace Madmox.Website.BusinessComponents.Authorization
{
    public class DefaultAdditionalPermissionsProvider : IAdditionalPermissionsProvider
    {
        public DefaultAdditionalPermissionsProvider()
        {
        }

        IEnumerable<ApplicationPermission> IAdditionalPermissionsProvider.ListApplicationPermissions(Guid applicationUserId) => Enumerable.Empty<ApplicationPermission>();
    }
}
