﻿using Madmox.Website.Core.Identity;
using System.Collections.Generic;

namespace Madmox.Website.BusinessComponents.Authorization
{
    public interface IAuthorizer
    {
        bool HasApplicationPermission(ApplicationPermission permission);
        List<ApplicationPermission> ListApplicationPermissions();
    }
}
