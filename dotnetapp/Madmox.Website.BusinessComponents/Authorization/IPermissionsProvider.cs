﻿using Madmox.Website.Core.Identity;
using System.Collections.Generic;

namespace Madmox.Website.BusinessComponents.Authorization
{
    public interface IPermissionsProvider
    {
        List<ApplicationPermission> ListApplicationPermissions(UserContext userContext);
    }
}
