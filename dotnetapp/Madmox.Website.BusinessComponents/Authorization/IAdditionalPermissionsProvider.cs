﻿using Madmox.Website.Core.Identity;
using System;
using System.Collections.Generic;

namespace Madmox.Website.BusinessComponents.Authorization
{
    public interface IAdditionalPermissionsProvider
    {
        IEnumerable<ApplicationPermission> ListApplicationPermissions(Guid applicationUserId);
    }
}
