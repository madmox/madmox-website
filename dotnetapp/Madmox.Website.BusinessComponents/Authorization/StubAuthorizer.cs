﻿using Madmox.Website.Core.Identity;
using System;
using System.Collections.Generic;

namespace Madmox.Website.BusinessComponents.Authorization
{
    public class StubAuthorizer : IAuthorizer
    {
        public StubAuthorizer()
        {
        }

        bool IAuthorizer.HasApplicationPermission(ApplicationPermission permission)
        {
            throw new NotImplementedException();
        }

        List<ApplicationPermission> IAuthorizer.ListApplicationPermissions()
        {
            throw new NotImplementedException();
        }
    }
}
