﻿using Madmox.Website.Core.Identity;
using System.Threading.Tasks;

namespace Madmox.Website.BusinessComponents.Authorization
{
    public interface IAuthorizerProvider
    {
        Task<IAuthorizer> GetAuthorizer(UserIdentity identity, bool reload = false);
    }
}
