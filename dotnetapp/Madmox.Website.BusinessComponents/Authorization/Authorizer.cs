﻿using Madmox.Website.Core.Identity;
using System.Collections.Generic;

namespace Madmox.Website.BusinessComponents.Authorization
{
    public class Authorizer : IAuthorizer
    {
        private readonly IPermissionsProvider _permissionsProvider;

        private UserContext UserContext { get; }

        public Authorizer(UserContext userContext, IPermissionsProvider permissionsProvider)
        {
            this.UserContext = userContext;
            this._permissionsProvider = permissionsProvider;
        }

        bool IAuthorizer.HasApplicationPermission(ApplicationPermission permission)
        {
            return this.HasApplicationPermission(permission);
        }

        List<ApplicationPermission> IAuthorizer.ListApplicationPermissions()
        {
            return this._permissionsProvider.ListApplicationPermissions(this.UserContext);
        }

        // Internals

        private bool HasApplicationPermission(ApplicationPermission permission)
        {
            List<ApplicationPermission> permissions = this._permissionsProvider.ListApplicationPermissions(this.UserContext);
            return permissions.Contains(permission);
        }
    }
}
