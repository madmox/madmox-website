﻿using Madmox.Website.BusinessComponents.Authentication;
using Madmox.Website.BusinessComponents.Authorization;
using Madmox.Website.BusinessComponents.PasswordManagement;
using Microsoft.Extensions.Configuration;

namespace Microsoft.Extensions.DependencyInjection
{
    public static class IServiceCollectionExtensions
    {
        public static IServiceCollection AddApplicationBusinessComponentsServices(this IServiceCollection @this, IConfiguration config)
        {
            // Authentication
            @this.AddScoped<IAuthenticator, Authenticator>();

            // Authorization
            @this.AddScoped<IAdditionalPermissionsProvider, DefaultAdditionalPermissionsProvider>();
            @this.AddScoped<IAuthorizerProvider, AuthorizerProvider>();
            @this.AddScoped<IPermissionsProvider, PermissionsProvider>();

            // PasswordManager
            @this.AddScoped<IPasswordManager, PasswordManager>();

            return @this;
        }
    }
}
