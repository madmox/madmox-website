﻿namespace Madmox.Website.BusinessComponents.Errors
{
    public class AuthorizationException : BusinessException
    {
        public const string AUTHORIZATION_FAILED = "AUTHORIZATION_FAILED";

        public AuthorizationException(string message) : base(AUTHORIZATION_FAILED, message)
        {
        }

        public AuthorizationException(string code, string message) : base(code, message)
        {
        }
    }
}
