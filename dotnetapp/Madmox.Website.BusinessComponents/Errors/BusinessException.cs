﻿using System;

namespace Madmox.Website.BusinessComponents.Errors
{
    public abstract class BusinessException : Exception
    {
        public string Code { get; }

        public BusinessException(string code, string message) : base(message)
        {
            this.Code = code;
        }
    }
}
