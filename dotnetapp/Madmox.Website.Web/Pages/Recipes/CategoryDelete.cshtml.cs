using Madmox.Website.BusinessComponents.Authorization;
using Madmox.Website.Core.Identity;
using Madmox.Website.Core.Types;
using Madmox.Website.DataAccess.Assets;
using Madmox.Website.DataAccess.Database.Entities;
using Madmox.Website.DataAccess.Database.Repositories;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.RazorPages;
using System;
using System.Threading.Tasks;

namespace Madmox.Website.Web.Pages.Recipes
{
    [Authorize]
    public class CategoryDeleteModel : PageModel
    {
        private readonly IAssetsDriver _assetsDriver;
        private readonly IAuthorizerProvider _authorizerProvider;
        private readonly IRecipeCategoryRepository _recipeCategoryRepository;
        private readonly IRecipeRepository _recipeRepository;

        public Guid Id { get; set; }
        public string Name { get; set; }

        public CategoryDeleteModel(
            IAssetsDriver assetsDriver,
            IAuthorizerProvider authorizerProvider,
            IRecipeCategoryRepository recipeCategoryRepository,
            IRecipeRepository recipeRepository
        )
        {
            this._assetsDriver = assetsDriver;
            this._authorizerProvider = authorizerProvider;
            this._recipeCategoryRepository = recipeCategoryRepository;
            this._recipeRepository = recipeRepository;
        }

        public async Task<IActionResult> OnGet(Guid id, string slug)
        {
            var currentUser = (UserIdentity)this.User;
            IAuthorizer authorizer = await this._authorizerProvider.GetAuthorizer(currentUser);
            bool authorized = authorizer.HasApplicationPermission(ApplicationPermission.ADMIN_ACCESS);
            if (!authorized)
            {
                return new ForbidResult();
            }

            // Gather relevant data
            RecipeCategoryEntity recipeCategory = await this._recipeCategoryRepository.Get(id);
            if (recipeCategory == null)
            {
                return new NotFoundResult();
            }
            EntitySet<RecipeEntity> recipes = await this._recipeRepository.ListByCategoryId(id);

            // Validate input data
            string actualSlug = recipeCategory.Name.Slugify();
            if (recipes.Count > 0)
            {
                string redirectUrl = this.Url.Page("./Category", new { id = id, slug = actualSlug });
                return new RedirectResult(redirectUrl, permanent: false);
            }
            if (!string.Equals(actualSlug, slug, StringComparison.Ordinal))
            {
                string redirectUrl = this.Url.Page("./CategoryDelete", new { id = id, slug = actualSlug });
                return new RedirectResult(redirectUrl, permanent: true);
            }

            // Build result
            this.InitializeViewModel(recipeCategory);
            return new PageResult();
        }

        public async Task<IActionResult> OnPost(Guid id, string slug)
        {
            var currentUser = (UserIdentity)this.User;
            IAuthorizer authorizer = await this._authorizerProvider.GetAuthorizer(currentUser);
            bool authorized = authorizer.HasApplicationPermission(ApplicationPermission.ADMIN_ACCESS);
            if (!authorized)
            {
                return new ForbidResult();
            }

            // Gather relevant data
            RecipeCategoryEntity recipeCategory = await this._recipeCategoryRepository.Get(id);
            if (recipeCategory == null)
            {
                return new NotFoundResult();
            }
            EntitySet<RecipeEntity> recipes = await this._recipeRepository.ListByCategoryId(id);

            // Validate input data
            string actualSlug = recipeCategory.Name.Slugify();
            if (recipes.Count > 0)
            {
                string redirectUrl = this.Url.Page("./Category", new { id = id, slug = actualSlug });
                return new RedirectResult(redirectUrl, permanent: false);
            }
            if (!this.ModelState.IsValid)
            {
                this.InitializeViewModel(recipeCategory);
                return new PageResult();
            }

            // Update database
            await this._recipeCategoryRepository.Delete(id);

            // Update assets
            await this._assetsDriver.DeleteAsset($"images/recipes/categories/{id}/photo.png");

            // Build result
            string url = this.Url.Page("./Index");
            return new RedirectResult(url, permanent: false);
        }

        private void InitializeViewModel(RecipeCategoryEntity recipeCategory)
        {
            this.Id = recipeCategory.Id;
            this.Name = recipeCategory.Name;
        }
    }
}
