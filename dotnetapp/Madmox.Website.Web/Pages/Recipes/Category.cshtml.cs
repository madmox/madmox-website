using Madmox.Website.BusinessComponents.Authorization;
using Madmox.Website.Core.Identity;
using Madmox.Website.Core.Types;
using Madmox.Website.DataAccess.Database.Entities;
using Madmox.Website.DataAccess.Database.Repositories;
using Madmox.Website.Web.ViewModels.Recipes.Category;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.RazorPages;
using System;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace Madmox.Website.Web.Pages.Recipes
{
    public class CategoryModel : PageModel
    {
        private readonly IAuthorizerProvider _authorizerProvider;
        private readonly IRecipeCategoryRepository _recipeCategoryRepository;
        private readonly IRecipeRepository _recipeRepository;

        public Guid Id { get; set; }
        public string Name { get; set; }
        public List<RecipeVM> Recipes { get; set; }
        public bool CanManage { get; set; }

        public CategoryModel(
            IAuthorizerProvider authorizerProvider,
            IRecipeCategoryRepository recipeCategoryRepository,
            IRecipeRepository recipeRepository
        )
        {
            this._authorizerProvider = authorizerProvider;
            this._recipeCategoryRepository = recipeCategoryRepository;
            this._recipeRepository = recipeRepository;
        }

        public async Task<IActionResult> OnGet(Guid id, string slug)
        {
            // Gather relevant data
            RecipeCategoryEntity category = await this._recipeCategoryRepository.Get(id);
            if (category == null)
            {
                return new NotFoundResult();
            }
            EntitySet<RecipeEntity> recipes = await this._recipeRepository.ListByCategoryId(id);

            // Validate input data
            string actualSlug = category.Name.Slugify();
            if (!string.Equals(actualSlug, slug, StringComparison.Ordinal))
            {
                string url = this.Url.Page("./Category", new { id = category.Id, slug = actualSlug });
                return new RedirectResult(url, permanent: true);
            }

            // Build result
            await this.InitializeViewModel(category, recipes);
            return new PageResult();
        }

        private async Task InitializeViewModel(RecipeCategoryEntity category, EntitySet<RecipeEntity> recipes)
        {
            var currentUser = (UserIdentity)this.User;
            IAuthorizer authorizer = await this._authorizerProvider.GetAuthorizer(currentUser);

            this.Id = category.Id;
            this.Name = category.Name;
            this.Recipes = new List<RecipeVM>();
            foreach (RecipeEntity recipe in recipes)
            {
                var entry = new RecipeVM();
                entry.Map(recipe);
                this.Recipes.Add(entry);
            }
            this.CanManage = authorizer.HasApplicationPermission(ApplicationPermission.ADMIN_ACCESS);
        }
    }
}
