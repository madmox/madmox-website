using Madmox.Website.BusinessComponents.Authorization;
using Madmox.Website.Core.Identity;
using Madmox.Website.DataAccess.Database.Entities;
using Madmox.Website.DataAccess.Database.Repositories;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.RazorPages;
using NodaTime;
using System;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace Madmox.Website.Web.Pages.Recipes
{
    public class RecipeModel : PageModel
    {
        private readonly IAuthorizerProvider _authorizerProvider;
        private readonly IRecipeRepository _recipeRepository;

        public Guid Id { get; set; }
        public Guid CategoryId { get; set; }
        public string Name { get; set; }
        public int NbPersons { get; set; }
        public Duration PreparationTime { get; set; }
        public Duration TotalTime { get; set; }
        public List<string> Ingredients { get; set; }
        public List<string> Tools { get; set; }
        public List<string> Steps { get; set; }
        public bool CanManage { get; set; }

        public RecipeModel(
            IAuthorizerProvider authorizerProvider,
            IRecipeRepository recipeRepository
        )
        {
            this._authorizerProvider = authorizerProvider;
            this._recipeRepository = recipeRepository;
        }

        public async Task<IActionResult> OnGet(Guid id, string slug)
        {
            // Gather relevant data
            RecipeEntity recipe = await this._recipeRepository.Get(id);
            if (recipe == null)
            {
                return new NotFoundResult();
            }

            // Validate input data
            string actualSlug = recipe.Name.Slugify();
            if (!string.Equals(actualSlug, slug, StringComparison.Ordinal))
            {
                string url = this.Url.Page("./Recipe", new { id = recipe.Id, slug = actualSlug });
                return new RedirectResult(url, permanent: true);
            }

            await this.InitializeViewModel(recipe);

            return new PageResult();
        }

        private async Task InitializeViewModel(RecipeEntity recipe)
        {
            var currentUser = (UserIdentity)this.User;
            IAuthorizer authorizer = await this._authorizerProvider.GetAuthorizer(currentUser);

            this.Id = recipe.Id;
            this.CategoryId = recipe.CategoryId;
            this.Name = recipe.Name;
            this.NbPersons = recipe.NbPersons;
            this.PreparationTime = recipe.PreparationTime;
            this.TotalTime = recipe.TotalTime;
            this.Ingredients = recipe.Ingredients;
            this.Tools = recipe.Tools;
            this.Steps = recipe.Steps;
            this.CanManage = authorizer.HasApplicationPermission(ApplicationPermission.ADMIN_ACCESS);
        }
    }
}
