using Madmox.Website.BusinessComponents.Authorization;
using Madmox.Website.Core.Identity;
using Madmox.Website.Core.Validation;
using Madmox.Website.DataAccess.Assets;
using Madmox.Website.DataAccess.Database.Entities;
using Madmox.Website.DataAccess.Database.Repositories;
using Madmox.Website.Web.Validation;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.RazorPages;
using NodaTime.Text;
using System;
using System.ComponentModel.DataAnnotations;
using System.IO;
using System.Linq;
using System.Threading.Tasks;

namespace Madmox.Website.Web.Pages.Recipes
{
    [Authorize]
    public class RecipeNewModel : PageModel
    {
        private readonly IAssetsDriver _assetsDriver;
        private readonly IAuthorizerProvider _authorizerProvider;
        private readonly IRecipeCategoryRepository _recipeCategoryRepository;
        private readonly IRecipeRepository _recipeRepository;

        public Guid CategoryId { get; set; }

        [BindProperty]
        [Required(ErrorMessage = "Ce champ est requis.")]
        [StringLength(ValidationConstants.STRING_MAX_LENGTH, ErrorMessage = "Ce champ contient trop de caract�res.")]
        public string Name { get; set; }

        [BindProperty]
        [Required(ErrorMessage = "Ce champ est requis.")]
        [MaxFileSize(maxSize: 1_000_000, ErrorMessage = "Le fichier est trop volumineux.")]
        [AllowedFileExtensions(extensions: "png", ErrorMessage = "L'extension du fichier est invalide.")]
        public IFormFile PhotoFile { get; set; }

        [BindProperty]
        [Required(ErrorMessage = "Ce champ est requis.")]
        [ValidInteger(MinValue = 1, MaxValue = 100, ErrorMessage = "Ce champ doit �tre un entier compris entre {0} et {1}.")]
        public string NbPersons { get; set; }

        [BindProperty]
        [Required(ErrorMessage = "Ce champ est requis.")]
        [ValidDuration(ErrorMessage = "Le format du champ est invalide.")]
        public string PreparationTime { get; set; }

        [BindProperty]
        [Required(ErrorMessage = "Ce champ est requis.")]
        [ValidDuration(ErrorMessage = "Le format du champ est invalide.")]
        public string TotalTime { get; set; }

        [BindProperty]
        [Required(ErrorMessage = "Ce champ est requis.")]
        [MultiLineString(MinLineCount = 0, MaxLineCount = 100, MaxLineLength = ValidationConstants.STRING_MAX_LENGTH, ErrorMessage = "Ce champ doit contenir entre {0} et {1} lignes d'au plus {2} caract�res.")]
        public string Ingredients { get; set; }

        [BindProperty]
        [Required(ErrorMessage = "Ce champ est requis.")]
        [MultiLineString(MinLineCount = 0, MaxLineCount = 100, MaxLineLength = ValidationConstants.STRING_MAX_LENGTH, ErrorMessage = "Ce champ doit contenir entre {0} et {1} lignes d'au plus {2} caract�res.")]
        public string Tools { get; set; }

        [BindProperty]
        [Required(ErrorMessage = "Ce champ est requis.")]
        [MultiLineString(MinLineCount = 0, MaxLineCount = 100, MaxLineLength = ValidationConstants.STRING_MAX_LENGTH, ErrorMessage = "Ce champ doit contenir entre {0} et {1} lignes d'au plus {2} caract�res.")]
        public string Steps { get; set; }

        public RecipeNewModel(
            IAssetsDriver assetsDriver,
            IAuthorizerProvider authorizerProvider,
            IRecipeCategoryRepository recipeCategoryRepository,
            IRecipeRepository recipeRepository
        )
        {
            this._assetsDriver = assetsDriver;
            this._authorizerProvider = authorizerProvider;
            this._recipeCategoryRepository = recipeCategoryRepository;
            this._recipeRepository = recipeRepository;
        }

        public async Task<IActionResult> OnGet([FromQuery(Name = "category_id")] Guid categoryId)
        {
            var currentUser = (UserIdentity)this.User;
            IAuthorizer authorizer = await this._authorizerProvider.GetAuthorizer(currentUser);
            bool authorized = authorizer.HasApplicationPermission(ApplicationPermission.ADMIN_ACCESS);
            if (!authorized)
            {
                return new ForbidResult();
            }

            // Gather relevant data
            RecipeCategoryEntity recipeCategory = await this._recipeCategoryRepository.Get(categoryId);
            if (recipeCategory == null)
            {
                return new NotFoundResult();
            }

            // Build result
            this.InitializeViewModel(recipeCategory);
            return new PageResult();
        }

        public async Task<IActionResult> OnPost([FromQuery(Name = "category_id")] Guid categoryId)
        {
            var currentUser = (UserIdentity)this.User;
            IAuthorizer authorizer = await this._authorizerProvider.GetAuthorizer(currentUser);
            bool authorized = authorizer.HasApplicationPermission(ApplicationPermission.ADMIN_ACCESS);
            if (!authorized)
            {
                return new ForbidResult();
            }

            // Gather relevant data
            RecipeCategoryEntity recipeCategory = await this._recipeCategoryRepository.Get(categoryId);
            if (recipeCategory == null)
            {
                return new NotFoundResult();
            }

            // Validate input data
            if (!this.ModelState.IsValid)
            {
                this.InitializeViewModel(recipeCategory);
                return new PageResult();
            }

            // Update database
            RecipeEntity recipe = this.CreateDataModel(recipeCategory);
            await this._recipeRepository.Insert(recipe);

            // Update assets
            using Stream assetStream = this.PhotoFile.OpenReadStream();
            await this._assetsDriver.UploadAsset($"images/recipes/recipes/{recipe.Id}/photo.png", assetStream);

            // Build result
            string actualSlug = recipe.Name.Slugify();
            string url = this.Url.Page("./Recipe", new { id = recipe.Id, slug = actualSlug });
            return new RedirectResult(url, permanent: false);
        }

        private void InitializeViewModel(RecipeCategoryEntity recipeCategory)
        {
            this.CategoryId = recipeCategory.Id;
        }

        private RecipeEntity CreateDataModel(RecipeCategoryEntity recipeCategory)
        {
            return new RecipeEntity()
            {
                Id = Guid.NewGuid(),
                CategoryId = recipeCategory.Id,
                Name = this.Name,
                NbPersons = int.Parse(this.NbPersons),
                PreparationTime = DurationPattern.Roundtrip.Parse(this.PreparationTime).Value,
                TotalTime = DurationPattern.Roundtrip.Parse(this.TotalTime).Value,
                Ingredients = this.Ingredients.Split(new string[] { Environment.NewLine }, StringSplitOptions.RemoveEmptyEntries).ToList(),
                Tools = this.Tools.Split(new string[] { Environment.NewLine }, StringSplitOptions.RemoveEmptyEntries).ToList(),
                Steps = this.Steps.Split(new string[] { Environment.NewLine }, StringSplitOptions.RemoveEmptyEntries).ToList(),
            };
        }
    }
}
