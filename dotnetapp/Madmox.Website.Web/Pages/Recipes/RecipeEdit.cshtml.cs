using Madmox.Website.BusinessComponents.Authorization;
using Madmox.Website.Core.Helpers;
using Madmox.Website.Core.Identity;
using Madmox.Website.Core.Types;
using Madmox.Website.Core.Validation;
using Madmox.Website.DataAccess.Assets;
using Madmox.Website.DataAccess.Database.Entities;
using Madmox.Website.DataAccess.Database.Repositories;
using Madmox.Website.Web.Validation;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.RazorPages;
using Microsoft.AspNetCore.Mvc.Rendering;
using NodaTime.Text;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.IO;
using System.Linq;
using System.Threading.Tasks;

namespace Madmox.Website.Web.Pages.Recipes
{
    [Authorize]
    public class RecipeEditModel : PageModel
    {
        private readonly IAssetsDriver _assetsDriver;
        private readonly IAuthorizerProvider _authorizerProvider;
        private readonly IRecipeCategoryRepository _recipeCategoryRepository;
        private readonly IRecipeRepository _recipeRepository;

        public Guid Id { get; set; }
        public string OriginalName { get; set; }
        public List<SelectListItem> AvailableCategories { get; set; }

        [BindProperty]
        [Required(ErrorMessage = "Ce champ est requis.")]
        [StringLength(ValidationConstants.STRING_MAX_LENGTH, ErrorMessage = "Ce champ contient trop de caract�res.")]
        public string Name { get; set; }

        [BindProperty]
        [Required(ErrorMessage = "Ce champ est requis.")]
        public Guid CategoryId { get; set; }

        [BindProperty]
        [MaxFileSize(maxSize: 1_000_000, ErrorMessage = "Le fichier est trop volumineux.")]
        [AllowedFileExtensions(extensions: "png", ErrorMessage = "L'extension du fichier est invalide.")]
        public IFormFile NewPhotoFile { get; set; }

        [BindProperty]
        [Required(ErrorMessage = "Ce champ est requis.")]
        [ValidInteger(MinValue = 1, MaxValue = 100, ErrorMessage = "Ce champ doit �tre un entier compris entre {0} et {1}.")]
        public string NbPersons { get; set; }

        [BindProperty]
        [Required(ErrorMessage = "Ce champ est requis.")]
        [ValidDuration(ErrorMessage = "Le format du champ est invalide.")]
        public string PreparationTime { get; set; }

        [BindProperty]
        [Required(ErrorMessage = "Ce champ est requis.")]
        [ValidDuration(ErrorMessage = "Le format du champ est invalide.")]
        public string TotalTime { get; set; }

        [BindProperty]
        [Required(ErrorMessage = "Ce champ est requis.")]
        [MultiLineString(MinLineCount = 0, MaxLineCount = 100, MaxLineLength = ValidationConstants.STRING_MAX_LENGTH, ErrorMessage = "Ce champ doit contenir entre {0} et {1} lignes d'au plus {2} caract�res.")]
        public string Ingredients { get; set; }

        [BindProperty]
        [Required(ErrorMessage = "Ce champ est requis.")]
        [MultiLineString(MinLineCount = 0, MaxLineCount = 100, MaxLineLength = ValidationConstants.STRING_MAX_LENGTH, ErrorMessage = "Ce champ doit contenir entre {0} et {1} lignes d'au plus {2} caract�res.")]
        public string Tools { get; set; }

        [BindProperty]
        [Required(ErrorMessage = "Ce champ est requis.")]
        [MultiLineString(MinLineCount = 0, MaxLineCount = 100, MaxLineLength = ValidationConstants.STRING_MAX_LENGTH, ErrorMessage = "Ce champ doit contenir entre {0} et {1} lignes d'au plus {2} caract�res.")]
        public string Steps { get; set; }

        public RecipeEditModel(
            IAssetsDriver assetsDriver,
            IAuthorizerProvider authorizerProvider,
            IRecipeCategoryRepository recipeCategoryRepository,
            IRecipeRepository recipeRepository
        )
        {
            this._assetsDriver = assetsDriver;
            this._authorizerProvider = authorizerProvider;
            this._recipeCategoryRepository = recipeCategoryRepository;
            this._recipeRepository = recipeRepository;
        }

        public async Task<IActionResult> OnGet(Guid id, string slug)
        {
            var currentUser = (UserIdentity)this.User;
            IAuthorizer authorizer = await this._authorizerProvider.GetAuthorizer(currentUser);
            bool authorized = authorizer.HasApplicationPermission(ApplicationPermission.ADMIN_ACCESS);
            if (!authorized)
            {
                return new ForbidResult();
            }

            // Gather relevant data
            RecipeEntity recipe = await this._recipeRepository.Get(id);
            if (recipe == null)
            {
                return new NotFoundResult();
            }

            // Validate input data
            string actualSlug = recipe.Name.Slugify();
            if (!string.Equals(actualSlug, slug, StringComparison.Ordinal))
            {
                string url = this.Url.Page("./RecipeEdit", new { id = id, slug = actualSlug });
                return new RedirectResult(url, permanent: true);
            }

            // Build result
            await this.InitializeViewModel(recipe, resetFormData: true);
            return new PageResult();
        }

        public async Task<IActionResult> OnPost(Guid id, string slug)
        {
            var currentUser = (UserIdentity)this.User;
            IAuthorizer authorizer = await this._authorizerProvider.GetAuthorizer(currentUser);
            bool authorized = authorizer.HasApplicationPermission(ApplicationPermission.ADMIN_ACCESS);
            if (!authorized)
            {
                return new ForbidResult();
            }

            // Gather relevant data
            RecipeEntity recipe = await this._recipeRepository.Get(id);
            if (recipe == null)
            {
                return new NotFoundResult();
            }

            // Validate input data
            if (!this.ModelState.IsValid)
            {
                await this.InitializeViewModel(recipe, resetFormData: false);
                return new PageResult();
            }
            RecipeCategoryEntity recipeCategory = await this._recipeCategoryRepository.Get(this.CategoryId);
            if (recipeCategory == null)
            {
                this.ModelState.AddModelError(nameof(this.CategoryId), "Cette cat�gorie n'existe pas.");
                await this.InitializeViewModel(recipe, resetFormData: false);
                return new PageResult();
            }

            // Update database
            RecipeEntity updatedRecipe = EntityHelper.CloneEntity(recipe);
            this.UpdateDataModel(updatedRecipe);
            await this._recipeRepository.Update(updatedRecipe);

            // Update assets
            if (this.NewPhotoFile != null)
            {
                using Stream assetStream = this.NewPhotoFile.OpenReadStream();
                await this._assetsDriver.UploadAsset($"images/recipes/recipes/{id}/photo.png", assetStream);
            }

            // Build result
            string actualSlug = updatedRecipe.Name.Slugify();
            string url = this.Url.Page("./Recipe", new { id = id, slug = actualSlug });
            return new RedirectResult(url, permanent: false);
        }

        private async Task InitializeViewModel(RecipeEntity recipe, bool resetFormData)
        {
            EntitySet<RecipeCategoryEntity> recipeCategories = await this._recipeCategoryRepository.List();

            this.Id = recipe.Id;
            this.OriginalName = recipe.Name;
            this.AvailableCategories = recipeCategories
                .OrderBy(x => x.Position)
                .Select(x => new SelectListItem(x.Name, x.Id.ToString()))
                .ToList();

            if (resetFormData)
            {
                this.Name = recipe.Name;
                this.CategoryId = recipe.CategoryId;
                this.NbPersons = recipe.NbPersons.ToString();
                this.PreparationTime = DurationPattern.Roundtrip.Format(recipe.PreparationTime);
                this.TotalTime = DurationPattern.Roundtrip.Format(recipe.TotalTime);
                this.Ingredients = string.Join(Environment.NewLine, recipe.Ingredients);
                this.Tools = string.Join(Environment.NewLine, recipe.Tools);
                this.Steps = string.Join(Environment.NewLine, recipe.Steps);
            }
        }

        private void UpdateDataModel(RecipeEntity recipe)
        {
            recipe.Name = this.Name;
            recipe.CategoryId = this.CategoryId;
            recipe.NbPersons = int.Parse(this.NbPersons);
            recipe.PreparationTime = DurationPattern.Roundtrip.Parse(this.PreparationTime).Value;
            recipe.TotalTime = DurationPattern.Roundtrip.Parse(this.TotalTime).Value;
            recipe.Ingredients = this.Ingredients.Split(new string[] { Environment.NewLine }, StringSplitOptions.RemoveEmptyEntries).ToList();
            recipe.Tools = this.Tools.Split(new string[] { Environment.NewLine }, StringSplitOptions.RemoveEmptyEntries).ToList();
            recipe.Steps = this.Steps.Split(new string[] { Environment.NewLine }, StringSplitOptions.RemoveEmptyEntries).ToList();
        }
    }
}
