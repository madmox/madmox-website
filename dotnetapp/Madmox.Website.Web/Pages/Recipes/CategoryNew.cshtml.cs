using Madmox.Website.BusinessComponents.Authorization;
using Madmox.Website.Core.Identity;
using Madmox.Website.Core.Validation;
using Madmox.Website.DataAccess.Assets;
using Madmox.Website.DataAccess.Database.Entities;
using Madmox.Website.DataAccess.Database.Repositories;
using Madmox.Website.Web.Validation;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.RazorPages;
using System;
using System.ComponentModel.DataAnnotations;
using System.IO;
using System.Threading.Tasks;

namespace Madmox.Website.Web.Pages.Recipes
{
    [Authorize]
    public class CategoryNewModel : PageModel
    {
        private readonly IAssetsDriver _assetsDriver;
        private readonly IAuthorizerProvider _authorizerProvider;
        private readonly IRecipeCategoryRepository _recipeCategoryRepository;

        [BindProperty]
        [Required(ErrorMessage = "Ce champ est requis.")]
        [StringLength(ValidationConstants.STRING_MAX_LENGTH, ErrorMessage = "Ce champ contient trop de caract�res.")]
        public string Name { get; set; }

        [BindProperty]
        [Required(ErrorMessage = "Ce champ est requis.")]
        [MaxFileSize(maxSize: 1_000_000, ErrorMessage = "Le fichier est trop volumineux.")]
        [AllowedFileExtensions(extensions: "png", ErrorMessage = "L'extension du fichier est invalide.")]
        public IFormFile PhotoFile { get; set; }

        [BindProperty]
        [Required(ErrorMessage = "Ce champ est requis.")]
        [ValidInteger(MinValue = 0, ErrorMessage = "Ce champ doit �tre un entier sup�rieur ou �gal � {0}.")]
        public string Position { get; set; }

        public CategoryNewModel(
            IAssetsDriver assetsDriver,
            IAuthorizerProvider authorizerProvider,
            IRecipeCategoryRepository recipeCategoryRepository
        )
        {
            this._assetsDriver = assetsDriver;
            this._authorizerProvider = authorizerProvider;
            this._recipeCategoryRepository = recipeCategoryRepository;
        }

        public async Task<IActionResult> OnGet()
        {
            var currentUser = (UserIdentity)this.User;
            IAuthorizer authorizer = await this._authorizerProvider.GetAuthorizer(currentUser);
            bool authorized = authorizer.HasApplicationPermission(ApplicationPermission.ADMIN_ACCESS);
            if (!authorized)
            {
                return new ForbidResult();
            }

            // Build result
            return new PageResult();
        }

        public async Task<IActionResult> OnPost()
        {
            var currentUser = (UserIdentity)this.User;
            IAuthorizer authorizer = await this._authorizerProvider.GetAuthorizer(currentUser);
            bool authorized = authorizer.HasApplicationPermission(ApplicationPermission.ADMIN_ACCESS);
            if (!authorized)
            {
                return new ForbidResult();
            }

            // Validate input data
            if (!this.ModelState.IsValid)
            {
                return new PageResult();
            }

            // Update database
            RecipeCategoryEntity recipeCategory = this.CreateDataModel();
            await this._recipeCategoryRepository.Insert(recipeCategory);

            // Update assets
            using Stream assetStream = this.PhotoFile.OpenReadStream();
            await this._assetsDriver.UploadAsset($"images/recipes/categories/{recipeCategory.Id}/photo.png", assetStream);

            // Build result
            string actualSlug = recipeCategory.Name.Slugify();
            string url = this.Url.Page("./Category", new { id = recipeCategory.Id, slug = actualSlug });
            return new RedirectResult(url, permanent: false);
        }

        private RecipeCategoryEntity CreateDataModel()
        {
            return new RecipeCategoryEntity()
            {
                Id = Guid.NewGuid(),
                Name = this.Name,
                Position = int.Parse(this.Position),
            };
        }
    }
}
