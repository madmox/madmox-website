using Madmox.Website.BusinessComponents.Authorization;
using Madmox.Website.Core.Helpers;
using Madmox.Website.Core.Identity;
using Madmox.Website.Core.Validation;
using Madmox.Website.DataAccess.Assets;
using Madmox.Website.DataAccess.Database.Entities;
using Madmox.Website.DataAccess.Database.Repositories;
using Madmox.Website.Web.Validation;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.RazorPages;
using System;
using System.ComponentModel.DataAnnotations;
using System.IO;
using System.Threading.Tasks;

namespace Madmox.Website.Web.Pages.Recipes
{
    [Authorize]
    public class CategoryEditModel : PageModel
    {
        private readonly IAssetsDriver _assetsDriver;
        private readonly IAuthorizerProvider _authorizerProvider;
        private readonly IRecipeCategoryRepository _recipeCategoryRepository;

        public Guid Id { get; set; }
        public string OriginalName { get; set; }

        [BindProperty]
        [Required(ErrorMessage = "Ce champ est requis.")]
        [StringLength(ValidationConstants.STRING_MAX_LENGTH, ErrorMessage = "Ce champ contient trop de caract�res.")]
        public string Name { get; set; }

        [BindProperty]
        [MaxFileSize(maxSize: 1_000_000, ErrorMessage = "Le fichier est trop volumineux.")]
        [AllowedFileExtensions(extensions: "png", ErrorMessage = "L'extension du fichier est invalide.")]
        public IFormFile NewPhotoFile { get; set; }

        [BindProperty]
        [Required(ErrorMessage = "Ce champ est requis.")]
        [ValidInteger(MinValue = 0, ErrorMessage = "Ce champ doit �tre un entier sup�rieur ou �gal � {0}.")]
        public string Position { get; set; }

        public CategoryEditModel(
            IAssetsDriver assetsDriver,
            IAuthorizerProvider authorizerProvider,
            IRecipeCategoryRepository recipeCategoryRepository
        )
        {
            this._assetsDriver = assetsDriver;
            this._authorizerProvider = authorizerProvider;
            this._recipeCategoryRepository = recipeCategoryRepository;
        }

        public async Task<IActionResult> OnGet(Guid id, string slug)
        {
            var currentUser = (UserIdentity)this.User;
            IAuthorizer authorizer = await this._authorizerProvider.GetAuthorizer(currentUser);
            bool authorized = authorizer.HasApplicationPermission(ApplicationPermission.ADMIN_ACCESS);
            if (!authorized)
            {
                return new ForbidResult();
            }

            // Gather relevant data
            RecipeCategoryEntity recipeCategory = await this._recipeCategoryRepository.Get(id);
            if (recipeCategory == null)
            {
                return new NotFoundResult();
            }

            // Validate input data
            string actualSlug = recipeCategory.Name.Slugify();
            if (!string.Equals(actualSlug, slug, StringComparison.Ordinal))
            {
                string url = this.Url.Page("./RecipeEdit", new { id = id, slug = actualSlug });
                return new RedirectResult(url, permanent: true);
            }

            // Build result
            this.InitializeViewModel(recipeCategory, resetFormData: true);
            return new PageResult();
        }

        public async Task<IActionResult> OnPost(Guid id, string slug)
        {
            var currentUser = (UserIdentity)this.User;
            IAuthorizer authorizer = await this._authorizerProvider.GetAuthorizer(currentUser);
            bool authorized = authorizer.HasApplicationPermission(ApplicationPermission.ADMIN_ACCESS);
            if (!authorized)
            {
                return new ForbidResult();
            }

            // Gather relevant data
            RecipeCategoryEntity recipeCategory = await this._recipeCategoryRepository.Get(id);
            if (recipeCategory == null)
            {
                return new NotFoundResult();
            }

            // Validate input data
            if (!this.ModelState.IsValid)
            {
                this.InitializeViewModel(recipeCategory, resetFormData: false);
                return new PageResult();
            }

            // Update database
            RecipeCategoryEntity updatedRecipeCategory = EntityHelper.CloneEntity(recipeCategory);
            this.UpdateDataModel(updatedRecipeCategory);
            await this._recipeCategoryRepository.Update(updatedRecipeCategory);

            // Update assets
            if (this.NewPhotoFile != null)
            {
                using Stream assetStream = this.NewPhotoFile.OpenReadStream();
                await this._assetsDriver.UploadAsset($"images/recipes/categories/{id}/photo.png", assetStream);
            }

            // Build result
            string actualSlug = updatedRecipeCategory.Name.Slugify();
            string url = this.Url.Page("./Category", new { id = id, slug = actualSlug });
            return new RedirectResult(url, permanent: false);
        }

        private void InitializeViewModel(RecipeCategoryEntity recipeCategory, bool resetFormData)
        {
            this.Id = recipeCategory.Id;
            this.OriginalName = recipeCategory.Name;

            if (resetFormData)
            {
                this.Name = recipeCategory.Name;
                this.Position = recipeCategory.Position.ToString();
            }
        }

        private void UpdateDataModel(RecipeCategoryEntity recipeCategory)
        {
            recipeCategory.Name = this.Name;
            recipeCategory.Position = int.Parse(this.Position);
        }
    }
}
