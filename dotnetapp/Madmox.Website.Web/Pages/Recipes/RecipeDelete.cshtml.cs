using Madmox.Website.BusinessComponents.Authorization;
using Madmox.Website.Core.Identity;
using Madmox.Website.DataAccess.Assets;
using Madmox.Website.DataAccess.Database.Entities;
using Madmox.Website.DataAccess.Database.Repositories;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.RazorPages;
using System;
using System.Threading.Tasks;

namespace Madmox.Website.Web.Pages.Recipes
{
    [Authorize]
    public class RecipeDeleteModel : PageModel
    {
        private readonly IAssetsDriver _assetsDriver;
        private readonly IAuthorizerProvider _authorizerProvider;
        private readonly IRecipeRepository _recipeRepository;

        public Guid Id { get; set; }
        public Guid CategoryId { get; set; }
        public string Name { get; set; }

        public RecipeDeleteModel(
            IAssetsDriver assetsDriver,
            IAuthorizerProvider authorizerProvider,
            IRecipeRepository recipeRepository
        )
        {
            this._assetsDriver = assetsDriver;
            this._authorizerProvider = authorizerProvider;
            this._recipeRepository = recipeRepository;
        }

        public async Task<IActionResult> OnGet(Guid id, string slug)
        {
            var currentUser = (UserIdentity)this.User;
            IAuthorizer authorizer = await this._authorizerProvider.GetAuthorizer(currentUser);
            bool authorized = authorizer.HasApplicationPermission(ApplicationPermission.ADMIN_ACCESS);
            if (!authorized)
            {
                return new ForbidResult();
            }

            // Gather relevant data
            RecipeEntity recipe = await this._recipeRepository.Get(id);
            if (recipe == null)
            {
                return new NotFoundResult();
            }

            // Validate input data
            string actualSlug = recipe.Name.Slugify();
            if (!string.Equals(actualSlug, slug, StringComparison.Ordinal))
            {
                string redirectUrl = this.Url.Page("./RecipeDelete", new { id = id, slug = actualSlug });
                return new RedirectResult(redirectUrl, permanent: true);
            }

            // Build result
            this.InitializeViewModel(recipe);
            return new PageResult();
        }

        public async Task<IActionResult> OnPost(Guid id, string slug)
        {
            var currentUser = (UserIdentity)this.User;
            IAuthorizer authorizer = await this._authorizerProvider.GetAuthorizer(currentUser);
            bool authorized = authorizer.HasApplicationPermission(ApplicationPermission.ADMIN_ACCESS);
            if (!authorized)
            {
                return new ForbidResult();
            }

            // Gather relevant data
            RecipeEntity recipe = await this._recipeRepository.Get(id);
            if (recipe == null)
            {
                return new NotFoundResult();
            }

            // Validate input data
            if (!this.ModelState.IsValid)
            {
                this.InitializeViewModel(recipe);
                return new PageResult();
            }

            // Update database
            await this._recipeRepository.Delete(id);

            // Update assets
            await this._assetsDriver.DeleteAsset($"images/recipes/recipes/{id}/photo.png");

            // Build result
            string url = this.Url.Page("./Category", new { id = recipe.CategoryId });
            return new RedirectResult(url, permanent: false);
        }

        private void InitializeViewModel(RecipeEntity recipe)
        {
            this.Id = recipe.Id;
            this.CategoryId = recipe.CategoryId;
            this.Name = recipe.Name;
        }
    }
}
