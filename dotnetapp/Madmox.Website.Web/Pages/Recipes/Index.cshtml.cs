using Madmox.Website.BusinessComponents.Authorization;
using Madmox.Website.Core.Identity;
using Madmox.Website.Core.Types;
using Madmox.Website.DataAccess.Database.Entities;
using Madmox.Website.DataAccess.Database.Repositories;
using Madmox.Website.Web.ViewModels.Recipes.Index;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.RazorPages;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace Madmox.Website.Web.Pages.Recipes
{
    public class IndexModel : PageModel
    {
        private readonly IAuthorizerProvider _authorizerProvider;
        private readonly IRecipeCategoryRepository _recipeCategoryRepository;

        public List<CategoryVM> Categories { get; set; }
        public bool CanManage { get; set; }

        public IndexModel(
            IAuthorizerProvider authorizerProvider,
            IRecipeCategoryRepository recipeCategoryRepository
        )
        {
            this._authorizerProvider = authorizerProvider;
            this._recipeCategoryRepository = recipeCategoryRepository;
        }

        public async Task<IActionResult> OnGet()
        {
            // Gather relevant data
            EntitySet<RecipeCategoryEntity> categories = await this._recipeCategoryRepository.List();

            // Build result
            await this.InitializeViewModel(categories);
            return new PageResult();
        }

        private async Task InitializeViewModel(EntitySet<RecipeCategoryEntity> categories)
        {
            var currentUser = (UserIdentity)this.User;
            IAuthorizer authorizer = await this._authorizerProvider.GetAuthorizer(currentUser);

            this.Categories = new List<CategoryVM>();
            foreach (RecipeCategoryEntity category in categories)
            {
                var entry = new CategoryVM();
                entry.Map(category);
                this.Categories.Add(entry);
            }
            this.CanManage = authorizer.HasApplicationPermission(ApplicationPermission.ADMIN_ACCESS);
        }
    }
}
