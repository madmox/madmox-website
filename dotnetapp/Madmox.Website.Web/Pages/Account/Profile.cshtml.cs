using Madmox.Website.Core.Helpers;
using Madmox.Website.Core.Identity;
using Madmox.Website.DataAccess.Database.Entities;
using Madmox.Website.DataAccess.Database.Repositories;
using Madmox.Website.Web.Validation;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.RazorPages;
using System.ComponentModel.DataAnnotations;
using System.Threading.Tasks;

namespace Madmox.Website.Web.Pages.Account
{
    [Authorize]
    public class ProfileModel : PageModel
    {
        private readonly IApplicationUserRepository _applicationUserRepository;

        public bool DisplayUpdateConfirmation { get; set; }

        [BindProperty]
        [DataType(DataType.Text)]
        [Required(ErrorMessage = "Ce champ est requis.")]
        [StringLength(ValidationConstants.STRING_MAX_LENGTH, ErrorMessage = "Ce champ contient trop de caractères.")]
        public string LastName { get; set; }

        [BindProperty]
        [DataType(DataType.Text)]
        [Required(ErrorMessage = "Ce champ est requis.")]
        [StringLength(ValidationConstants.STRING_MAX_LENGTH, ErrorMessage = "Ce champ contient trop de caractères.")]
        public string FirstName { get; set; }

        public ProfileModel(
            IApplicationUserRepository applicationUserRepository
        )
        {
            this._applicationUserRepository = applicationUserRepository;
        }

        public async Task<IActionResult> OnGet([FromQuery(Name = "success")] bool success = false)
        {
            var currentUser = (UserIdentity)this.User;

            // Gather relevant data
            ApplicationUserEntity user = await this._applicationUserRepository.Get(currentUser.Id);

            // Build result
            this.InitializeViewModel(user, success, resetFormData: true);
            return new PageResult();
        }

        public async Task<IActionResult> OnPost()
        {
            var currentUser = (UserIdentity)this.User;

            // Gather relevant data
            ApplicationUserEntity user = await this._applicationUserRepository.Get(currentUser.Id);

            // Validate input data
            if (!this.ModelState.IsValid)
            {
                this.InitializeViewModel(user, success: false, resetFormData: false);
                return new PageResult();
            }

            // Update database
            ApplicationUserEntity updatedUser = EntityHelper.CloneEntity(user);
            this.UpdateDataModel(updatedUser);
            await this._applicationUserRepository.Update(updatedUser);

            // Build result
            string url = this.Url.Page("./Profile", new { success = true });
            return new RedirectResult(url, permanent: false);
        }

        private void InitializeViewModel(ApplicationUserEntity user, bool success, bool resetFormData)
        {
            this.DisplayUpdateConfirmation = success;

            if (resetFormData)
            {
                this.LastName = user.LastName;
                this.FirstName = user.FirstName;
            }
        }

        private void UpdateDataModel(ApplicationUserEntity updatedUser)
        {
            updatedUser.LastName = this.LastName;
            updatedUser.FirstName = this.FirstName;
        }
    }
}
