using Microsoft.AspNetCore.Authentication;
using Microsoft.AspNetCore.Authentication.Cookies;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.RazorPages;
using System;
using System.Threading.Tasks;

namespace Madmox.Website.Web.Pages.Account
{
    [Authorize]
    public class SignOutModel : PageModel
    {
        public SignOutModel()
        {
        }

        public IActionResult OnGet()
        {
            return new NotFoundResult();
        }

        public async Task<IActionResult> OnPost([FromQuery(Name = "ReturnUrl")] string returnUrl)
        {
            // Sign the user out
            await this.HttpContext.SignOutAsync(
                scheme: CookieAuthenticationDefaults.AuthenticationScheme
            );

            // Build result
            returnUrl = this.GetReturnUrl(returnUrl);
            return new RedirectResult(returnUrl, permanent: false);
        }

        private string GetReturnUrl(string returnUrl)
        {
            string indexUrl = this.Url.Page("/Index");

            if (string.IsNullOrEmpty(returnUrl) ||
                !returnUrl.StartsWith("/") ||
                !Uri.TryCreate(returnUrl, UriKind.Relative, out Uri parsedUrl))
            {
                return indexUrl;
            }
            else
            {
                return parsedUrl.OriginalString;
            }
        }
    }
}
