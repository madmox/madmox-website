using Madmox.Website.BusinessComponents.Authentication;
using Madmox.Website.BusinessComponents.PasswordManagement;
using Madmox.Website.Core.Identity;
using Madmox.Website.Core.Security;
using Madmox.Website.Web.Validation;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.RazorPages;
using System;
using System.ComponentModel.DataAnnotations;
using System.Threading.Tasks;

namespace Madmox.Website.Web.Pages.Account
{
    [Authorize]
    public class PasswordModel : PageModel
    {
        private readonly IPasswordManager _passwordManager;

        [BindProperty]
        [DataType(DataType.Password)]
        [Required(ErrorMessage = "Ce champ est requis.")]
        [StringLength(ValidationConstants.STRING_MAX_LENGTH, ErrorMessage = "Ce champ contient trop de caract�res.")]
        public string CurrentPassword { get; set; }

        [BindProperty]
        [DataType(DataType.Password)]
        [Required(ErrorMessage = "Ce champ est requis.")]
        [StringLength(SecurityConstants.PASSWORD_MAX_LENGTH, ErrorMessage = "Ce champ contient trop de caract�res.")]
        public string NewPassword { get; set; }

        [BindProperty]
        [DataType(DataType.Password)]
        [Required(ErrorMessage = "Ce champ est requis.")]
        [StringLength(SecurityConstants.PASSWORD_MAX_LENGTH, ErrorMessage = "Ce champ contient trop de caract�res.")]
        public string PasswordConfirmation { get; set; }

        public bool DisplayUpdateConfirmation { get; set; }

        public PasswordModel(
            IPasswordManager passwordManager
        )
        {
            this._passwordManager = passwordManager;
        }

        public IActionResult OnGet([FromQuery(Name = "success")] bool success = false)
        {
            // Build result
            this.DisplayUpdateConfirmation = success;
            return new PageResult();
        }

        public async Task<IActionResult> OnPost()
        {
            var currentUser = (UserIdentity)this.User;

            if (!this.ModelState.IsValid)
            {
                return new PageResult();
            }

            if (!string.Equals(this.NewPassword, this.PasswordConfirmation, StringComparison.Ordinal))
            {
                this.ModelState.AddModelError(nameof(this.PasswordConfirmation), "La confirmation ne correspond pas au mot de passe saisi.");
                return new PageResult();
            }

            try
            {
                await this._passwordManager.Update(currentUser, this.CurrentPassword, this.NewPassword, ipAddress: this.HttpContext.Connection.RemoteIpAddress?.ToString());
            }
            catch (TooManyFailedAttemptsException)
            {
                this.ModelState.AddModelError("", "Trop de tentatives de connexion �chou�es.");
                return new PageResult();
            }
            catch (InvalidCurrentPasswordException)
            {
                this.ModelState.AddModelError(nameof(this.CurrentPassword), "Mot de passe invalide.");
                return new PageResult();
            }
            catch (PasswordPolicyException)
            {
                this.ModelState.AddModelError(nameof(this.CurrentPassword), $"Le mot de passe doit faire entre {SecurityConstants.PASSWORD_MIN_LENGTH} et {SecurityConstants.PASSWORD_MAX_LENGTH} caract�res.");
                return new PageResult();
            }

            // Build result
            string url = this.Url.Page("./Password", new { success = true });
            return new RedirectResult(url, permanent: false);
        }
    }
}
