using Madmox.Website.BusinessComponents.Authentication;
using Madmox.Website.Core.Identity;
using Madmox.Website.Core.Validation;
using Madmox.Website.DataAccess.Database.Entities;
using Madmox.Website.Web.Validation;
using Microsoft.AspNetCore.Authentication;
using Microsoft.AspNetCore.Authentication.Cookies;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.RazorPages;
using System;
using System.ComponentModel.DataAnnotations;
using System.Threading.Tasks;

namespace Madmox.Website.Web.Pages.Account
{
    public class SignInModel : PageModel
    {
        private readonly IAuthenticator _authenticator;

        [BindProperty]
        [DataType(DataType.EmailAddress)]
        [Required(ErrorMessage = "Ce champ est requis.")]
        [StrictEmailAddress(ErrorMessage = "Le format de l'adresse est invalide.")]
        [StringLength(ValidationConstants.STRING_MAX_LENGTH, ErrorMessage = "Ce champ contient trop de caract�res.")]
        public string Email { get; set; }

        [BindProperty]
        [DataType(DataType.Password)]
        [Required(ErrorMessage = "Ce champ est requis.")]
        [StringLength(ValidationConstants.STRING_MAX_LENGTH, ErrorMessage = "Ce champ contient trop de caract�res.")]
        public string Password { get; set; }

        public SignInModel(
            IAuthenticator authenticator
        )
        {
            this._authenticator = authenticator;
        }

        public IActionResult OnGet([FromQuery(Name = "ReturnUrl")] string returnUrl)
        {
            var currentUser = (UserIdentity)this.User;
            if (currentUser.IsAuthenticated)
            {
                returnUrl = this.GetReturnUrl(returnUrl);
                return new RedirectResult(returnUrl, permanent: false);
            }

            return new PageResult();
        }

        public async Task<IActionResult> OnPost([FromQuery(Name = "ReturnUrl")] string returnUrl)
        {
            var currentUser = (UserIdentity)this.User;
            if (currentUser.IsAuthenticated)
            {
                returnUrl = this.GetReturnUrl(returnUrl);
                return new RedirectResult(returnUrl, permanent: false);
            }

            if (!this.ModelState.IsValid)
            {
                return new PageResult();
            }

            ApplicationUserEntity user;
            try
            {
                user = await this._authenticator.VerifyCredentials(this.Email, this.Password, ipAddress: this.HttpContext.Connection.RemoteIpAddress?.ToString());
            }
            catch (TooManyFailedAttemptsException)
            {
                this.ModelState.AddModelError("", "Trop de tentatives de connexion �chou�es.");
                return new PageResult();
            }

            if (user == null)
            {
                this.ModelState.AddModelError("", "Email et/ou mot de passe incorrect.");
                return new PageResult();
            }

            // Sign the user in
            var identity = new UserIdentity()
            {
                Id = user.Id,
                IsAuthenticated = true,
            };
            await this.HttpContext.SignInAsync(
                scheme: CookieAuthenticationDefaults.AuthenticationScheme,
                principal: identity.ToPrincipal(CookieAuthenticationDefaults.AuthenticationScheme)
            );

            // Build result
            returnUrl = this.GetReturnUrl(returnUrl);
            return new RedirectResult(returnUrl, permanent: false);
        }

        private string GetReturnUrl(string returnUrl)
        {
            string indexUrl = this.Url.Page("/Index");

            if (string.IsNullOrEmpty(returnUrl) ||
                !returnUrl.StartsWith("/") ||
                !Uri.TryCreate(returnUrl, UriKind.Relative, out Uri parsedUrl))
            {
                return indexUrl;
            }
            else
            {
                return parsedUrl.OriginalString;
            }
        }
    }
}
