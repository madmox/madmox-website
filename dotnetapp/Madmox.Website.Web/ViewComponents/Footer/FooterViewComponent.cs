﻿using Madmox.Website.Core.Git;
using Microsoft.AspNetCore.Mvc;

namespace Madmox.Website.Web.ViewComponents.Footer
{
    [ViewComponent(Name = "Footer")]
    public class FooterViewComponent : ViewComponent
    {
        private readonly IGitService _git;

        public FooterViewComponent(
            IGitService git
        )
        {
            this._git = git;
        }

        public IViewComponentResult Invoke()
        {
            string revisionHash = this._git.GetRevisionHash();

            var vm = new FooterViewModel()
            {
                RevisionHash = revisionHash
            };
            return this.View(vm);
        }
    }
}
