﻿using Madmox.Website.DataAccess.Database.Entities;
using System;

namespace Madmox.Website.Web.ViewComponents.Navigation
{
    public class RecipeCategoryDTO
    {
        public Guid Id { get; set; }
        public string Name { get; set; }
        public int Position { get; set; }

        public RecipeCategoryDTO()
        {
        }

        public RecipeCategoryDTO Map(RecipeCategoryEntity recipeCategory)
        {
            this.Id = recipeCategory.Id;
            this.Name = recipeCategory.Name;
            this.Position = recipeCategory.Position;

            return this;
        }
    }
}
