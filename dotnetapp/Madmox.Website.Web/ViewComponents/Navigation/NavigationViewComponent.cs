﻿using Madmox.Website.Core.Types;
using Madmox.Website.DataAccess.Database.Entities;
using Madmox.Website.DataAccess.Database.Repositories;
using Microsoft.AspNetCore.Mvc;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace Madmox.Website.Web.ViewComponents.Navigation
{
    [ViewComponent(Name = "Navigation")]
    public class NavigationViewComponent : ViewComponent
    {
        private readonly IRecipeCategoryRepository _recipeCategoryRepository;

        public NavigationViewComponent(
            IRecipeCategoryRepository recipeCategoryRepository
        )
        {
            this._recipeCategoryRepository = recipeCategoryRepository;
        }

        public async Task<IViewComponentResult> InvokeAsync()
        {
            string category = (string)this.ViewContext.ViewData[Constants.Navigation.CATEGORY];
            string subcategory = (string)this.ViewContext.ViewData[Constants.Navigation.SUBCATEGORY];
            var recipeCategories = new List<RecipeCategoryDTO>();
            if (category == "recipes")
            {
                EntitySet<RecipeCategoryEntity> dbRecipeCategories = await this._recipeCategoryRepository.List();
                foreach (RecipeCategoryEntity dbRecipeCategory in dbRecipeCategories)
                {
                    var entry = new RecipeCategoryDTO();
                    entry.Map(dbRecipeCategory);
                    recipeCategories.Add(entry);
                }
            }

            var vm = new NavigationResponseDTO()
            {
                Category = category,
                Subcategory = subcategory,
                RecipeCategories = recipeCategories
            };
            return this.View(vm);
        }
    }
}
