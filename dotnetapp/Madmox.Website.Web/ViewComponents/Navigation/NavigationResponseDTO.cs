﻿using System.Collections.Generic;

namespace Madmox.Website.Web.ViewComponents.Navigation
{
    public class NavigationResponseDTO
    {
        public string Category { get; set; }
        public string Subcategory { get; set; }
        public List<RecipeCategoryDTO> RecipeCategories { get; set; }

        public NavigationResponseDTO()
        {
        }
    }
}
