﻿using Madmox.Website.DataAccess.Assets;
using Madmox.Website.Web.Configuration;
using Madmox.Website.Web.Middlewares;
using Madmox.Website.Web.Security;
using Microsoft.AspNetCore.Authentication.Cookies;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.DataProtection;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Infrastructure;
using Microsoft.AspNetCore.Mvc.Routing;
using Microsoft.AspNetCore.Server.Kestrel.Core;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.FileProviders;
using Microsoft.Extensions.Hosting;
using Microsoft.Extensions.Options;
using Sentry.AspNetCore;
using System;
using System.IO;
using System.Security.Cryptography.X509Certificates;

WebApplicationBuilder builder = WebApplication.CreateBuilder(args);
ConfigureAppConfiguration(builder);
ConfigureServices(builder);
ConfigureLogging(builder);

WebApplication app = builder.Build();
ConfigurePipeline(app);

app.Run();

static void ConfigureAppConfiguration(WebApplicationBuilder builder)
{
    builder.Configuration.AddJsonFile("appsettings.local.json", optional: true);
}

static void ConfigureServices(WebApplicationBuilder builder)
{
    // .NET dependencies
    builder.Services.AddAuthentication(defaultScheme: CookieAuthenticationDefaults.AuthenticationScheme).AddCookie();
    builder.Services.AddHttpContextAccessor();
    builder.Services.AddRazorPages();
    builder.Services.AddSingleton<IActionContextAccessor, ActionContextAccessor>();
    builder.Services.AddScoped<IUrlHelper>(factory =>
    {
        IActionContextAccessor actionContextAccessor = factory.GetService<IActionContextAccessor>();
        return new UrlHelper(actionContextAccessor.ActionContext);
    });
    ConfigureDpApi(builder);

    builder.Services.Configure<KestrelServerOptions>(builder.Configuration.GetSection("Kestrel"));
    builder.Services.AddTransient<IConfigureOptions<CookieAuthenticationOptions>, ConfigureCookieAuthenticationOptions>();
    builder.Services.AddTransient<IConfigureOptions<ForwardedHeadersOptions>, ConfigureForwardedHeadersOptions>();
    builder.Services.AddTransient<IConfigureOptions<JsonOptions>, ConfigureJsonOptions>();

    // Third-party dependencies
    builder.Services.AddTransient<IConfigureOptions<SentryAspNetCoreOptions>, ConfigureSentryAspNetCoreOptions>();

    // Sub-projects dependencies
    builder.Services.SetupDataAccess();
    builder.Services.AddApplicationCoreServices(builder.Configuration);
    builder.Services.AddApplicationDataAccessServices(builder.Configuration);
    builder.Services.AddApplicationBusinessComponentsServices(builder.Configuration);

    // Project dependencies
    builder.Services.AddScoped<CustomCookieAuthenticationEvents>();
    builder.Services.AddGitRevisionHash();
    builder.Services.AddTransient<IConfigureOptions<GitRevisionHashOptions>, ConfigureGitRevisionHashOptions>();
}

static void ConfigureDpApi(WebApplicationBuilder builder)
{
    DpApiConfiguration securityConfig = builder.Configuration.GetSection("DpApi").Get<DpApiConfiguration>();
    if (!string.IsNullOrEmpty(securityConfig.KeysFolder))
    {
        var directoryInfos = new DirectoryInfo(securityConfig.KeysFolder);
        var certificate = new X509Certificate2(securityConfig.CertificatePath, securityConfig.CertificatePassword);
        builder.Services.AddDataProtection()
            .PersistKeysToFileSystem(directoryInfos)
            .ProtectKeysWithCertificate(certificate);
    }
}

static void ConfigureLogging(WebApplicationBuilder builder)
{
    builder.WebHost.UseSentry();
}

static void ConfigurePipeline(WebApplication app)
{
    AssetsConfiguration assetsConfig = app.Configuration.GetSection("Assets").Get<AssetsConfiguration>();
    if (string.IsNullOrEmpty(assetsConfig.AssetsFolder))
    {
        throw new InvalidOperationException($"Assets folder is not defined.");
    }
    string assetsFolder = Path.GetFullPath(assetsConfig.AssetsFolder);

    app.UseForwardedHeaders();

    if (app.Environment.IsDevelopment())
    {
        app.UseDeveloperExceptionPage();
    }
    else
    {
        app.UseExceptionHandler("/error");
        app.UseHsts();
    }

    app.UseStatusCodePagesWithReExecute(pathFormat: "/error/{0}");
    app.UseHttpsRedirection();
    app.UseStaticFiles(requestPath: "/static");
    app.UseStaticFiles(new StaticFileOptions()
    {
        RequestPath = "/assets",
        FileProvider = new PhysicalFileProvider(assetsFolder)
    });

    app.UseSecurityHeaders();
    app.UseGitRevisionHash();
    app.UseTransactionPerRequest();
    app.UseRouting();
    app.UseAuthentication();
    app.UseAuthorization();
    app.MapRazorPages();
}
