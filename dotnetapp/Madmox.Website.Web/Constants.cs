﻿using System.Globalization;

namespace Madmox.Website.Web
{
    public static class Constants
    {
        public static class Cultures
        {
            public static readonly CultureInfo FR_FR = CultureInfo.GetCultureInfo("fr-FR");
        }

        public static class Navigation
        {
            public const string CATEGORY = "navigation-category";
            public const string SUBCATEGORY = "navigation-subcategory";
        }

        public static class Security
        {
            public const string COOKIE_NAME = "auth";
        }
    }
}
