﻿using Madmox.Website.DataAccess.Database.Entities;
using System;

namespace Madmox.Website.Web.ViewModels.Recipes.Index
{
    public class CategoryVM
    {
        public Guid Id { get; set; }
        public string Name { get; set; }
        public int Position { get; set; }

        public CategoryVM()
        {
        }

        public CategoryVM Map(RecipeCategoryEntity category)
        {
            this.Id = category.Id;
            this.Name = category.Name;
            this.Position = category.Position;

            return this;
        }
    }
}
