﻿using Madmox.Website.DataAccess.Database.Entities;
using System;

namespace Madmox.Website.Web.ViewModels.Recipes.Category
{
    public class RecipeVM
    {
        public Guid Id { get; set; }
        public string Name { get; set; }

        public RecipeVM()
        {
        }

        public RecipeVM Map(RecipeEntity recipe)
        {
            this.Id = recipe.Id;
            this.Name = recipe.Name;

            return this;
        }
    }
}
