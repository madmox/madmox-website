﻿namespace Madmox.Website.Web.Security
{
    public class DpApiConfiguration
    {
        public string KeysFolder { get; set; }
        public string CertificatePath { get; set; }
        public string CertificatePassword { get; set; }
    }
}
