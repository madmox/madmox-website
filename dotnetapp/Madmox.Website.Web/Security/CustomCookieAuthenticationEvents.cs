﻿using Madmox.Website.Core.Identity;
using Madmox.Website.DataAccess.Database.Entities;
using Madmox.Website.DataAccess.Database.Repositories;
using Microsoft.AspNetCore.Authentication;
using Microsoft.AspNetCore.Authentication.Cookies;
using System.Threading.Tasks;

namespace Madmox.Website.Web.Security
{
    public class CustomCookieAuthenticationEvents : CookieAuthenticationEvents
    {
        private readonly IApplicationUserRepository _applicationUserRepository;

        public CustomCookieAuthenticationEvents(
            IApplicationUserRepository applicationUserRepository
        )
        {
            this._applicationUserRepository = applicationUserRepository;
        }

        public override async Task ValidatePrincipal(CookieValidatePrincipalContext context)
        {
            if (context.Principal.Identity.IsAuthenticated)
            {
                var user = (UserIdentity)context.Principal;

                // Get user from database
                ApplicationUserEntity dbUser = await this._applicationUserRepository.Get(user.Id);

                // Check if user still exists in database
                if (dbUser == null)
                {
                    context.RejectPrincipal();
                    await context.HttpContext.SignOutAsync(CookieAuthenticationDefaults.AuthenticationScheme);
                    return;
                }
            }
        }
    }
}
