﻿using Madmox.Website.DataAccess.Database.Infrastructure;
using Microsoft.AspNetCore.Http;
using Microsoft.Extensions.Logging;
using Npgsql;
using System.IO;
using System.Threading.Tasks;

namespace Madmox.Website.Web.Middlewares
{
    public class RequestTransactionMiddleware
    {
        private const int REQUEST_BUFFER_THRESHOLD = 2 * 1024 * 1024; // 2 MB
        private const int MAX_RETRIES = 5;

        private readonly RequestDelegate _next;

        public RequestTransactionMiddleware(
            RequestDelegate next
        )
        {
            this._next = next;
        }

        public async Task InvokeAsync(
            HttpContext context,
            IDatabaseDriver databaseDriver,
            ILogger<RequestTransactionMiddleware> logger
        )
        {
            // Because we run in SQL "serializable" transaction isolation level, SQL serialization failures
            // can happen when a row is updated by concurrent transactions (i.e. requests).
            // In this case, we want to rerun the failed action without changing its input.

            // Enable request buffering, otherwise the request body cannot be read multiple times.
            // Buffer threshold defines the maximum request size that can be buffered in memory,
            // after which it will be buffered in a temporary file.
            context.Request.EnableBuffering(bufferThreshold: REQUEST_BUFFER_THRESHOLD);

            // Replace the response body stream with a memory stream to prevent the response from being sent
            // to the client until after this middleware is fully executed.
            Stream originalResponseBodyStream = context.Response.Body;
            using var buffer = new MemoryStream();
            context.Response.Body = buffer;

            try
            {
                // Loop until either of the following happens:
                // - The request succeeds.
                // - The request fails with a SQL serialization failure and has been retried too many times.
                // - The request fails with an error that is not a SQL serialization failure.
                int attempt = 0;
                while (true)
                {
                    attempt++;

                    try
                    {
                        // Process request
                        await this._next(context);

                        // Try to commit transactions
                        await databaseDriver.Commit();

                        // Exit retry loop
                        break;
                    }
                    catch (PostgresException ex)
                    when (ex.SqlState == PostgresErrorCodes.SerializationFailure && attempt <= MAX_RETRIES)
                    {
                        logger.LogInformation("SQL serialization failure: retry MVC pipeline (retry {retriesCount} of {maxRetries}).", attempt, MAX_RETRIES);

                        // Rollback transactions
                        await databaseDriver.Rollback();

                        // Resets request and response streams so that they can be read/written again.
                        context.Request.Body.Seek(0, SeekOrigin.Begin);
                        context.Response.Body.SetLength(0);
                    }
                    catch
                    {
                        // Rollback transactions and rethrow
                        await databaseDriver.Rollback();
                        throw;
                    }
                }
            }
            finally
            {
                context.Response.Body = originalResponseBodyStream;
                buffer.Seek(0, SeekOrigin.Begin);
                await buffer.CopyToAsync(context.Response.Body);
            }
        }
    }
}
