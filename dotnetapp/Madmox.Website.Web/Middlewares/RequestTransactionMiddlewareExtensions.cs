﻿using Microsoft.AspNetCore.Builder;
using System;

namespace Madmox.Website.Web.Middlewares
{
    public static class RequestTransactionMiddlewareExtensions
    {
        public static IApplicationBuilder UseTransactionPerRequest(this IApplicationBuilder builder)
        {
            if (builder == null)
            {
                throw new ArgumentNullException(nameof(builder));
            }

            return builder.UseMiddleware<RequestTransactionMiddleware>();
        }
    }
}
