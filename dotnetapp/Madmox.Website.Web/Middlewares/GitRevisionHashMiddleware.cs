﻿using Microsoft.AspNetCore.Http;
using Microsoft.Extensions.Options;
using System;
using System.Threading.Tasks;

namespace Madmox.Website.Web.Middlewares
{
    public class GitRevisionHashMiddleware
    {
        private readonly RequestDelegate _next;
        private readonly GitRevisionHashOptions _options;

        public GitRevisionHashMiddleware(
            RequestDelegate next,
            IOptions<GitRevisionHashOptions> options
        )
        {
            this._next = next;
            this._options = options.Value;

            if (string.IsNullOrEmpty(this._options.HttpHeaderName))
            {
                throw new ArgumentException("HTTP header name is not set.");
            }

            if (string.IsNullOrEmpty(this._options.RevisionHash))
            {
                throw new ArgumentException("Revision hash is not set.");
            }
        }

        public async Task InvokeAsync(HttpContext context)
        {
            context.Response.OnStarting(() =>
            {
                context.Response.Headers[this._options.HttpHeaderName] = this._options.RevisionHash;
                return Task.CompletedTask;
            });

            await this._next(context);
        }
    }
}
