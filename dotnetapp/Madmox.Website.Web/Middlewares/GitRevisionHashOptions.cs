﻿namespace Madmox.Website.Web.Middlewares
{
    public class GitRevisionHashOptions
    {
        public string HttpHeaderName { get; set; }
        public string RevisionHash { get; set; }
    }
}
