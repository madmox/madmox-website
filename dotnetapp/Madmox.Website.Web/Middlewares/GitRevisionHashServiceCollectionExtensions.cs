﻿using Microsoft.Extensions.DependencyInjection;
using System;

namespace Madmox.Website.Web.Middlewares
{
    public static class GitRevisionHashServiceCollectionExtensions
    {
        public static IServiceCollection AddGitRevisionHash(
            this IServiceCollection serviceCollection
        )
        {
            if (serviceCollection == null)
            {
                throw new ArgumentNullException(nameof(serviceCollection));
            }

            _ = serviceCollection.AddOptions();

            return serviceCollection;
        }

        public static IServiceCollection AddGitRevisionHash(
            this IServiceCollection serviceCollection,
            Action<GitRevisionHashOptions> configure
        )
        {
            if (serviceCollection == null)
            {
                throw new ArgumentNullException(nameof(serviceCollection));
            }

            if (configure == null)
            {
                throw new ArgumentNullException(nameof(configure));
            }

            _ = serviceCollection.Configure(configure);

            return serviceCollection.AddGitRevisionHash();
        }
    }
}
