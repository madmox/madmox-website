﻿using Microsoft.AspNetCore.Builder;
using System;

namespace Madmox.Website.Web.Middlewares
{
    public static class SecurityHeadersMiddlewareExtensions
    {
        public static IApplicationBuilder UseSecurityHeaders(this IApplicationBuilder builder)
        {
            if (builder == null)
            {
                throw new ArgumentNullException(nameof(builder));
            }

            return builder.UseMiddleware<SecurityHeadersMiddleware>();
        }
    }
}
