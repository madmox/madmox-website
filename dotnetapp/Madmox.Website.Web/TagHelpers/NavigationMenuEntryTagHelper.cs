﻿using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Rendering;
using Microsoft.AspNetCore.Mvc.TagHelpers;
using Microsoft.AspNetCore.Mvc.ViewFeatures;
using Microsoft.AspNetCore.Razor.TagHelpers;
using System;
using System.Text.Encodings.Web;

namespace Madmox.Website.Web.TagHelpers
{
    [HtmlTargetElement("nav-entry", Attributes = "category, url")]
    public class NavigationMenuEntryTagHelper : TagHelper
    {
        private readonly IUrlHelper _urlHelper;

        [ViewContext]
        public ViewContext ViewContext { get; set; }

        [HtmlAttributeName("category")]
        public string Category { get; set; }

        [HtmlAttributeName("subcategory")]
        public string Subcategory { get; set; }

        [HtmlAttributeName("url")]
        public string Url { get; set; }

        public NavigationMenuEntryTagHelper(
            IUrlHelper urlHelper
        )
        {
            this._urlHelper = urlHelper;
        }

        public override void Process(TagHelperContext context, TagHelperOutput output)
        {
            output.TagName = "a";
            output.Attributes.Clear();

            string href = this._urlHelper.Content(this.Url);
            output.Attributes.SetAttribute("href", href);

            string category = (string)this.ViewContext.ViewData[Constants.Navigation.CATEGORY];
            string subcategory = (string)this.ViewContext.ViewData[Constants.Navigation.SUBCATEGORY];
            if ((string.IsNullOrEmpty(this.Category) || string.Equals(this.Category, category, StringComparison.Ordinal)) &&
                (string.IsNullOrEmpty(this.Subcategory) || string.Equals(this.Subcategory, subcategory, StringComparison.Ordinal)))
            {
                output.AddClass("active", HtmlEncoder.Default);
            }
        }
    }
}
