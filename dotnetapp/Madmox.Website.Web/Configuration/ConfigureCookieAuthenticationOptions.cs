﻿using Madmox.Website.Web.Security;
using Microsoft.AspNetCore.Authentication.Cookies;
using Microsoft.AspNetCore.Http;
using Microsoft.Extensions.Options;

namespace Madmox.Website.Web.Configuration
{
    public class ConfigureCookieAuthenticationOptions : IConfigureNamedOptions<CookieAuthenticationOptions>
    {
        public ConfigureCookieAuthenticationOptions()
        {
        }

        void IConfigureOptions<CookieAuthenticationOptions>.Configure(CookieAuthenticationOptions options)
        {
            ((IConfigureNamedOptions<CookieAuthenticationOptions>)this).Configure(Options.DefaultName, options);
        }

        void IConfigureNamedOptions<CookieAuthenticationOptions>.Configure(string name, CookieAuthenticationOptions options)
        {
            if (name == CookieAuthenticationDefaults.AuthenticationScheme)
            {
                options.LoginPath = "/account/signin";
                options.AccessDeniedPath = "/accessdenied";
                options.EventsType = typeof(CustomCookieAuthenticationEvents);

                options.Cookie.HttpOnly = true;
                options.Cookie.Name = Constants.Security.COOKIE_NAME;
                options.Cookie.SameSite = SameSiteMode.Strict;
                options.Cookie.SecurePolicy = CookieSecurePolicy.Always;
            }
        }
    }
}
