﻿using Madmox.Website.Core.Git;
using Madmox.Website.Web.Middlewares;
using Microsoft.Extensions.Options;

namespace Madmox.Website.Web.Configuration
{
    public class ConfigureGitRevisionHashOptions : IConfigureOptions<GitRevisionHashOptions>
    {
        private readonly IGitService _git;

        public ConfigureGitRevisionHashOptions(IGitService git)
        {
            this._git = git;
        }

        void IConfigureOptions<GitRevisionHashOptions>.Configure(GitRevisionHashOptions options)
        {
            options.HttpHeaderName = "X-GitRevisionHash";
            options.RevisionHash = this._git.GetRevisionHash();
        }
    }
}
