﻿using Madmox.Website.Core.Configuration;
using Madmox.Website.Core.Git;
using Microsoft.Extensions.Options;
using Sentry.AspNetCore;

namespace Madmox.Website.Web.Configuration
{
    public class ConfigureSentryAspNetCoreOptions : IConfigureOptions<SentryAspNetCoreOptions>
    {
        private readonly CoreConfiguration _coreConfig;
        private readonly IGitService _git;

        public ConfigureSentryAspNetCoreOptions(
            IOptions<CoreConfiguration> options,
            IGitService git
        )
        {
            this._coreConfig = options.Value;
            this._git = git;
        }

        void IConfigureOptions<SentryAspNetCoreOptions>.Configure(SentryAspNetCoreOptions options)
        {
            options.Environment = this._coreConfig.Environment;
            options.Release = this._git.GetRevisionHash();
        }
    }
}
