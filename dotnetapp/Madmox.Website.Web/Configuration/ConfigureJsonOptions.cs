﻿using Madmox.Website.Core.Json;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Options;

namespace Madmox.Website.Web.Configuration
{
    public class ConfigureJsonOptions : IConfigureOptions<JsonOptions>
    {
        public ConfigureJsonOptions()
        {
        }

        void IConfigureOptions<JsonOptions>.Configure(JsonOptions options)
        {
            JsonSettings.ConfigureOptions(options.JsonSerializerOptions);
        }
    }
}
