﻿using Microsoft.AspNetCore.Http;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.IO;
using System.Linq;

namespace Madmox.Website.Web.Validation
{
    /// <summary>
    /// An attribute validating a file's extension or a list thereof
    /// </summary>
    [AttributeUsage(AttributeTargets.Property | AttributeTargets.Field, AllowMultiple = false)]
    public sealed class AllowedFileExtensionsAttribute : ValidationAttribute
    {
        public string[] Extensions { get; }

        public AllowedFileExtensionsAttribute(string extensions)
        {
            this.Extensions = extensions.ToLowerInvariant().Split(',');
        }

        public override bool IsValid(object value)
        {
            if (value == null)
            {
                return true;
            }

            return value switch
            {
                IFormFile x => this.Verify(x),
                IEnumerable<IFormFile> _ => (value as IEnumerable<IFormFile>).All(x => this.Verify(x)),
                _ => false
            };
        }

        private bool Verify(IFormFile value)
        {
            string fileExtension = Path.GetExtension(value.FileName)?.TrimStart('.').ToLowerInvariant();
            if (!this.Extensions.Contains(fileExtension))
            {
                return false;
            }

            return true;
        }

        public override string FormatErrorMessage(string name)
        {
            return string.Format(this.ErrorMessage, string.Join(",", this.Extensions));
        }
    }
}
