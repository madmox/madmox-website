﻿namespace Madmox.Website.Web.Validation
{
    public static class ValidationConstants
    {
        public const int EMAIL_MAX_LENGTH = 1024;
        public const int STRING_MAX_LENGTH = 8192; // 2^13
        public const int BIG_STRING_MAX_LENGTH = 1_048_576; // 2^20
        public const int INLINE_ARRAY_MAX_LENGTH = 1024;
    }
}
