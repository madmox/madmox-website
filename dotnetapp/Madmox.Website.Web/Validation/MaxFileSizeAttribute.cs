﻿using Microsoft.AspNetCore.Http;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;

namespace Madmox.Website.Web.Validation
{
    /// <summary>
    /// An attribute validating a file's size or a list thereof
    /// </summary>
    [AttributeUsage(AttributeTargets.Property | AttributeTargets.Field, AllowMultiple = false)]
    public sealed class MaxFileSizeAttribute : ValidationAttribute
    {
        public int MaxSize { get; set; }

        public MaxFileSizeAttribute(int maxSize)
        {
            this.MaxSize = maxSize;
        }

        public override bool IsValid(object value)
        {
            if (value == null)
            {
                return true;
            }

            return value switch
            {
                IFormFile x => this.Verify(x),
                IEnumerable<IFormFile> _ => (value as IEnumerable<IFormFile>).All(x => this.Verify(x)),
                _ => false
            };
        }

        private bool Verify(IFormFile value)
        {
            if (value.Length > this.MaxSize)
            {
                return false;
            }

            return true;
        }

        public override string FormatErrorMessage(string name)
        {
            return string.Format(this.ErrorMessage, this.MaxSize);
        }
    }
}
