﻿using System;
using System.Text.Json;

namespace Madmox.Website.Core.Json
{
    public static class JsonValues
    {
        public static readonly JsonElement NULL = JsonSerializer.SerializeToElement((object)null, JsonSettings.DEFAULT_SERIALIZER_OPTIONS);
        public static readonly JsonElement EMPTY_OBJECT = JsonSerializer.SerializeToElement(new { }, JsonSettings.DEFAULT_SERIALIZER_OPTIONS);
        public static readonly JsonElement EMPTY_ARRAY = JsonSerializer.SerializeToElement(Array.Empty<object>(), JsonSettings.DEFAULT_SERIALIZER_OPTIONS);
        public static readonly JsonElement FALSE = JsonSerializer.SerializeToElement(false, JsonSettings.DEFAULT_SERIALIZER_OPTIONS);
        public static readonly JsonElement TRUE = JsonSerializer.SerializeToElement(true, JsonSettings.DEFAULT_SERIALIZER_OPTIONS);

        public static JsonElement FromObject<T>(T value)
        {
            return JsonSerializer.SerializeToElement(value, JsonSettings.DEFAULT_SERIALIZER_OPTIONS);
        }
    }
}
