﻿using NodaTime;
using NodaTime.Serialization.SystemTextJson;
using System.Text.Json;

namespace Madmox.Website.Core.Json
{
    public static class JsonSettings
    {
        public static JsonSerializerOptions DEFAULT_SERIALIZER_OPTIONS { get; }

        static JsonSettings()
        {
            DEFAULT_SERIALIZER_OPTIONS = new JsonSerializerOptions();
            ConfigureOptions(DEFAULT_SERIALIZER_OPTIONS);
        }

        public static void ConfigureOptions(JsonSerializerOptions options)
        {
            options.ConfigureForNodaTime(DateTimeZoneProviders.Tzdb);
        }
    }
}
