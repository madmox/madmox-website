﻿using Madmox.Website.Core.Helpers;
using System;
using System.Reflection;
using System.Text.Json;
using System.Text.Json.Serialization;

namespace Madmox.Website.Core.Json
{
    public class JsonStringEnumMemberConverter : JsonConverterFactory
    {
        public override bool CanConvert(Type typeToConvert)
        {
            return typeToConvert.IsEnum;
        }

        public override JsonConverter CreateConverter(Type typeToConvert, JsonSerializerOptions options)
        {
            Type converterType = typeof(JsonStringEnumMemberConverterImpl<>).MakeGenericType(typeToConvert);
            var converter = (JsonConverter)Activator.CreateInstance(
                converterType,
                BindingFlags.Instance | BindingFlags.Public,
                binder: null,
                args: null,
                culture: null
            );

            return converter;
        }

        private class JsonStringEnumMemberConverterImpl<T> : JsonConverter<T> where T : Enum
        {
            public override T Read(ref Utf8JsonReader reader, Type typeToConvert, JsonSerializerOptions options)
            {
                string strValue = reader.GetString();
                try
                {
                    return EnumHelper.Parse<T>(strValue);
                }
                catch (Exception ex)
                {
                    throw new JsonException(ex.Message, ex);
                }
            }

            public override void Write(Utf8JsonWriter writer, T value, JsonSerializerOptions options)
            {
                string strValue = value.ToEnumString();
                writer.WriteStringValue(strValue);
            }
        }
    }
}
