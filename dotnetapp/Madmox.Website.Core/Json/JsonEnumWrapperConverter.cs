﻿using Madmox.Website.Core.Helpers;
using Madmox.Website.Core.Types;
using System;
using System.Reflection;
using System.Text.Json;
using System.Text.Json.Serialization;

namespace Madmox.Website.Core.Json
{
    public class JsonEnumWrapperConverter : JsonConverterFactory
    {
        public override bool CanConvert(Type typeToConvert)
        {
            return
                typeToConvert.IsGenericType &&
                typeToConvert.GetGenericTypeDefinition() == typeof(EnumWrapper<>);
        }

        public override JsonConverter CreateConverter(Type typeToConvert, JsonSerializerOptions options)
        {
            Type enumType = typeToConvert.GetGenericArguments()[0];
            Type converterType = typeof(JsonEnumWrapperConverterImpl<>).MakeGenericType(enumType);
            var converter = (JsonConverter)Activator.CreateInstance(
                converterType,
                BindingFlags.Instance | BindingFlags.Public,
                binder: null,
                args: null,
                culture: null
            );

            return converter;
        }

        private class JsonEnumWrapperConverterImpl<T> : JsonConverter<EnumWrapper<T>> where T : Enum
        {
            public override EnumWrapper<T> Read(ref Utf8JsonReader reader, Type typeToConvert, JsonSerializerOptions options)
            {
                string strValue = reader.GetString();
                try
                {
                    return EnumHelper.Parse<T>(strValue);
                }
                catch (Exception ex)
                {
                    throw new JsonException(ex.Message, ex);
                }
            }

            public override void Write(Utf8JsonWriter writer, EnumWrapper<T> value, JsonSerializerOptions options)
            {
                string strValue = value.ToString();
                writer.WriteStringValue(strValue);
            }
        }
    }
}
