﻿using NodaTime;

namespace Madmox.Website.Core.Security
{
    public static class SecurityConstants
    {
        // Password policy
        public const int PASSWORD_MIN_LENGTH = 8;
        public const int PASSWORD_MAX_LENGTH = 128;

        // Failed signin attempts policy
        public static readonly Duration FAILED_VERIFY_CREDENTIALS_SCAN_PERIOD = Duration.FromMinutes(5);
        public const int FAILED_VERIFY_CREDENTIALS_THRESHOLD = 5;
    }
}
