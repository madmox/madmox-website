﻿using System;

namespace Madmox.Website.Core.Security
{
    public class PasswordPolicyException : Exception
    {
        public PasswordPolicyException(string message) : base(message)
        {
        }
    }
}
