﻿using Microsoft.AspNetCore.Cryptography.KeyDerivation;
using System;
using System.Security.Cryptography;

namespace Madmox.Website.Core.Security
{
    /// <summary>
    /// Adapted from https://github.com/dotnet/AspNetCore/blob/master/src/Identity/Extensions.Core/src/PasswordHasher.cs
    /// </summary>
    public class Pbkdf2PasswordHasher : IPasswordHasher
    {
        public Pbkdf2PasswordHasher()
        {
        }

        string IPasswordHasher.ComputePasswordHash(string password)
        {
            if (string.IsNullOrEmpty(password))
            {
                return "";
            }

            using var rng = RandomNumberGenerator.Create();

            const KeyDerivationPrf prf = KeyDerivationPrf.HMACSHA256;
            const int iterCount = 1000; // default for Rfc2898DeriveBytes
            const int subkeyLength = 256 / 8; // 256 bits
            const int saltLength = 128 / 8; // 128 bits

            byte[] salt = new byte[saltLength];
            rng.GetBytes(salt);
            byte[] subkey = KeyDerivation.Pbkdf2(password, salt, prf, iterCount, subkeyLength);

            // Concatenate PRF|ITER_COUNT|SALT_SIZE|SALT|SUBKEY to output
            byte[] outputBytes = new byte[(sizeof(uint) * 3) + saltLength + subkeyLength];
            WriteNetworkByteOrder(outputBytes, 0, (uint)prf);
            WriteNetworkByteOrder(outputBytes, sizeof(uint), (uint)iterCount);
            WriteNetworkByteOrder(outputBytes, sizeof(uint) * 2, (uint)saltLength);
            Buffer.BlockCopy(salt, 0, outputBytes, sizeof(uint) * 3, saltLength);
            Buffer.BlockCopy(subkey, 0, outputBytes, (sizeof(uint) * 3) + saltLength, subkeyLength);

            return Convert.ToBase64String(outputBytes);
        }

        bool IPasswordHasher.VerifyPassword(string password, string hashedPassword)
        {
            if (string.IsNullOrEmpty(password) || string.IsNullOrEmpty(hashedPassword))
            {
                return false;
            }

            try
            {
                byte[] hashedPasswordBytes = Convert.FromBase64String(hashedPassword);

                // Read header information
                var prf = (KeyDerivationPrf)ReadNetworkByteOrder(hashedPasswordBytes, 0);
                int iterCount = (int)ReadNetworkByteOrder(hashedPasswordBytes, sizeof(uint));
                int saltLength = (int)ReadNetworkByteOrder(hashedPasswordBytes, sizeof(uint) * 2);

                // Read the salt: must be >= 128 bits
                if (saltLength < 128 / 8)
                {
                    return false;
                }
                byte[] salt = new byte[saltLength];
                Buffer.BlockCopy(hashedPasswordBytes, sizeof(uint) * 3, salt, 0, saltLength);

                // Read the subkey (the rest of the payload): must be >= 128 bits
                int subkeyLength = hashedPasswordBytes.Length - (sizeof(uint) * 3) - saltLength;
                if (subkeyLength < 128 / 8)
                {
                    return false;
                }
                byte[] expectedSubkey = new byte[subkeyLength];
                Buffer.BlockCopy(hashedPasswordBytes, (sizeof(uint) * 3) + saltLength, expectedSubkey, 0, subkeyLength);

                // Hash the incoming password and verify it
                byte[] actualSubkey = KeyDerivation.Pbkdf2(password, salt, prf, iterCount, subkeyLength);
                return CryptographicOperations.FixedTimeEquals(actualSubkey, expectedSubkey);
            }
            catch
            {
                // This should never occur except in the case of a malformed payload, where
                // we might go off the end of the array. Regardless, a malformed payload
                // implies verification failed.
                return false;
            }
        }

        private static uint ReadNetworkByteOrder(byte[] buffer, int offset)
        {
            return ((uint)buffer[offset + 0] << 24)
                | ((uint)buffer[offset + 1] << 16)
                | ((uint)buffer[offset + 2] << 8)
                | ((uint)buffer[offset + 3]);
        }

        private static void WriteNetworkByteOrder(byte[] buffer, int offset, uint value)
        {
            buffer[offset + 0] = (byte)(value >> 24);
            buffer[offset + 1] = (byte)(value >> 16);
            buffer[offset + 2] = (byte)(value >> 8);
            buffer[offset + 3] = (byte)(value >> 0);
        }
    }
}
