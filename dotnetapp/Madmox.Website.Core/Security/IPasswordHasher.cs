﻿namespace Madmox.Website.Core.Security
{
    public interface IPasswordHasher
    {
        string ComputePasswordHash(string password);
        bool VerifyPassword(string password, string hashedPassword);
    }
}
