﻿namespace Madmox.Website.Core.Security
{
    public class PasswordPolicyManager : IPasswordPolicyManager
    {
        public PasswordPolicyManager()
        {
        }

        /// <summary>
        /// See OWASP password security requirements:
        /// https://github.com/OWASP/ASVS/blob/master/4.0/en/0x11-V2-Authentication.md#v21-password-security-requirements
        /// Note: some of the requirements are checked on the client side.
        /// </summary>
        void IPasswordPolicyManager.VerifyPasswordPolicy(string password)
        {
            if (password == null)
            {
                throw new PasswordPolicyException("Password is null");
            }

            // Password with a length less than 12 are considered weak
            if (password.Length < SecurityConstants.PASSWORD_MIN_LENGTH)
            {
                throw new PasswordPolicyException($"Password is too short (min: {SecurityConstants.PASSWORD_MIN_LENGTH} characters)");
            }

            // Prevent password length attacks by denying long passwords
            if (password.Length > SecurityConstants.PASSWORD_MAX_LENGTH)
            {
                throw new PasswordPolicyException($"Password is too long (max: {SecurityConstants.PASSWORD_MAX_LENGTH} characters)");
            }
        }
    }
}
