﻿namespace Madmox.Website.Core.Security
{
    public interface IPasswordPolicyManager
    {
        void VerifyPasswordPolicy(string password);
    }
}
