﻿using System;
using System.Collections;
using System.Collections.Concurrent;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;

namespace Madmox.Website.Core.Types
{
    public class EntitySet<T> : IReadOnlyList<T> where T : class
    {
        private readonly ConcurrentDictionary<string, Dictionary<KeyWrapper, EntitySet<T>>> _cache = new ConcurrentDictionary<string, Dictionary<KeyWrapper, EntitySet<T>>>();

        private IReadOnlyList<T> Values { get; }

        public EntitySet()
        {
            this.Values = new List<T>();
        }

        public EntitySet(IReadOnlyList<T> values)
        {
            this.Values = values;
        }

        public T GetByKey(object key, Expression<Func<T, object>> keySelector)
        {
            EntitySet<T> values = this.ListByKey(key, keySelector);
            return values.SingleOrDefault();
        }

        public EntitySet<T> ListByKey(object key, Expression<Func<T, object>> keySelector)
        {
            string cacheKey = this.GetPropertyPath(keySelector);

            Dictionary<KeyWrapper, EntitySet<T>> dic;
            if (!this._cache.ContainsKey(cacheKey))
            {
                Func<T, object> func = keySelector.Compile();
                dic = this.Values
                    .GroupBy(keySelector: x => new KeyWrapper(func(x)))
                    .ToDictionary(keySelector: x => x.Key, elementSelector: x => x.ToEntitySet());
                if (!this._cache.TryAdd(cacheKey, dic))
                {
                    dic = this._cache[cacheKey];
                }
            }
            else
            {
                dic = this._cache[cacheKey];
            }

            var wrappedKey = new KeyWrapper(key);
            return dic.GetValueOrDefault(wrappedKey, defaultValue: new EntitySet<T>());
        }

        public void ForEach(Action<T> action)
        {
            foreach (T value in this.Values)
            {
                action(value);
            }
        }

        private string GetPropertyPath(Expression<Func<T, object>> expression)
        {
            MemberExpression me;
            if (expression.Body.NodeType == ExpressionType.MemberAccess)
            {
                me = (MemberExpression)expression.Body;
            }
            else if (expression.Body.NodeType == ExpressionType.Convert || expression.Body.NodeType == ExpressionType.ConvertChecked)
            {
                var ue = (UnaryExpression)expression.Body;
                me = (MemberExpression)ue.Operand;
            }
            else
            {
                throw new ArgumentException("Invalid expression type");
            }

            return this.GetPropertyPath(me);
        }

        private string GetPropertyPath(MemberExpression expression)
        {
            if (expression.Expression.NodeType != ExpressionType.MemberAccess)
            {
                return $"{expression.Member.Name}";
            }

            string parentPropertyName = this.GetPropertyPath((MemberExpression)expression.Expression);
            return $"{parentPropertyName}.{expression.Member.Name}";
        }

        #region IReadOnlyList implementation

        IEnumerator IEnumerable.GetEnumerator() => this.Values.GetEnumerator();
        public IEnumerator<T> GetEnumerator() => this.Values.GetEnumerator();
        public int Count => this.Values.Count;
        public T this[int index] => this.Values[index];

        #endregion

        private struct KeyWrapper
        {
            public object Key { get; }

            public KeyWrapper(object key) : this()
            {
                this.Key = key;
            }

            public override string ToString() => this.Key?.ToString() ?? "NULL";

            public override bool Equals(object obj)
            {
                if (obj == null)
                {
                    return this.Key == null;
                }

                if (obj is not KeyWrapper wrapper)
                {
                    return false;
                }

                if (this.Key == null && wrapper.Key == null)
                {
                    return true;
                }
                else if (this.Key == null || wrapper.Key == null)
                {
                    return false;
                }

                return this.Key.Equals(wrapper.Key);
            }

            public override int GetHashCode() => this.Key?.GetHashCode() ?? 0;
        }
    }
}
