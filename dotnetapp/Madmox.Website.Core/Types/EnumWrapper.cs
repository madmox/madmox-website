﻿using System;

namespace Madmox.Website.Core.Types
{
    public struct EnumWrapper<T> : IEquatable<EnumWrapper<T>>, IEquatable<T>
        where T : Enum
    {
        private T Value { get; }

        private EnumWrapper(T value)
        {
            this.Value = value;
        }

        public override bool Equals(object other)
        {
            switch (other)
            {
                case T enumValue:
                    return this.Equals(enumValue);
                case EnumWrapper<T> wrapperValue:
                    return this.Equals(wrapperValue);
                default:
                    return false;
            }
        }

        public bool Equals(EnumWrapper<T> other)
        {
            return this.Value.Equals(other.Value);
        }

        public bool Equals(T other)
        {
            return this.Value.Equals(other);
        }

        public override int GetHashCode()
        {
            return this.Value.GetHashCode();
        }

        public override string ToString()
        {
            return this.Value.ToEnumString();
        }

        public static implicit operator EnumWrapper<T>(T value)
        {
            return new EnumWrapper<T>(value);
        }

        public static implicit operator T(EnumWrapper<T> value)
        {
            return value.Value;
        }

        public static bool operator ==(EnumWrapper<T> obj1, EnumWrapper<T> obj2)
        {
            return obj1.Equals(obj2);
        }

        public static bool operator !=(EnumWrapper<T> obj1, EnumWrapper<T> obj2)
        {
            return !obj1.Equals(obj2);
        }
    }
}
