﻿using Madmox.Website.Core.Json;
using System.Runtime.Serialization;
using System.Text.Json.Serialization;

namespace Madmox.Website.Core.Types
{
    [JsonConverter(typeof(JsonStringEnumMemberConverter))]
    public enum IpLogAction
    {
        [EnumMember(Value = "VERIFY_IDENTITY")]
        VERIFY_IDENTITY,

        [EnumMember(Value = "VERIFY_CREDENTIALS")]
        VERIFY_CREDENTIALS,
    }
}
