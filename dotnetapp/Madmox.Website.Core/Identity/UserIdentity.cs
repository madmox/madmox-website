﻿using System;
using System.Collections.Generic;
using System.Security.Claims;

namespace Madmox.Website.Core.Identity
{
    public class UserIdentity
    {
        public const string ADMIN_ROLE = "admin";

        public Guid Id { get; set; }
        public bool IsAuthenticated { get; set; }

        public UserIdentity()
        {
        }

        public static explicit operator UserIdentity(ClaimsPrincipal principal)
        {
            var user = new UserIdentity();

            user.Id = Guid.TryParse(principal.Identity.Name, out Guid id) ? id : Guid.Empty;
            user.IsAuthenticated = principal.Identity.IsAuthenticated;

            return user;
        }

        public ClaimsPrincipal ToPrincipal(string authenticationType)
        {
            var claims = new List<Claim>()
            {
                new Claim(ClaimTypes.Name, this.Id.ToString()),
            };
            var identity = new ClaimsIdentity(claims, authenticationType);
            return new ClaimsPrincipal(identity);
        }
    }
}
