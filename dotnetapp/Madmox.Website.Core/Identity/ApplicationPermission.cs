﻿using Madmox.Website.Core.Json;
using System.Runtime.Serialization;
using System.Text.Json.Serialization;

namespace Madmox.Website.Core.Identity
{
    [JsonConverter(typeof(JsonStringEnumMemberConverter))]
    public enum ApplicationPermission
    {
        [EnumMember(Value = "ADMIN_ACCESS")]
        ADMIN_ACCESS,
    }
}
