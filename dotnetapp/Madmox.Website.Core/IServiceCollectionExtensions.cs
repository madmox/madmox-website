﻿using Madmox.Website.Core.Configuration;
using Madmox.Website.Core.Git;
using Madmox.Website.Core.Security;
using Microsoft.Extensions.Configuration;

namespace Microsoft.Extensions.DependencyInjection
{
    public static class IServiceCollectionExtensions
    {
        public static IServiceCollection AddApplicationCoreServices(this IServiceCollection @this, IConfiguration config)
        {
            // Configuration
            @this.Configure<CoreConfiguration>(config.GetSection("Core"));

            // Git
            @this.AddSingleton<IGitService, GitService>();

            // Security
            @this.AddSingleton<IPasswordHasher, Pbkdf2PasswordHasher>();
            @this.AddSingleton<IPasswordPolicyManager, PasswordPolicyManager>();

            return @this;
        }
    }
}
