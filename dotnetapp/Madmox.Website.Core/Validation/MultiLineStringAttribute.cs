﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;

namespace Madmox.Website.Core.Validation
{
    /// <summary>
    /// An attribute validating a multiline string or a list thereof
    /// </summary>
    [AttributeUsage(AttributeTargets.Property | AttributeTargets.Field, AllowMultiple = false)]
    public sealed class MultiLineStringAttribute : ValidationAttribute
    {
        public int MinLineCount { get; set; } = 0;
        public int MaxLineCount { get; set; } = int.MaxValue;
        public int MaxLineLength { get; set; } = int.MaxValue;

        public MultiLineStringAttribute()
        {
        }

        public override bool IsValid(object value)
        {
            if (value == null)
            {
                return true;
            }

            return value switch
            {
                string x => this.Verify(x),
                IEnumerable<string> _ => (value as IEnumerable<string>).All(x => this.Verify(x)),
                _ => false
            };
        }

        private bool Verify(string value)
        {
            string[] parsedValue = value.Split(new string[] { Environment.NewLine }, StringSplitOptions.RemoveEmptyEntries);

            if (parsedValue.Length < this.MinLineCount || parsedValue.Length > this.MaxLineCount)
            {
                return false;
            }

            if (parsedValue.Any(x => x.Length > this.MaxLineLength))
            {
                return false;
            }

            return true;
        }

        public override string FormatErrorMessage(string name)
        {
            return string.Format(this.ErrorMessage, this.MinLineCount, this.MaxLineCount, this.MaxLineLength);
        }
    }
}
