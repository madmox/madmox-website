﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Net.Mail;
using System.Text.RegularExpressions;

namespace Madmox.Website.Core.Validation
{
    /// <summary>
    /// An attribute validating an email address or a list thereof.
    /// </summary>
    [AttributeUsage(AttributeTargets.Property | AttributeTargets.Field, AllowMultiple = false)]
    public sealed class StrictEmailAddressAttribute : ValidationAttribute
    {
        private const string EMAIL_VALIDATION_REGEX = @"^[a-zA-Z0-9.\-_]{1,}@[a-zA-Z0-9.-]{2,}\.[a-zA-Z]{2,}$";

        public StrictEmailAddressAttribute()
        {
        }

        public override bool IsValid(object value)
        {
            if (value == null)
            {
                return true;
            }

            return value switch
            {
                string x => Verify(x),
                IEnumerable<string> _ => (value as IEnumerable<string>).All(x => Verify(x)),
                _ => false
            };
        }

        private static bool Verify(string value)
        {
            if (value == null)
            {
                return true;
            }

            if (!Regex.IsMatch(value, EMAIL_VALIDATION_REGEX))
            {
                return false;
            }

            try
            {
                var addr = new MailAddress(value);
                return addr.Address == value;
            }
            catch
            {
                return false;
            }
        }
    }
}
