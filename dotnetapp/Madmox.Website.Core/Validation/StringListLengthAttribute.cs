﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace Madmox.Website.Core.Validation
{
    /// <summary>
    /// An attribute validating the length of each string in a list of strings
    /// </summary>
    [AttributeUsage(AttributeTargets.Property | AttributeTargets.Field, AllowMultiple = false)]
    public sealed class StringListLengthAttribute : ValidationAttribute
    {
        public int MaximumLength { get; }
        public int MinimumLength { get; set; }

        public StringListLengthAttribute(int maximumLength)
        {
            this.MaximumLength = maximumLength;
        }

        public override bool IsValid(object value)
        {
            if (value == null)
            {
                return true;
            }

            var values = (IEnumerable<string>)value;
            foreach (string s in values)
            {
                if (s != null && (s.Length < this.MinimumLength || s.Length > this.MaximumLength))
                {
                    return false;
                }
            }

            return true;
        }
    }
}
