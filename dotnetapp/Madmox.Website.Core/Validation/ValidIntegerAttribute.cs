﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;

namespace Madmox.Website.Core.Validation
{
    /// <summary>
    /// An attribute validating an integer or a list thereof
    /// </summary>
    [AttributeUsage(AttributeTargets.Property | AttributeTargets.Field, AllowMultiple = false)]
    public sealed class ValidIntegerAttribute : ValidationAttribute
    {
        public int MinValue { get; set; } = int.MinValue;
        public int MaxValue { get; set; } = int.MaxValue;

        public ValidIntegerAttribute()
        {
        }

        public override bool IsValid(object value)
        {
            if (value == null)
            {
                return true;
            }

            return value switch
            {
                string x => this.Verify(x),
                IEnumerable<string> _ => (value as IEnumerable<string>).All(x => this.Verify(x)),
                _ => false
            };
        }

        private bool Verify(string value)
        {
            if (!int.TryParse(value, out int parsedInt))
            {
                return false;
            }

            if (parsedInt < this.MinValue || parsedInt > this.MaxValue)
            {
                return false;
            }

            return true;
        }

        public override string FormatErrorMessage(string name)
        {
            return string.Format(this.ErrorMessage, this.MinValue, this.MaxValue);
        }
    }
}
