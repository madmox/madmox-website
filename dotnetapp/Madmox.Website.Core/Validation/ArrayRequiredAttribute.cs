﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;

namespace Madmox.Website.Core.Validation
{
    /// <summary>
    /// An attribute validating that each value in an array is not null, and that the array itself is not null.
    /// </summary>
    [AttributeUsage(AttributeTargets.Property | AttributeTargets.Field, AllowMultiple = false)]
    public sealed class ArrayRequiredAttribute : ValidationAttribute
    {
        public ArrayRequiredAttribute()
        {
        }

        public override bool IsValid(object value)
        {
            if (value == null)
            {
                return false;
            }

            if (value is IEnumerable<object> genericEnumerable)
            {
                return genericEnumerable.All(x => x != null);
            }
            else if (value is IEnumerable nonGenericEnumerable)
            {
                return nonGenericEnumerable.Cast<object>().All(x => x != null);
            }
            else
            {
                throw new ArgumentException("The parameter does not implement IEnumerable.", nameof(value));
            }
        }
    }
}
