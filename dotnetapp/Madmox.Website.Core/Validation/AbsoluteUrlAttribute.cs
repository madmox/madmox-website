﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;

namespace Madmox.Website.Core.Validation
{
    /// <summary>
    /// An attribute validating an Uri or a list thereof
    /// </summary>
    [AttributeUsage(AttributeTargets.Property | AttributeTargets.Field, AllowMultiple = false)]
    public sealed class AbsoluteUrlAttribute : ValidationAttribute
    {
        public AbsoluteUrlAttribute()
        {
        }

        public override bool IsValid(object value)
        {
            if (value == null)
            {
                return true;
            }

            return value switch
            {
                Uri x => Verify(x),
                IEnumerable<Uri> _ => (value as IEnumerable<Uri>).All(x => Verify(x)),
                _ => false
            };
        }

        private static bool Verify(Uri value)
        {
            return value.IsAbsoluteUri;
        }
    }
}
