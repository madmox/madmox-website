﻿using NodaTime.Text;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;

namespace Madmox.Website.Core.Validation
{
    /// <summary>
    /// An attribute validating a duration or a list thereof
    /// </summary>
    [AttributeUsage(AttributeTargets.Property | AttributeTargets.Field, AllowMultiple = false)]
    public sealed class ValidDurationAttribute : ValidationAttribute
    {
        public ValidDurationAttribute()
        {
        }

        public override bool IsValid(object value)
        {
            if (value == null)
            {
                return true;
            }

            return value switch
            {
                string x => Verify(x),
                IEnumerable<string> _ => (value as IEnumerable<string>).All(x => Verify(x)),
                _ => false
            };
        }

        private static bool Verify(string value)
        {
            return DurationPattern.Roundtrip.Parse(value).Success;
        }

        public override string FormatErrorMessage(string name)
        {
            return string.Format(this.ErrorMessage, DurationPattern.Roundtrip.PatternText);
        }
    }
}
