﻿using Madmox.Website.Core.Helpers;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;

namespace Madmox.Website.Core.Validation
{
    /// <summary>
    /// An attribute validating a base64 string or a list thereof
    /// </summary>
    [AttributeUsage(AttributeTargets.Property | AttributeTargets.Field, AllowMultiple = false)]
    public sealed class Base64StringAttribute : ValidationAttribute
    {
        public bool IsBase64Url { get; set; } = false;
        public int MinBytes { get; set; } = -1;
        public int MaxBytes { get; set; } = -1;

        public Base64StringAttribute()
        {
        }

        public override bool IsValid(object value)
        {
            if (value == null)
            {
                return true;
            }

            return value switch
            {
                string x => this.Verify(x),
                IEnumerable<string> _ => (value as IEnumerable<string>).All(x => this.Verify(x)),
                _ => false
            };
        }

        private bool Verify(string value)
        {
            byte[] b;
            if (this.IsBase64Url)
            {
                if (!Base64Helper.TryParseFromBase64Url(value, out b))
                {
                    return false;
                }
            }
            else
            {
                if (!Base64Helper.TryParseFromBase64(value, out b))
                {
                    return false;
                }
            }

            return
                (this.MinBytes < 0 || b.Length >= this.MinBytes) &&
                (this.MaxBytes < 0 || b.Length <= this.MaxBytes);
        }
    }
}
