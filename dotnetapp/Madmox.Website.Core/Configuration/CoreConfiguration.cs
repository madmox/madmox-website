﻿using System;

namespace Madmox.Website.Core.Configuration
{
    public class CoreConfiguration
    {
        public string Environment { get; set; }
        public Uri WebsiteBaseUrl { get; set; }
    }
}
