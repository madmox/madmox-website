﻿using System.IO;
using System.Reflection;

namespace Madmox.Website.Core.Git
{
    public class GitService : IGitService
    {
        private string _revisionHash = null;
        private readonly object _lock = new object();

        public GitService()
        {
        }

        string IGitService.GetRevisionHash()
        {
            if (this._revisionHash == null)
            {
                lock (this._lock)
                {
                    if (this._revisionHash == null)
                    {
                        var executingAssembly = Assembly.GetExecutingAssembly();
                        using Stream stream = executingAssembly.GetManifestResourceStream("Madmox.Website.Core." + "version.txt");
                        using var reader = new StreamReader(stream);
                        this._revisionHash = reader.ReadLine();
                    }
                }
            }
            return this._revisionHash;
        }
    }
}
