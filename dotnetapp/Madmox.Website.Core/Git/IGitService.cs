﻿namespace Madmox.Website.Core.Git
{
    public interface IGitService
    {
        string GetRevisionHash();
    }
}
