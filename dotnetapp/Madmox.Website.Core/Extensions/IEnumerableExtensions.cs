﻿using Madmox.Website.Core.Types;
using System.Collections.Generic;
using System.Linq;

namespace System
{
    public static class IEnumerableExtensions
    {
        public static EntitySet<T> ToEntitySet<T>(this IEnumerable<T> source) where T : class => new(source.ToList());
    }
}
