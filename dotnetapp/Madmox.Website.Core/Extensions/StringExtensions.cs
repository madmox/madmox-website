﻿using Slugify;

namespace System
{
    public static class StringExtensions
    {
        public static string Format(this string value, params object[] args)
        {
            return string.Format(value, args);
        }

        public static string Slugify(this string value)
        {
            var helper = new SlugHelper();
            return helper.GenerateSlug(value);
        }
    }
}
