﻿using System.Linq;
using System.Runtime.Serialization;

namespace System
{
    public static class EnumExtensions
    {
        public static string ToEnumString(this Enum value)
        {
            Type enumType = value.GetType();
            string name = Enum.GetName(enumType, value);
            if (name == null)
            {
                throw new ArgumentOutOfRangeException($"The value {value} does not exist in the enum {enumType.FullName}");
            }

            var enumMemberAttribute = (EnumMemberAttribute)enumType
                .GetField(name)
                .GetCustomAttributes(typeof(EnumMemberAttribute), inherit: true)
                .FirstOrDefault();
            return enumMemberAttribute?.Value ?? name;
        }
    }
}
