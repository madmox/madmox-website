﻿using Flurl;

namespace System
{
    public static class UriExtensions
    {
        public static Uri AppendPathSegment(this Uri uri, string pathSegment)
        {
            Url flurl = uri?.ToString().AppendPathSegment(pathSegment);
            return flurl != null ? new Uri(flurl.ToString()) : null;
        }

        public static Uri SetQueryParam(this Uri uri, string parameterName, string parameterValue)
        {
            Url flurl = uri?.ToString().SetQueryParam(parameterName, parameterValue);
            return flurl != null ? new Uri(flurl.ToString()) : null;
        }

        public static Uri SetQueryParams(this Uri uri, object values)
        {
            Url flurl = uri?.ToString().SetQueryParams(values);
            return flurl != null ? new Uri(flurl.ToString()) : null;
        }

        public static Uri SetPathAndQuery(this Uri uri, string pathAndQuery)
        {
            return new Uri(uri, pathAndQuery);
        }
    }
}
