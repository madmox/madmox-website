﻿namespace NodaTime
{
    public static class NodaTimeExtensions
    {
        public static Instant TruncateToMillisecond(this Instant instant)
        {
            return Instant.FromUnixTimeMilliseconds(instant.ToUnixTimeMilliseconds());
        }

        public static LocalDateTime TruncateToMillisecond(this LocalDateTime date)
        {
            return new LocalDateTime(date.Year, date.Month, date.Day, date.Hour, date.Minute, date.Second, date.Millisecond);
        }

        public static ZonedDateTime AtClosestValid(this DateTimeZone tz, LocalDateTime local)
        {
            return tz.ResolveLocal(local, resolver: mapping =>
            {
                if (mapping.Count == 0)
                {
                    // Handles the case where the specified local date is skipped
                    // in the time zone by using the next valid date.
                    return mapping.LateInterval.Start.InZone(mapping.Zone);
                }
                else
                {
                    // Otherwise, use the last available result (single if unambiguous).
                    return mapping.Last();
                }
            });
        }
    }
}
