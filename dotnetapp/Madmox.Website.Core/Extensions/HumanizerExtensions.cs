﻿using Humanizer.Localisation;
using NodaTime;
using System.Globalization;

namespace Humanizer
{
    public static class HumanizerExtensions
    {
        public static string Humanize(
            this Duration value,
            int precision = 1, CultureInfo culture = null,
            TimeUnit maxUnit = TimeUnit.Week, TimeUnit minUnit = TimeUnit.Millisecond,
            string collectionSeparator = ", ", bool toWords = false
        )
        {
            return value.ToTimeSpan().Humanize(precision, culture, maxUnit, minUnit, collectionSeparator, toWords);
        }
    }
}
