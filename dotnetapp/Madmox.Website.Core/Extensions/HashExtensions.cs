﻿using System.Security.Cryptography;
using System.Text;

namespace System
{
    public static class HashExtensions
    {
        /// <summary>
        /// Creates a SHA256 hash of the specified input.
        /// </summary>
        /// <param name="input">The input.</param>
        /// <returns>A hash</returns>
        public static string Sha256(this string input)
        {
            if (string.IsNullOrEmpty(input))
            {
                return string.Empty;
            }

            using var sha = SHA256.Create();
            byte[] bytes = Encoding.UTF8.GetBytes(input);
            byte[] hash = sha.ComputeHash(bytes);

            return Convert.ToBase64String(hash);
        }

        /// <summary>
        /// Creates a SHA256 hash of the specified input.
        /// </summary>
        /// <param name="input">The input.</param>
        /// <returns>A hash.</returns>
        public static byte[] Sha256(this byte[] input)
        {
            if (input == null)
            {
                return null;
            }

            using var sha = SHA256.Create();
            return sha.ComputeHash(input);
        }

        /// <summary>
        /// Equivalent of PHP's hash_hmac using SHA256 algorithm.
        /// </summary>
        /// <param name="input">Message to be hashed.</param>
        /// <param name="secret">Shared secret key used for generating the HMAC variant of the message digest.</param>
        /// <returns>The hashed string, output as lowercase hexits.</returns>
        public static string HmacSha256(this string input, string secret)
        {
            byte[] key = Encoding.UTF8.GetBytes(secret);
            byte[] inputAsBytes = Encoding.UTF8.GetBytes(input);
            var hash = new HMACSHA256(key);
            byte[] hashBytes = hash.ComputeHash(inputAsBytes);
            return BitConverter.ToString(hashBytes).Replace("-", "").ToLower();
        }

        /// <summary>
        /// Perform HMAC hash on the input, using the SHA256 algotithm.
        /// </summary>
        /// <param name="input">Data to be hashed.</param>
        /// <param name="secret">Key used for generating the HMAC variant of the message digest.</param>
        /// <returns>The hashed data, output as binary.</returns>
        public static byte[] HmacSha256(this byte[] input, byte[] key)
        {
            var hash = new HMACSHA256(key);
            return hash.ComputeHash(input);
        }
    }
}
