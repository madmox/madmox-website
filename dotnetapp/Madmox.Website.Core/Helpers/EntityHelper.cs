﻿using Madmox.Website.Core.Json;
using System.Text.Json;

namespace Madmox.Website.Core.Helpers
{
    public static class EntityHelper
    {
        public static T CloneEntity<T>(T entity)
        {
            string json = JsonSerializer.Serialize(entity, JsonSettings.DEFAULT_SERIALIZER_OPTIONS);
            return JsonSerializer.Deserialize<T>(json, JsonSettings.DEFAULT_SERIALIZER_OPTIONS);
        }
    }
}
