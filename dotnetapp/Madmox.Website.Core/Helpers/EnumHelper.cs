﻿using System;
using System.Linq;

namespace Madmox.Website.Core.Helpers
{
    public static class EnumHelper
    {
        public static T Parse<T>(string input) where T : Enum
        {
            if (string.IsNullOrEmpty(input))
            {
                throw new ArgumentException("Value is null or empty", nameof(input));
            }

            if (EnumHelper.TryParse(input, out T parsedValue))
            {
                return parsedValue;
            }
            else
            {
                throw new ArgumentOutOfRangeException($"Cannot parse value {input} as enum type {typeof(T).FullName}");
            }
        }

        public static bool TryParse<T>(string input, out T result) where T : Enum
        {
            if (!string.IsNullOrEmpty(input))
            {
                foreach (T value in Enum.GetValues(typeof(T)).Cast<T>())
                {
                    string valueStr = value.ToEnumString();
                    if (string.Equals(input, valueStr, StringComparison.Ordinal))
                    {
                        result = value;
                        return true;
                    }
                }
            }

            result = default;
            return false;
        }
    }
}
