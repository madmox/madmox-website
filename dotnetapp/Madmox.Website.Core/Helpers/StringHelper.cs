﻿using System;
using System.Text;

namespace Madmox.Website.Core.Helpers
{
    public static class StringHelper
    {
        private static readonly Random RNG = new();

        public static string GenerateRandomString(string alphabet, int length)
        {
            if (string.IsNullOrEmpty(alphabet))
            {
                throw new ArgumentNullException(nameof(alphabet));
            }
            if (length < 0)
            {
                throw new ArgumentException("Value must be greater than or equal to 0.", nameof(length));
            }

            int nbSymbols = alphabet.Length;

            var sb = new StringBuilder("");
            for (int i = 0; i < length; i++)
            {
                int rnd = RNG.Next(0, nbSymbols - 1);
                char symbol = alphabet[rnd];
                sb.Append(symbol);
            }

            return sb.ToString();
        }
    }
}
