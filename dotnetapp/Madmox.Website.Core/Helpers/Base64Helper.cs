﻿using System;

namespace Madmox.Website.Core.Helpers
{
    public static class Base64Helper
    {
        public static string ToBase64Url(byte[] value)
        {
            if (value == null)
            {
                throw new ArgumentNullException(nameof(value));
            }

            string base64 = Convert.ToBase64String(value);
            return base64.Replace('/', '_').Replace('+', '-');
        }

        public static byte[] FromBase64Url(string input)
        {
            if (input == null)
            {
                throw new ArgumentNullException(nameof(input));
            }

            // Add padding if needed
            switch (input.Length % 4)
            {
                case 2:
                    input += "==";
                    break;
                case 3:
                    input += "=";
                    break;
            }

            // Convert to standard base64 encoding
            input = input.Replace('_', '/');
            input = input.Replace('-', '+');

            // Parse as standard base64
            return Convert.FromBase64String(input);
        }

        public static bool TryParseFromBase64Url(string input, out byte[] value)
        {
            value = null;

            if (input == null)
            {
                return false;
            }

            // Add padding if needed
            switch (input.Length % 4)
            {
                case 2:
                    input += "==";
                    break;
                case 3:
                    input += "=";
                    break;
            }

            // Convert to standard base64 encoding
            input = input.Replace('_', '/');
            input = input.Replace('-', '+');

            // Parse as standard base64
            try
            {
                value = Convert.FromBase64String(input);
            }
            catch (FormatException)
            {
                return false;
            }

            return true;
        }

        public static bool TryParseFromBase64(string input, out byte[] value)
        {
            value = null;

            if (input == null)
            {
                return false;
            }

            // Add padding if needed
            switch (input.Length % 4)
            {
                case 2:
                    input += "==";
                    break;
                case 3:
                    input += "=";
                    break;
            }

            // Parse as standard base64
            try
            {
                value = Convert.FromBase64String(input);
            }
            catch (FormatException)
            {
                return false;
            }

            return true;
        }
    }
}
