﻿using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace Madmox.Website.Core.Helpers
{
    public static class ValidationHelper
    {
        public static void AddValidationResult(List<ValidationResult> validationResults, string errorMessage, params string[] memberNames)
        {
            var validationResult = new ValidationResult(errorMessage, memberNames);
            validationResults.Add(validationResult);
        }
    }
}
