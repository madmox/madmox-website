﻿using Madmox.Website.Core.Identity;

namespace Madmox.Website.IntegrationTests.Fakes
{
    public class TestUserProvider : ITestUserProvider
    {
        public TestUserProvider()
        {
        }

        UserIdentity ITestUserProvider.User { get; set; }
    }
}
