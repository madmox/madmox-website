﻿using Madmox.Website.Core.Identity;
using Microsoft.AspNetCore.Authentication;
using Microsoft.Extensions.Logging;
using Microsoft.Extensions.Options;
using System.Security.Claims;
using System.Text.Encodings.Web;
using System.Threading.Tasks;

namespace Madmox.Website.IntegrationTests.Fakes
{
    public class TestAuthHandler : AuthenticationHandler<AuthenticationSchemeOptions>
    {
        public const string AUTHENTICATION_SCHEME = "Test";

        private readonly ITestUserProvider _testUserProvider;

        public TestAuthHandler(
            IOptionsMonitor<AuthenticationSchemeOptions> options,
            ILoggerFactory logger,
            UrlEncoder encoder,
            ISystemClock clock,
            ITestUserProvider testUserProvider
        )
            : base(options, logger, encoder, clock)
        {
            this._testUserProvider = testUserProvider;
        }

        protected override Task<AuthenticateResult> HandleAuthenticateAsync()
        {
            AuthenticateResult result;

            UserIdentity user = this._testUserProvider.User;
            if (user.IsAuthenticated)
            {
                ClaimsPrincipal principal = user.ToPrincipal(AUTHENTICATION_SCHEME);
                var ticket = new AuthenticationTicket(principal, AUTHENTICATION_SCHEME);
                result = AuthenticateResult.Success(ticket);
            }
            else
            {
                result = AuthenticateResult.NoResult();
            }

            return Task.FromResult(result);
        }

        protected override Task HandleChallengeAsync(AuthenticationProperties properties)
        {
            this.Response.Redirect("/account/signin");

            return Task.CompletedTask;
        }

        protected override Task HandleForbiddenAsync(AuthenticationProperties properties)
        {
            this.Response.Redirect("/accessdenied");

            return Task.CompletedTask;
        }
    }
}
