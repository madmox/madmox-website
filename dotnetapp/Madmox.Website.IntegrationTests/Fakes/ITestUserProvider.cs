﻿using Madmox.Website.Core.Identity;

namespace Madmox.Website.IntegrationTests.Fakes
{
    public interface ITestUserProvider
    {
        UserIdentity User { get; set; }
    }
}
