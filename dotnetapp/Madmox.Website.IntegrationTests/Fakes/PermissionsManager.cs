﻿using Madmox.Website.BusinessComponents.Authorization;
using Madmox.Website.Core.Identity;
using System;
using System.Collections.Generic;

namespace Madmox.Website.IntegrationTests.Fakes
{
    public class PermissionsManager : IAdditionalPermissionsProvider, IPermissionsManager
    {
        private readonly Dictionary<string, List<ApplicationPermission>> _applicationPermissions = new();

        IEnumerable<ApplicationPermission> IAdditionalPermissionsProvider.ListApplicationPermissions(Guid applicationUserId)
        {
            string key = $"{applicationUserId}";
            return this._applicationPermissions.GetValueOrDefault(key, new List<ApplicationPermission>());
        }

        void IPermissionsManager.AddApplicationPermission(Guid applicationUserId, ApplicationPermission permission)
        {
            string key = $"{applicationUserId}";
            List<ApplicationPermission> permissions;
            if (this._applicationPermissions.ContainsKey(key))
            {
                permissions = this._applicationPermissions[key];
            }
            else
            {
                permissions = new List<ApplicationPermission>();
                this._applicationPermissions.Add(key, permissions);
            }
            permissions.Add(permission);
        }
    }
}
