﻿using Madmox.Website.Core.Identity;
using System;

namespace Madmox.Website.IntegrationTests.Fakes
{
    public interface IPermissionsManager
    {
        void AddApplicationPermission(Guid applicationUserId, ApplicationPermission permission);
    }
}
