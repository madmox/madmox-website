﻿using RichardSzalay.MockHttp;
using System.Net.Http;

namespace Madmox.Website.IntegrationTests.Fakes
{
    public class MockedHttpClientFactory : IHttpClientFactory
    {
        private readonly MockHttpMessageHandler _handler;

        public MockedHttpClientFactory(MockHttpMessageHandler handler)
        {
            this._handler = handler;
        }

        HttpClient IHttpClientFactory.CreateClient(string name)
        {
            return this._handler.ToHttpClient();
        }
    }
}
