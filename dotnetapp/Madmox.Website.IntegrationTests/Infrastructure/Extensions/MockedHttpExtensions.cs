﻿using Madmox.Website.Core.Json;
using System.Net;
using System.Text.Json;

namespace RichardSzalay.MockHttp
{
    public static class MockedHttpExtensions
    {
        public static MockedRequest RespondWithJson(this MockedRequest source, HttpStatusCode statusCode, object content, string contentType)
        {
            string json = JsonSerializer.Serialize(content, JsonSettings.DEFAULT_SERIALIZER_OPTIONS);
            return source.Respond(statusCode, contentType, json);
        }
    }
}
