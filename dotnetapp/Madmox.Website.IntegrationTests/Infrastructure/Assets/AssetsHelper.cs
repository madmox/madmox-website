﻿using Madmox.Website.IntegrationTests.Infrastructure.Config;
using System;
using System.IO;
using System.Threading.Tasks;

namespace Madmox.Website.IntegrationTests.Infrastructure.Assets
{
    public class AssetsHelper
    {
        private static AssetsHelper _instance;
        private static readonly object _lock = new();

        public static AssetsHelper Instance
        {
            get
            {
                if (_instance == null)
                {
                    lock (_lock)
                    {
                        if (_instance == null)
                        {
                            _instance = new AssetsHelper();
                        }
                    }
                }
                return _instance;
            }
        }

        public Task Initialize()
        {
            string assetsFolder = ValidateAssetsFolder();
            if (Directory.Exists(assetsFolder))
            {
                Directory.Delete(assetsFolder, recursive: true);
            }
            Directory.CreateDirectory(assetsFolder);

            return Task.CompletedTask;
        }

        public Task CleanUp()
        {
            string assetsFolder = ValidateAssetsFolder();
            if (Directory.Exists(assetsFolder))
            {
                Directory.Delete(assetsFolder, recursive: true);
            }

            return Task.CompletedTask;
        }

        public Task Upsert(string path, byte[] content)
        {
            string assetsFolder = ValidateAssetsFolder();
            string assetFullPath = Path.GetFullPath(Path.Combine(assetsFolder, path));
            string assetFolder = Path.GetDirectoryName(assetFullPath);
            Directory.CreateDirectory(assetFolder);
            File.WriteAllBytes(assetFullPath, content);
            return Task.CompletedTask;
        }

        public Task<bool> Exists(string path)
        {
            string assetsFolder = ValidateAssetsFolder();
            string assetFullPath = Path.GetFullPath(Path.Combine(assetsFolder, path));
            bool exists = File.Exists(assetFullPath);
            return Task.FromResult(exists);
        }

        private static string ValidateAssetsFolder()
        {
            if (string.IsNullOrEmpty(ConfigHelper.Instance.AssetsConfiguration.AssetsFolder))
            {
                throw new InvalidOperationException($"Assets folder is not defined.");
            }

            return Path.GetFullPath(ConfigHelper.Instance.AssetsConfiguration.AssetsFolder);
        }
    }
}
