﻿using System;
using System.Collections.Generic;
using System.IO;

namespace Madmox.Website.IntegrationTests.Infrastructure.Data
{
    public class DataHelper
    {
        private static DataHelper _instance;
        private static readonly object _lock = new();

        public static DataHelper Instance
        {
            get
            {
                if (_instance == null)
                {
                    lock (_lock)
                    {
                        if (_instance == null)
                        {
                            _instance = new DataHelper();
                        }
                    }
                }
                return _instance;
            }
        }

        public string LoadContent(string path, Dictionary<string, object> parameters)
        {
            string dataFilePath = $"Data/{path}";
            if (!File.Exists(dataFilePath))
            {
                throw new ArgumentException($"File does not exist: {dataFilePath}");
            }

            string content = File.ReadAllText(dataFilePath);
            if (parameters != null)
            {
                foreach (KeyValuePair<string, object> kvp in parameters)
                {
                    content = content.Replace($"{{{{{kvp.Key}}}}}", kvp.Value.ToString());
                }
            }
            return content;
        }
    }
}
