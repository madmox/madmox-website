﻿using Madmox.Website.Core.Configuration;
using Madmox.Website.DataAccess.Assets;
using Madmox.Website.DataAccess.Database;
using Microsoft.Extensions.Configuration;

namespace Madmox.Website.IntegrationTests.Infrastructure.Config
{
    public class ConfigHelper
    {
        private static ConfigHelper _instance;
        private static readonly object _lock = new();

        public static ConfigHelper Instance
        {
            get
            {
                if (_instance == null)
                {
                    lock (_lock)
                    {
                        if (_instance == null)
                        {
                            _instance = new ConfigHelper();
                        }
                    }
                }
                return _instance;
            }
        }

        private readonly IConfigurationRoot _config;

        private ConfigHelper()
        {
            this._config = new ConfigurationBuilder()
                .AddJsonFile("appsettings.json", optional: false)
                .AddJsonFile("appsettings.tests.json", optional: true)
                .AddEnvironmentVariables()
                .Build();
        }

        public T GetConfiguration<T>(string sectionName) => this._config.GetSection(sectionName).Get<T>();

        public CoreConfiguration CoreConfiguration => this._config.GetSection("Core").Get<CoreConfiguration>();
        public AssetsConfiguration AssetsConfiguration => this._config.GetSection("Assets").Get<AssetsConfiguration>();
        public DatabaseConfiguration DatabaseConfiguration => this._config.GetSection("Database").Get<DatabaseConfiguration>();
    }
}
