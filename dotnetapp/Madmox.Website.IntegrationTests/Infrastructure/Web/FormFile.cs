﻿namespace Madmox.Website.IntegrationTests.Infrastructure.Web
{
    public class FormFile
    {
        public string FileName { get; }
        public byte[] FileContent { get; }

        public FormFile(string fileName, byte[] fileContent)
        {
            this.FileName = fileName;
            this.FileContent = fileContent;
        }
    }
}
