﻿using AngleSharp;
using AngleSharp.Dom;
using AngleSharp.Html.Dom;
using AngleSharp.Io;
using System.Collections.Generic;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Threading;
using System.Threading.Tasks;

namespace Madmox.Website.IntegrationTests.Infrastructure.Web
{
    public static class HtmlHelper
    {
        public static async Task<IHtmlDocument> GetDocumentAsync(HttpResponseMessage response)
        {
            string content = await response.Content.ReadAsStringAsync();
            IDocument document = await BrowsingContext.New().OpenAsync(htmlResponse => ResponseFactory(htmlResponse, response, content), CancellationToken.None);
            return (IHtmlDocument)document;
        }

        private static void ResponseFactory(VirtualResponse htmlResponse, HttpResponseMessage response, string content)
        {
            htmlResponse.Address(response.RequestMessage.RequestUri).Status(response.StatusCode);
            MapHeaders(response.Headers, htmlResponse);
            MapHeaders(response.Content.Headers, htmlResponse);
            htmlResponse.Content(content);
        }

        private static void MapHeaders(HttpHeaders headers, VirtualResponse htmlResponse)
        {
            foreach (KeyValuePair<string, IEnumerable<string>> header in headers)
            {
                foreach (string value in header.Value)
                {
                    htmlResponse.Header(header.Key, value);
                }
            }
        }
    }
}
