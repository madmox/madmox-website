﻿using Dapper;
using Madmox.Website.IntegrationTests.Infrastructure.Config;
using Npgsql;
using System;
using System.IO;
using System.Linq;
using System.Threading.Tasks;

namespace Madmox.Website.IntegrationTests.Infrastructure.Database
{
    public class DatabaseHelper
    {
        private static DatabaseHelper _instance;
        private static readonly object _lock = new();

        public static DatabaseHelper Instance
        {
            get
            {
                if (_instance == null)
                {
                    lock (_lock)
                    {
                        if (_instance == null)
                        {
                            _instance = new DatabaseHelper();
                        }
                    }
                }
                return _instance;
            }
        }

        public NpgsqlConnection Connection { get; private set; }

        private DatabaseHelper()
        {
            var builder = new NpgsqlConnectionStringBuilder()
            {
                Host = ConfigHelper.Instance.DatabaseConfiguration.Host,
                Port = ConfigHelper.Instance.DatabaseConfiguration.Port,
                Database = ConfigHelper.Instance.DatabaseConfiguration.Name,
                Username = ConfigHelper.Instance.DatabaseConfiguration.Username,
                Password = ConfigHelper.Instance.DatabaseConfiguration.Password,
                SslMode = SslMode.Disable
            };
            this.Connection = new NpgsqlConnection(builder.ConnectionString);
        }

        public async Task Initialize()
        {
            await this.Connection.OpenAsync();
            await this.ResetSchema();
            await this.RunScripts("./DatabaseScripts");
        }

        public async Task Finalize()
        {
            await this.ResetSchema();
            await this.Connection.DisposeAsync();
        }

        public Task InitializeConnection()
        {
            // This is needed because npgsql statically caches PostgreSQL types when the first SQL connection is opened.
            // https://www.npgsql.org/doc/types/enums_and_composites.html#mapping-your-clr-types
            // Note that your PostgreSQL enum and composites types (some_enum_type and some_composite_type in the sample above)
            // must be defined in your database before the first connection is created (see CREATE TYPE). If you're creating
            // PostgreSQL types within your program, call NpgsqlConnection.ReloadTypes() to make sure Npgsql becomes properly
            // aware of them.
            this.Connection.ReloadTypes();

            return Task.CompletedTask;
        }

        public async Task ResetData()
        {
            // Truncate all tables
            string sql = @"
DO
$func$
BEGIN
    EXECUTE (
        SELECT COALESCE('TRUNCATE TABLE ' || string_agg(oid::regclass::text, ', ') || ' CASCADE', 'SELECT ''dummy''')
        FROM pg_class
        WHERE relkind = 'r' AND relnamespace = 'public'::regnamespace
    );
END
$func$;
";
            await this.Connection.ExecuteAsync(sql);
        }

        private async Task ResetSchema()
        {
            // Drop/recreate public schema
            string sql = @"
DROP SCHEMA public CASCADE;
CREATE SCHEMA public;
GRANT ALL ON SCHEMA public TO postgres;
GRANT ALL ON SCHEMA public TO public;
";
            await this.Connection.ExecuteAsync(sql);
        }

        private async Task RunScripts(string scriptFolderPath)
        {
            if (!Directory.Exists(scriptFolderPath))
            {
                throw new ArgumentException($"Directory does not exist: {scriptFolderPath}");
            }

            var scripts = Directory.GetFiles(scriptFolderPath, searchPattern: "*.sql", searchOption: SearchOption.AllDirectories)
                .OrderBy(s => s)
                .Select(File.ReadAllText)
                .ToList();
            string script = string.Join(Environment.NewLine, scripts);
            if (!string.IsNullOrEmpty(script))
            {
                await this.Connection.ExecuteAsync(script);
            }
        }
    }
}
