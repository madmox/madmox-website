﻿using Dapper;
using Madmox.Website.Core.Types;
using Madmox.Website.DataAccess.Database.Entities;
using Madmox.Website.DataAccess.Database.Infrastructure;
using System;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace Madmox.Website.IntegrationTests.Infrastructure.Database
{
    public static class RecipeHelper
    {
        public static async Task<RecipeEntity> Get(Guid id)
        {
            var p = new { Id = id };
            string sql = $@"
SELECT
    {SqlHelper<RecipeEntity>.SelectedColumns}
FROM {SqlHelper<RecipeEntity>.Table}
WHERE
    {SqlHelper<RecipeEntity>.PrefixedColumn(x => x.Id)} = @{nameof(p.Id)}
;
";
            return await DatabaseHelper.Instance.Connection.QuerySingleOrDefaultAsync<RecipeEntity>(sql, p);
        }

        public static async Task<EntitySet<RecipeEntity>> List()
        {
            var p = new { };
            string sql = $@"
SELECT
    {SqlHelper<RecipeEntity>.SelectedColumns}
FROM {SqlHelper<RecipeEntity>.Table}
;
";
            IEnumerable<RecipeEntity> result = await DatabaseHelper.Instance.Connection.QueryAsync<RecipeEntity>(sql, p);
            return result.ToEntitySet();
        }

        public static async Task Insert(RecipeEntity entity)
        {
            string sql = $@"
INSERT INTO {SqlHelper<RecipeEntity>.Table} (
    {SqlHelper<RecipeEntity>.Columns}
) VALUES (
    {SqlHelper<RecipeEntity>.Parameters}
)
;
";
            await DatabaseHelper.Instance.Connection.ExecuteAsync(sql, entity);
        }
    }
}
