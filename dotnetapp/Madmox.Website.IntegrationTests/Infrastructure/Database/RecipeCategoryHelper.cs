﻿using Dapper;
using Madmox.Website.Core.Types;
using Madmox.Website.DataAccess.Database.Entities;
using Madmox.Website.DataAccess.Database.Infrastructure;
using System;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace Madmox.Website.IntegrationTests.Infrastructure.Database
{
    public static class RecipeCategoryHelper
    {
        public static async Task<RecipeCategoryEntity> Get(Guid id)
        {
            var p = new { Id = id };
            string sql = $@"
SELECT
    {SqlHelper<RecipeCategoryEntity>.SelectedColumns}
FROM {SqlHelper<RecipeCategoryEntity>.Table}
WHERE
    {SqlHelper<RecipeCategoryEntity>.PrefixedColumn(x => x.Id)} = @{nameof(p.Id)}
;
";
            return await DatabaseHelper.Instance.Connection.QuerySingleOrDefaultAsync<RecipeCategoryEntity>(sql, p);
        }

        public static async Task<EntitySet<RecipeCategoryEntity>> List()
        {
            var p = new { };
            string sql = $@"
SELECT
    {SqlHelper<RecipeCategoryEntity>.SelectedColumns}
FROM {SqlHelper<RecipeCategoryEntity>.Table}
;
";
            IEnumerable<RecipeCategoryEntity> result = await DatabaseHelper.Instance.Connection.QueryAsync<RecipeCategoryEntity>(sql, p);
            return result.ToEntitySet();
        }

        public static async Task Insert(RecipeCategoryEntity entity)
        {
            string sql = $@"
INSERT INTO {SqlHelper<RecipeCategoryEntity>.Table} (
    {SqlHelper<RecipeCategoryEntity>.Columns}
) VALUES (
    {SqlHelper<RecipeCategoryEntity>.Parameters}
)
;
";
            await DatabaseHelper.Instance.Connection.ExecuteAsync(sql, entity);
        }
    }
}
