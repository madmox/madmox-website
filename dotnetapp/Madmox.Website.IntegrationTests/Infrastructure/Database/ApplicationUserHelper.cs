﻿using Dapper;
using Madmox.Website.DataAccess.Database.Entities;
using Madmox.Website.DataAccess.Database.Infrastructure;
using System;
using System.Threading.Tasks;

namespace Madmox.Website.IntegrationTests.Infrastructure.Database
{
    public static class ApplicationUserHelper
    {
        public static async Task<ApplicationUserEntity> Get(Guid id)
        {
            var p = new { Id = id };
            string sql = $@"
SELECT
    {SqlHelper<ApplicationUserEntity>.SelectedColumns}
FROM {SqlHelper<ApplicationUserEntity>.Table}
WHERE
    {SqlHelper<ApplicationUserEntity>.PrefixedColumn(x => x.Id)} = @{nameof(p.Id)}
;
";
            return await DatabaseHelper.Instance.Connection.QuerySingleOrDefaultAsync<ApplicationUserEntity>(sql, p);
        }

        public static async Task Insert(ApplicationUserEntity entity)
        {
            string sql = $@"
INSERT INTO {SqlHelper<ApplicationUserEntity>.Table} (
    {SqlHelper<ApplicationUserEntity>.Columns}
) VALUES (
    {SqlHelper<ApplicationUserEntity>.Parameters}
)
;
";
            await DatabaseHelper.Instance.Connection.ExecuteAsync(sql, entity);
        }
    }
}
