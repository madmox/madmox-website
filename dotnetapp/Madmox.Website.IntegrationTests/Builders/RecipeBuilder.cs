﻿using Madmox.Website.DataAccess.Database.Entities;
using Madmox.Website.IntegrationTests.Infrastructure.Database;
using NodaTime;
using System;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace Madmox.Website.IntegrationTests.Builders
{
    public class RecipeBuilder
    {
        private readonly RecipeEntity _entity;

        private RecipeBuilder()
        {
            var id = Guid.NewGuid();
            Instant now = SystemClock.Instance.GetCurrentInstant();
            this._entity = new RecipeEntity()
            {
                Id = id,
                CategoryId = Guid.Empty,
                Name = $"{id}",
                NbPersons = 0,
                PreparationTime = Duration.Zero,
                TotalTime = Duration.Zero,
                Ingredients = new List<string>(),
                Tools = new List<string>(),
                Steps = new List<string>(),
                CreatedAt = now,
                UpdatedAt = now
            };
        }

        public static RecipeBuilder Start()
        {
            return new RecipeBuilder();
        }

        public async Task<RecipeEntity> Build(bool persist = true)
        {
            if (persist)
            {
                await RecipeHelper.Insert(this._entity);
            }
            return this._entity;
        }

        public RecipeBuilder WithCategory(RecipeCategoryEntity value)
        {
            this._entity.CategoryId = value.Id;
            return this;
        }

        public RecipeBuilder WithName(string value)
        {
            this._entity.Name = value;
            return this;
        }
    }
}
