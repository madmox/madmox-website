﻿using Madmox.Website.DataAccess.Database.Entities;
using Madmox.Website.IntegrationTests.Infrastructure.Database;
using NodaTime;
using System;
using System.Threading.Tasks;

namespace Madmox.Website.IntegrationTests.Builders
{
    public class RecipeCategoryBuilder
    {
        private readonly RecipeCategoryEntity _entity;

        private RecipeCategoryBuilder()
        {
            var id = Guid.NewGuid();
            Instant now = SystemClock.Instance.GetCurrentInstant();
            this._entity = new RecipeCategoryEntity()
            {
                Id = id,
                Name = $"{id}",
                Position = 0,
                CreatedAt = now,
                UpdatedAt = now
            };
        }

        public static RecipeCategoryBuilder Start()
        {
            return new RecipeCategoryBuilder();
        }

        public async Task<RecipeCategoryEntity> Build(bool persist = true)
        {
            if (persist)
            {
                await RecipeCategoryHelper.Insert(this._entity);
            }
            return this._entity;
        }

        public RecipeCategoryBuilder WithName(string value)
        {
            this._entity.Name = value;
            return this;
        }
    }
}
