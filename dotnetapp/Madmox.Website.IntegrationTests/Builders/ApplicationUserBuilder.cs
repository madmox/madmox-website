﻿using Madmox.Website.DataAccess.Database.Entities;
using Madmox.Website.IntegrationTests.Infrastructure.Database;
using NodaTime;
using System;
using System.Threading.Tasks;

namespace Madmox.Website.IntegrationTests.Builders
{
    public class ApplicationUserBuilder
    {
        private readonly ApplicationUserEntity _entity;

        private ApplicationUserBuilder()
        {
            var id = Guid.NewGuid();
            Instant now = SystemClock.Instance.GetCurrentInstant();
            this._entity = new ApplicationUserEntity()
            {
                Id = id,
                Email = $"{id}@example.com",
                IsPasswordClearText = false,
                Password = "",
                IsAdmin = false,
                LastName = "Doe",
                FirstName = "John",
                CreatedAt = now,
                UpdatedAt = now
            };
        }

        public static ApplicationUserBuilder Start()
        {
            return new ApplicationUserBuilder();
        }

        public async Task<ApplicationUserEntity> Build(bool persist = true)
        {
            if (persist)
            {
                await ApplicationUserHelper.Insert(this._entity);
            }
            return this._entity;
        }

        public ApplicationUserBuilder WithEmail(string value)
        {
            this._entity.Email = value;

            return this;
        }

        public ApplicationUserBuilder WithClearTextPassword(string value)
        {
            this._entity.IsPasswordClearText = true;
            this._entity.Password = value;

            return this;
        }

        public ApplicationUserBuilder IsAdmin(bool value)
        {
            this._entity.IsAdmin = value;

            return this;
        }
    }
}
