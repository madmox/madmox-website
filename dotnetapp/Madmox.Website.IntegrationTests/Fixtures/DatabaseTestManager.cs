﻿using Madmox.Website.IntegrationTests.Infrastructure.Assets;
using Madmox.Website.IntegrationTests.Infrastructure.Database;
using NUnit.Framework;
using System.Threading.Tasks;

namespace Madmox.Website.IntegrationTests.Fixtures
{
    [SetUpFixture]
    public class DatabaseTestManager
    {
        [OneTimeSetUp]
        public async Task OneTimeSetUp()
        {
            await DatabaseHelper.Instance.Initialize();
            await AssetsHelper.Instance.Initialize();
        }

        [OneTimeTearDown]
        public async Task OneTimeTearDown()
        {
            await AssetsHelper.Instance.CleanUp();
            await DatabaseHelper.Instance.Finalize();
        }
    }
}
