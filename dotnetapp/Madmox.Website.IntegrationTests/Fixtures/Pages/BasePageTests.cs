﻿using Flurl;
using Madmox.Website.BusinessComponents.Authorization;
using Madmox.Website.Core.Identity;
using Madmox.Website.DataAccess.Database.Entities;
using Madmox.Website.IntegrationTests.Fakes;
using Madmox.Website.IntegrationTests.Infrastructure.Assets;
using Madmox.Website.IntegrationTests.Infrastructure.Config;
using Madmox.Website.IntegrationTests.Infrastructure.Database;
using Madmox.Website.IntegrationTests.Infrastructure.Web;
using Microsoft.AspNetCore.Authentication;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.RazorPages;
using Microsoft.AspNetCore.Mvc.Testing;
using Microsoft.AspNetCore.TestHost;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using NodaTime;
using NUnit.Framework;
using RichardSzalay.MockHttp;
using System;
using System.Collections.Generic;
using System.IO;
using System.Net.Http;
using System.Threading.Tasks;

namespace Madmox.Website.IntegrationTests.Fixtures.Pages
{
    public abstract class BasePageTests
    {
        private WebApplicationFactory<Program> _factory;
        protected Instant TestStart { get; private set; }
        protected Instant CallStart { get; private set; }
        protected Instant CallEnd { get; private set; }
        protected HttpClient Client { get; private set; }
        protected MockHttpMessageHandler MockHttp { get; private set; }
        protected IPermissionsManager PermissionsManager { get; private set; }
        protected ITestUserProvider TestUserProvider { get; private set; }

        // DI helpers

        private void RegisterAllFakes(IServiceCollection services)
        {
            this.RegisterGlobalFakes(services);
            this.RegisterLocalFakes(services);
        }

        private void RegisterGlobalFakes(IServiceCollection services)
        {
            // Any HTTP call will be made using a fake HttpClient
            // (tests should not contact external APIs)
            this.MockHttp = new MockHttpMessageHandler();
            var httpClientFactory = new MockedHttpClientFactory(this.MockHttp);
            services.AddSingleton<IHttpClientFactory>(httpClientFactory);

            // Enable permissions injection
            var permissionsManager = new PermissionsManager();
            this.PermissionsManager = permissionsManager;
            services.AddSingleton<IAdditionalPermissionsProvider>(permissionsManager);

            // Test authentication handler
            var testUserProvider = new TestUserProvider();
            this.TestUserProvider = testUserProvider;
            services.AddSingleton<ITestUserProvider>(testUserProvider);
            services.AddAuthentication(TestAuthHandler.AUTHENTICATION_SCHEME)
                .AddScheme<AuthenticationSchemeOptions, TestAuthHandler>(TestAuthHandler.AUTHENTICATION_SCHEME, options => { });

            // Disable AntiForgery on all pages
            services.Configure<RazorPagesOptions>(x => x.Conventions.ConfigureFilter(new IgnoreAntiforgeryTokenAttribute()));
        }

        private void CleanUpGlobalFakes()
        {
            if (this.MockHttp != null)
            {
                this.MockHttp.Dispose();
                this.MockHttp = null;
            }
        }

        /// <summary>
        /// Registers fakes for a specific test class. The base implementation does nothing.
        /// </summary>
        /// <param name="services"></param>
        protected virtual void RegisterLocalFakes(IServiceCollection services) { }

        /// <summary>
        /// Additional configuration for the HttpClient instance
        /// </summary>
        protected virtual void ConfigureHttpClientOptions(WebApplicationFactoryClientOptions clientOptions) { }

        // Tests setup

        [SetUp]
        public async Task SetUpBaseHttpTests()
        {
            await AssetsHelper.Instance.Initialize();

            this.TestStart = NodaTime.SystemClock.Instance.GetCurrentInstant();
            this._factory = new WebApplicationFactory<Program>();
            var clientOptions = new WebApplicationFactoryClientOptions()
            {
                BaseAddress = ConfigHelper.Instance.CoreConfiguration.WebsiteBaseUrl,
                AllowAutoRedirect = false,
                HandleCookies = true,
            };
            this.ConfigureHttpClientOptions(clientOptions);
            this.Client = this._factory
                .WithWebHostBuilder(builder =>
                {
                    // Override config with test-specific appsettings
                    builder.ConfigureAppConfiguration((hostingContext, config) =>
                    {
                        // Clear all configuration sources defined by the web project
                        config.Sources.Clear();

                        // Set the config base directory to the test directory,
                        // otherwise it would default to the web project directory
                        config.SetBasePath(Directory.GetCurrentDirectory());

                        // Add test appsettings
                        config.AddJsonFile("appsettings.json", optional: false);
                        config.AddJsonFile("appsettings.tests.json", optional: true);
                    });

                    // Override services for tests
                    builder.ConfigureTestServices(this.RegisterAllFakes);
                })
                .CreateClient(clientOptions);
            this.SetAnonymousUser();

            await DatabaseHelper.Instance.InitializeConnection();
        }

        [TearDown]
        public async Task TearDownBaseHttpTests()
        {
            await DatabaseHelper.Instance.ResetData();

            this.SetAnonymousUser();
            if (this.Client != null)
            {
                this.Client.Dispose();
                this.Client = null;
            }
            if (this._factory != null)
            {
                this._factory.Dispose();
                this._factory = null;
            }
            this.CleanUpGlobalFakes();

            await AssetsHelper.Instance.CleanUp();
        }

        // HTTP-related helpers

        protected void SetAnonymousUser()
        {
            this.TestUserProvider.User = new UserIdentity()
            {
                Id = Guid.Empty,
                IsAuthenticated = false
            };
        }

        protected void SetCurrentUser(ApplicationUserEntity user)
        {
            this.TestUserProvider.User = new UserIdentity()
            {
                Id = user.Id,
                IsAuthenticated = true
            };
        }

        protected async Task<HttpResponseMessage> GetAsync(
            string path, object[] urlParams = null, Dictionary<string, object> queryStringParams = null
        )
        {
            return await this.SendAsync(HttpMethod.Get, path, urlParams, queryStringParams, formData: null, formFiles: null);
        }

        protected async Task<HttpResponseMessage> PostAsync(
            string path, object[] urlParams = null, Dictionary<string, object> queryStringParams = null,
            Dictionary<string, string> formData = null, Dictionary<string, FormFile> formFiles = null
        )
        {
            return await this.SendAsync(HttpMethod.Post, path, urlParams, queryStringParams, formData, formFiles);
        }

        private async Task<HttpResponseMessage> SendAsync(
            HttpMethod httpMethod, string path, object[] urlParams, Dictionary<string, object> queryStringParams,
            Dictionary<string, string> formData, Dictionary<string, FormFile> formFiles
        )
        {
            string url = GenerateUrl(path, urlParams, queryStringParams);
            using var request = new HttpRequestMessage(httpMethod, url);
            if (formFiles != null)
            {
                var multipartContent = new MultipartFormDataContent();
                foreach (KeyValuePair<string, FormFile> kvp in formFiles)
                {
                    multipartContent.Add(new ByteArrayContent(kvp.Value.FileContent), kvp.Key, kvp.Value.FileName);
                }
                if (formData != null)
                {
                    foreach (KeyValuePair<string, string> kvp in formData)
                    {
                        multipartContent.Add(new StringContent(kvp.Value), kvp.Key);
                    }
                }
                request.Content = multipartContent;
            }
            else if (formData != null)
            {
                request.Content = new FormUrlEncodedContent(formData);
            }

            this.CallStart = NodaTime.SystemClock.Instance.GetCurrentInstant();
            HttpResponseMessage response = await this.Client.SendAsync(request);
            this.CallEnd = NodaTime.SystemClock.Instance.GetCurrentInstant();

            return response;
        }

        private static string GenerateUrl(string path, object[] urlParams, Dictionary<string, object> queryStringParams)
        {
            urlParams ??= Array.Empty<object>();

            string result = string.Format(path, urlParams);

            if (queryStringParams != null)
            {
                result = result.SetQueryParams(queryStringParams);
            }

            return result;
        }
    }

}
