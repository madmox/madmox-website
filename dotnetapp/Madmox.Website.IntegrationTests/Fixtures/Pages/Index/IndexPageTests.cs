﻿using AngleSharp.Html.Dom;
using FakeItEasy;
using FluentAssertions;
using Madmox.Website.DataAccess.Database;
using Madmox.Website.DataAccess.Database.Entities;
using Madmox.Website.DataAccess.Database.Infrastructure;
using Madmox.Website.IntegrationTests.Builders;
using Madmox.Website.IntegrationTests.Infrastructure.Config;
using Madmox.Website.IntegrationTests.Infrastructure.Web;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Logging;
using Microsoft.Extensions.Options;
using NUnit.Framework;
using System.Net;
using System.Net.Http;
using System.Threading.Tasks;

namespace Madmox.Website.IntegrationTests.Fixtures.Pages.Index
{
    public class IndexPageTests : BasePageTests
    {
        private const string PAGE_URL = "/";

        private IDatabaseDriver _databaseDriver;

        protected override void RegisterLocalFakes(IServiceCollection services)
        {
            this._databaseDriver = A.Fake<IDatabaseDriver>();
            services.AddSingleton(this._databaseDriver);
        }

        [SetUp]
        public void SetUp()
        {
            IOptions<DatabaseConfiguration> options = Options.Create(ConfigHelper.Instance.DatabaseConfiguration);
            ILogger<DatabaseDriver> logger = A.Fake<ILogger<DatabaseDriver>>();
            IDatabaseDriver databaseDriver = new DatabaseDriver(logger, options);

            A.CallTo(() => this._databaseDriver.Commit())
                .ReturnsLazily(() => databaseDriver.Commit());
            A.CallTo(() => this._databaseDriver.Rollback())
                .ReturnsLazily(() => databaseDriver.Rollback());
        }

        [Test]
        public async Task get_page_when_user_is_anonymous()
        {
            // Arrange
            this.SetAnonymousUser();

            // Act
            HttpResponseMessage response = await this.GetAsync(
                path: PAGE_URL
            );

            // Assert
            response.StatusCode.Should().Be(HttpStatusCode.OK);
            IHtmlDocument document = await HtmlHelper.GetDocumentAsync(response);
            string accountText = document.QuerySelector(".account > a:first-child")?.TextContent;
            accountText.Should().Contain("Connexion");
        }

        [Test]
        public async Task get_page_when_user_is_authenticated()
        {
            // Arrange
            ApplicationUserEntity user = await ApplicationUserBuilder.Start()
                .Build();
            this.SetCurrentUser(user);

            // Act
            HttpResponseMessage response = await this.GetAsync(
                path: PAGE_URL
            );

            // Assert
            response.StatusCode.Should().Be(HttpStatusCode.OK);
            IHtmlDocument document = await HtmlHelper.GetDocumentAsync(response);
            string accountText = document.QuerySelector(".account > a:first-child")?.TextContent;
            accountText.Should().Contain("Mon compte");
        }

        [Test]
        public async Task get_page_commits_transaction_when_returning()
        {
            // Arrange
            this.SetAnonymousUser();

            // Act
            HttpResponseMessage response = await this.GetAsync(
                path: PAGE_URL
            );

            // Assert
            response.StatusCode.Should().Be(HttpStatusCode.OK);
            A.CallTo(() => this._databaseDriver.Commit()).MustHaveHappenedOnceExactly();
            A.CallTo(() => this._databaseDriver.Rollback()).MustNotHaveHappened();
        }
    }
}
