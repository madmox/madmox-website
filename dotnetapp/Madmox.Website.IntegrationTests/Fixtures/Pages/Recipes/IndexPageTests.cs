﻿using AngleSharp.Html.Dom;
using FluentAssertions;
using Madmox.Website.IntegrationTests.Infrastructure.Web;
using NUnit.Framework;
using System.Net;
using System.Net.Http;
using System.Threading.Tasks;

namespace Madmox.Website.IntegrationTests.Fixtures.Pages.Recipes
{
    public class IndexPageTests : BasePageTests
    {
        private const string PAGE_URL = "/recipes/categories";

        [Test]
        public async Task get_page_when_user_is_anonymous()
        {
            // Arrange
            this.SetAnonymousUser();

            // Act
            HttpResponseMessage response = await this.GetAsync(
                path: PAGE_URL
            );

            // Assert
            response.StatusCode.Should().Be(HttpStatusCode.OK);
            IHtmlDocument document = await HtmlHelper.GetDocumentAsync(response);
            string pageTitle = document.QuerySelector("html > head > title")?.TextContent;
            pageTitle.Should().Contain("www.madmox.fr");
        }
    }
}
