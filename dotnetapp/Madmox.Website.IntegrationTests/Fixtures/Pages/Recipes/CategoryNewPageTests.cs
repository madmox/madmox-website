﻿using AngleSharp.Html.Dom;
using FluentAssertions;
using Madmox.Website.Core.Types;
using Madmox.Website.DataAccess.Database.Entities;
using Madmox.Website.IntegrationTests.Builders;
using Madmox.Website.IntegrationTests.Infrastructure.Assets;
using Madmox.Website.IntegrationTests.Infrastructure.Database;
using Madmox.Website.IntegrationTests.Infrastructure.Web;
using Madmox.Website.Web.Pages.Recipes;
using NUnit.Framework;
using System.Collections.Generic;
using System.Net;
using System.Net.Http;
using System.Threading.Tasks;

namespace Madmox.Website.IntegrationTests.Fixtures.Pages.Recipes
{
    public class CategoryNewPageTests : BasePageTests
    {
        private const string PAGE_URL = "/recipes/categories/new";

        [Test]
        public async Task get_page_as_admin_user()
        {
            // Arrange
            ApplicationUserEntity user = await ApplicationUserBuilder.Start()
                .IsAdmin(true)
                .Build();
            this.SetCurrentUser(user);

            // Act
            HttpResponseMessage response = await this.GetAsync(
                path: PAGE_URL
            );

            // Assert
            response.StatusCode.Should().Be(HttpStatusCode.OK);
            IHtmlDocument document = await HtmlHelper.GetDocumentAsync(response);
            string recipeName = document.QuerySelector(".page-view h1")?.TextContent;
            recipeName.Should().Be("Nouvelle catégorie");
        }

        [Test]
        public async Task get_page_as_non_admin_user_redirects_to_access_denied()
        {
            // Arrange
            ApplicationUserEntity user = await ApplicationUserBuilder.Start()
                .IsAdmin(false)
                .Build();
            this.SetCurrentUser(user);

            // Act
            HttpResponseMessage response = await this.GetAsync(
                path: PAGE_URL
            );

            // Assert
            response.StatusCode.Should().Be(HttpStatusCode.Redirect);
            response.Headers.Location.Should().NotBeNull();
            response.Headers.Location.Should().Be("/accessdenied");
        }

        [Test]
        public async Task post_page_as_admin_user_creates_recipe()
        {
            // Arrange
            ApplicationUserEntity user = await ApplicationUserBuilder.Start()
                .IsAdmin(true)
                .Build();
            this.SetCurrentUser(user);
            byte[] photoFile = new byte[] { 0, 1, 2, 3 };

            // Act
            HttpResponseMessage response = await this.PostAsync(
                path: PAGE_URL,
                formData: new Dictionary<string, string>()
                {
                    { nameof(CategoryNewModel.Name), "Nom 2" },
                    { nameof(CategoryNewModel.Position), "0" },
                },
                formFiles: new Dictionary<string, FormFile>()
                {
                    { nameof(CategoryNewModel.PhotoFile), new FormFile("photo.png", photoFile) },
                }
            );

            // Assert
            response.StatusCode.Should().Be(HttpStatusCode.Redirect);
            EntitySet<RecipeCategoryEntity> dbRecipeCategories = await RecipeCategoryHelper.List();
            dbRecipeCategories.Should().HaveCount(1);
            RecipeCategoryEntity dbRecipeCategory = dbRecipeCategories[0];
            dbRecipeCategory.Should().NotBeNull();
            dbRecipeCategory.Name.Should().Be("Nom 2");
            response.Headers.Location.Should().NotBeNull();
            response.Headers.Location.OriginalString.Should().Match(x => x.StartsWith($"/recipes/categories/{dbRecipeCategory.Id}"));
            bool assetExists = await AssetsHelper.Instance.Exists($"images/recipes/categories/{dbRecipeCategory.Id}/photo.png");
            assetExists.Should().BeTrue();
        }

        [Test]
        public async Task post_page_as_non_admin_user_redirects_to_access_denied()
        {
            // Arrange
            ApplicationUserEntity user = await ApplicationUserBuilder.Start()
                .IsAdmin(false)
                .Build();
            this.SetCurrentUser(user);
            byte[] photoFile = new byte[] { 0, 1, 2, 3 };

            // Act
            HttpResponseMessage response = await this.PostAsync(
                path: PAGE_URL,
                formData: new Dictionary<string, string>()
                {
                    { nameof(CategoryNewModel.Name), "Nom 2" },
                    { nameof(CategoryNewModel.Position), "0" },
                },
                formFiles: new Dictionary<string, FormFile>()
                {
                    { nameof(CategoryNewModel.PhotoFile), new FormFile("photo.png", photoFile) },
                }
            );

            // Assert
            response.StatusCode.Should().Be(HttpStatusCode.Redirect);
            response.Headers.Location.Should().NotBeNull();
            response.Headers.Location.Should().Be("/accessdenied");
        }
    }
}
