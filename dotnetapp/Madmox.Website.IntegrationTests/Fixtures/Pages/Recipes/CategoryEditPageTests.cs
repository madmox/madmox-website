﻿using AngleSharp.Html.Dom;
using FluentAssertions;
using Madmox.Website.DataAccess.Database.Entities;
using Madmox.Website.IntegrationTests.Builders;
using Madmox.Website.IntegrationTests.Infrastructure.Assets;
using Madmox.Website.IntegrationTests.Infrastructure.Database;
using Madmox.Website.IntegrationTests.Infrastructure.Web;
using Madmox.Website.Web.Pages.Recipes;
using NUnit.Framework;
using System;
using System.Collections.Generic;
using System.Net;
using System.Net.Http;
using System.Threading.Tasks;

namespace Madmox.Website.IntegrationTests.Fixtures.Pages.Recipes
{
    public class CategoryEditPageTests : BasePageTests
    {
        private const string PAGE_URL = "/recipes/categories/{0}/{1}/edit";

        [Test]
        public async Task get_page_as_admin_user()
        {
            // Arrange
            ApplicationUserEntity user = await ApplicationUserBuilder.Start()
                .IsAdmin(true)
                .Build();
            RecipeCategoryEntity recipeCategory = await RecipeCategoryBuilder.Start()
                .Build();
            this.SetCurrentUser(user);

            // Act
            HttpResponseMessage response = await this.GetAsync(
                path: PAGE_URL,
                urlParams: new object[] { recipeCategory.Id, recipeCategory.Name.Slugify() }
            );

            // Assert
            response.StatusCode.Should().Be(HttpStatusCode.OK);
            IHtmlDocument document = await HtmlHelper.GetDocumentAsync(response);
            string recipeName = document.QuerySelector(".page-view h1")?.TextContent;
            recipeName.Should().Be(recipeCategory.Name);
        }

        [Test]
        public async Task get_page_as_non_admin_user_redirects_to_access_denied()
        {
            // Arrange
            ApplicationUserEntity user = await ApplicationUserBuilder.Start()
                .IsAdmin(false)
                .Build();
            RecipeCategoryEntity recipeCategory = await RecipeCategoryBuilder.Start()
                .Build();
            this.SetCurrentUser(user);

            // Act
            HttpResponseMessage response = await this.GetAsync(
                path: PAGE_URL,
                urlParams: new object[] { recipeCategory.Id, recipeCategory.Name.Slugify() }
            );

            // Assert
            response.StatusCode.Should().Be(HttpStatusCode.Redirect);
            response.Headers.Location.Should().NotBeNull();
            response.Headers.Location.Should().Be("/accessdenied");
        }

        [Test]
        public async Task post_page_as_admin_user_updates_recipe()
        {
            // Arrange
            ApplicationUserEntity user = await ApplicationUserBuilder.Start()
                .IsAdmin(true)
                .Build();
            RecipeCategoryEntity recipeCategory = await RecipeCategoryBuilder.Start()
                .Build();
            this.SetCurrentUser(user);

            // Act
            HttpResponseMessage response = await this.PostAsync(
                path: PAGE_URL,
                urlParams: new object[] { recipeCategory.Id, recipeCategory.Name.Slugify() },
                formData: new Dictionary<string, string>()
                {
                    { nameof(CategoryEditModel.Name), "Nom 2" },
                    { nameof(CategoryEditModel.Position), "0" },
                }
            );

            // Assert
            response.StatusCode.Should().Be(HttpStatusCode.Redirect);
            response.Headers.Location.Should().NotBeNull();
            response.Headers.Location.OriginalString.Should().Match(x => x.StartsWith($"/recipes/categories/{recipeCategory.Id}"));
            RecipeCategoryEntity dbRecipeCategory = await RecipeCategoryHelper.Get(recipeCategory.Id);
            dbRecipeCategory.Should().NotBeNull();
            dbRecipeCategory.Name.Should().Be("Nom 2");
        }

        [Test]
        public async Task post_page_with_new_photo_creates_asset()
        {
            // Arrange
            ApplicationUserEntity user = await ApplicationUserBuilder.Start()
                .IsAdmin(true)
                .Build();
            RecipeCategoryEntity recipeCategory = await RecipeCategoryBuilder.Start()
                .Build();
            this.SetCurrentUser(user);
            byte[] photoFile = new byte[] { 0, 1, 2, 3 };

            // Act
            HttpResponseMessage response = await this.PostAsync(
                path: PAGE_URL,
                urlParams: new object[] { recipeCategory.Id, recipeCategory.Name.Slugify() },
                formData: new Dictionary<string, string>()
                {
                    { nameof(CategoryEditModel.Name), "Nom 2" },
                    { nameof(CategoryEditModel.Position), "0" },
                },
                formFiles: new Dictionary<string, FormFile>()
                {
                    { nameof(CategoryEditModel.NewPhotoFile), new FormFile("photo.png", photoFile) },
                }
            );

            // Assert
            response.StatusCode.Should().Be(HttpStatusCode.Redirect);
            response.Headers.Location.Should().NotBeNull();
            response.Headers.Location.OriginalString.Should().Match(x => x.StartsWith($"/recipes/categories/{recipeCategory.Id}"));
            bool assetExists = await AssetsHelper.Instance.Exists($"images/recipes/categories/{recipeCategory.Id}/photo.png");
            assetExists.Should().BeTrue();
        }

        [Test]
        public async Task post_page_as_non_admin_user_redirects_to_access_denied()
        {
            // Arrange
            ApplicationUserEntity user = await ApplicationUserBuilder.Start()
                .IsAdmin(false)
                .Build();
            RecipeCategoryEntity recipeCategory = await RecipeCategoryBuilder.Start()
                .Build();
            this.SetCurrentUser(user);

            // Act
            HttpResponseMessage response = await this.PostAsync(
                path: PAGE_URL,
                urlParams: new object[] { recipeCategory.Id, recipeCategory.Name.Slugify() },
                formData: new Dictionary<string, string>()
                {
                    { nameof(CategoryEditModel.Name), "Name 2" },
                    { nameof(CategoryEditModel.Position), "0" },
                }
            );

            // Assert
            response.StatusCode.Should().Be(HttpStatusCode.Redirect);
            response.Headers.Location.Should().NotBeNull();
            response.Headers.Location.Should().Be("/accessdenied");
        }
    }
}
