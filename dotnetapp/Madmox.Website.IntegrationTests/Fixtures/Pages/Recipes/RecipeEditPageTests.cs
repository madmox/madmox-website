﻿using AngleSharp.Html.Dom;
using FluentAssertions;
using Madmox.Website.DataAccess.Database.Entities;
using Madmox.Website.IntegrationTests.Builders;
using Madmox.Website.IntegrationTests.Infrastructure.Assets;
using Madmox.Website.IntegrationTests.Infrastructure.Database;
using Madmox.Website.IntegrationTests.Infrastructure.Web;
using Madmox.Website.Web.Pages.Recipes;
using NUnit.Framework;
using System;
using System.Collections.Generic;
using System.Net;
using System.Net.Http;
using System.Threading.Tasks;

namespace Madmox.Website.IntegrationTests.Fixtures.Pages.Recipes
{
    public class RecipeEditPageTests : BasePageTests
    {
        private const string PAGE_URL = "/recipes/recipes/{0}/{1}/edit";

        [Test]
        public async Task get_page_as_admin_user()
        {
            // Arrange
            ApplicationUserEntity user = await ApplicationUserBuilder.Start()
                .IsAdmin(true)
                .Build();
            RecipeCategoryEntity recipeCategory = await RecipeCategoryBuilder.Start()
                .Build();
            RecipeEntity recipe = await RecipeBuilder.Start()
                .WithCategory(recipeCategory)
                .Build();
            this.SetCurrentUser(user);

            // Act
            HttpResponseMessage response = await this.GetAsync(
                path: PAGE_URL,
                urlParams: new object[] { recipe.Id, recipe.Name.Slugify() }
            );

            // Assert
            response.StatusCode.Should().Be(HttpStatusCode.OK);
            IHtmlDocument document = await HtmlHelper.GetDocumentAsync(response);
            string recipeName = document.QuerySelector(".page-view h1")?.TextContent;
            recipeName.Should().Be(recipe.Name);
        }

        [Test]
        public async Task get_page_as_non_admin_user_redirects_to_access_denied()
        {
            // Arrange
            ApplicationUserEntity user = await ApplicationUserBuilder.Start()
                .IsAdmin(false)
                .Build();
            RecipeCategoryEntity recipeCategory = await RecipeCategoryBuilder.Start()
                .Build();
            RecipeEntity recipe = await RecipeBuilder.Start()
                .WithCategory(recipeCategory)
                .Build();
            this.SetCurrentUser(user);

            // Act
            HttpResponseMessage response = await this.GetAsync(
                path: PAGE_URL,
                urlParams: new object[] { recipe.Id, recipe.Name.Slugify() }
            );

            // Assert
            response.StatusCode.Should().Be(HttpStatusCode.Redirect);
            response.Headers.Location.Should().NotBeNull();
            response.Headers.Location.Should().Be("/accessdenied");
        }

        [Test]
        public async Task post_page_as_admin_user_updates_recipe()
        {
            // Arrange
            ApplicationUserEntity user = await ApplicationUserBuilder.Start()
                .IsAdmin(true)
                .Build();
            RecipeCategoryEntity recipeCategory1 = await RecipeCategoryBuilder.Start()
                .Build();
            RecipeCategoryEntity recipeCategory2 = await RecipeCategoryBuilder.Start()
                .Build();
            RecipeEntity recipe = await RecipeBuilder.Start()
                .WithCategory(recipeCategory1)
                .Build();
            this.SetCurrentUser(user);

            // Act
            HttpResponseMessage response = await this.PostAsync(
                path: PAGE_URL,
                urlParams: new object[] { recipe.Id, recipe.Name.Slugify() },
                formData: new Dictionary<string, string>()
                {
                    { nameof(RecipeEditModel.Name), "Nom 2" },
                    { nameof(RecipeEditModel.CategoryId), recipeCategory2.Id.ToString() },
                    { nameof(RecipeEditModel.NbPersons), "3" },
                    { nameof(RecipeEditModel.PreparationTime), "0:00:30:00" },
                    { nameof(RecipeEditModel.TotalTime), "0:01:00:00" },
                    { nameof(RecipeEditModel.Ingredients), $"Ingrédient 1{Environment.NewLine}Ingrédient 2" },
                    { nameof(RecipeEditModel.Tools), $"Ustensile 1{Environment.NewLine}Ustensile 2" },
                    { nameof(RecipeEditModel.Steps), $"Étape 1{Environment.NewLine}Étape 2" },
                }
            );

            // Assert
            response.StatusCode.Should().Be(HttpStatusCode.Redirect);
            response.Headers.Location.Should().NotBeNull();
            response.Headers.Location.OriginalString.Should().Match(x => x.StartsWith($"/recipes/recipes/{recipe.Id}"));
            RecipeEntity dbRecipe = await RecipeHelper.Get(recipe.Id);
            dbRecipe.Should().NotBeNull();
            dbRecipe.Name.Should().Be("Nom 2");
        }

        [Test]
        public async Task post_page_with_new_photo_creates_asset()
        {
            // Arrange
            ApplicationUserEntity user = await ApplicationUserBuilder.Start()
                .IsAdmin(true)
                .Build();
            RecipeCategoryEntity recipeCategory = await RecipeCategoryBuilder.Start()
                .Build();
            RecipeEntity recipe = await RecipeBuilder.Start()
                .WithCategory(recipeCategory)
                .Build();
            this.SetCurrentUser(user);
            byte[] photoFile = new byte[] { 0, 1, 2, 3 };

            // Act
            HttpResponseMessage response = await this.PostAsync(
                path: PAGE_URL,
                urlParams: new object[] { recipe.Id, recipe.Name.Slugify() },
                formData: new Dictionary<string, string>()
                {
                    { nameof(RecipeEditModel.Name), "Nom 2" },
                    { nameof(RecipeEditModel.CategoryId), recipeCategory.Id.ToString() },
                    { nameof(RecipeEditModel.NbPersons), "3" },
                    { nameof(RecipeEditModel.PreparationTime), "0:00:30:00" },
                    { nameof(RecipeEditModel.TotalTime), "0:01:00:00" },
                    { nameof(RecipeEditModel.Ingredients), $"Ingrédient 1{Environment.NewLine}Ingrédient 2" },
                    { nameof(RecipeEditModel.Tools), $"Ustensile 1{Environment.NewLine}Ustensile 2" },
                    { nameof(RecipeEditModel.Steps), $"Étape 1{Environment.NewLine}Étape 2" },
                },
                formFiles: new Dictionary<string, FormFile>()
                {
                    { nameof(RecipeEditModel.NewPhotoFile), new FormFile("photo.png", photoFile) },
                }
            );

            // Assert
            response.StatusCode.Should().Be(HttpStatusCode.Redirect);
            response.Headers.Location.Should().NotBeNull();
            response.Headers.Location.OriginalString.Should().Match(x => x.StartsWith($"/recipes/recipes/{recipe.Id}"));
            bool assetExists = await AssetsHelper.Instance.Exists($"images/recipes/recipes/{recipe.Id}/photo.png");
            assetExists.Should().BeTrue();
        }

        [Test]
        public async Task post_page_as_non_admin_user_redirects_to_access_denied()
        {
            // Arrange
            ApplicationUserEntity user = await ApplicationUserBuilder.Start()
                .IsAdmin(false)
                .Build();
            RecipeCategoryEntity recipeCategory = await RecipeCategoryBuilder.Start()
                .Build();
            RecipeEntity recipe = await RecipeBuilder.Start()
                .WithCategory(recipeCategory)
                .Build();
            this.SetCurrentUser(user);

            // Act
            HttpResponseMessage response = await this.PostAsync(
                path: PAGE_URL,
                urlParams: new object[] { recipe.Id, recipe.Name.Slugify() },
                formData: new Dictionary<string, string>()
                {
                    { nameof(RecipeEditModel.Name), "Name 2" },
                    { nameof(RecipeEditModel.CategoryId), recipeCategory.Id.ToString() },
                    { nameof(RecipeEditModel.NbPersons), "3" },
                    { nameof(RecipeEditModel.PreparationTime), "0:00:30:00" },
                    { nameof(RecipeEditModel.TotalTime), "0:01:00:00" },
                    { nameof(RecipeEditModel.Ingredients), $"Ingrédient 1{Environment.NewLine}Ingrédient 2" },
                    { nameof(RecipeEditModel.Tools), $"Ustensile 1{Environment.NewLine}Ustensile 2" },
                    { nameof(RecipeEditModel.Steps), $"Étape 1{Environment.NewLine}Étape 2" },
                }
            );

            // Assert
            response.StatusCode.Should().Be(HttpStatusCode.Redirect);
            response.Headers.Location.Should().NotBeNull();
            response.Headers.Location.Should().Be("/accessdenied");
        }
    }
}
