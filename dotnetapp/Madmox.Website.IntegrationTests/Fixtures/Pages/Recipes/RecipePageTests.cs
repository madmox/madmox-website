﻿using AngleSharp.Html.Dom;
using FluentAssertions;
using Madmox.Website.DataAccess.Database.Entities;
using Madmox.Website.IntegrationTests.Builders;
using Madmox.Website.IntegrationTests.Infrastructure.Web;
using NUnit.Framework;
using System;
using System.Net;
using System.Net.Http;
using System.Threading.Tasks;

namespace Madmox.Website.IntegrationTests.Fixtures.Pages.Recipes
{
    public class RecipePageTests : BasePageTests
    {
        private const string PAGE_URL = "/recipes/recipes/{0}/{1}";

        [Test]
        public async Task get_page_when_user_is_anonymous()
        {
            // Arrange
            RecipeCategoryEntity recipeCategory = await RecipeCategoryBuilder.Start()
                .WithName("Plats")
                .Build();
            RecipeEntity recipe = await RecipeBuilder.Start()
                .WithCategory(recipeCategory)
                .Build();
            this.SetAnonymousUser();

            // Act
            HttpResponseMessage response = await this.GetAsync(
                path: PAGE_URL,
                urlParams: new object[] { recipe.Id, recipe.Name.Slugify() }
            );

            // Assert
            response.StatusCode.Should().Be(HttpStatusCode.OK);
            IHtmlDocument document = await HtmlHelper.GetDocumentAsync(response);
            string pageTitle = document.QuerySelector("html > head > title")?.TextContent;
            pageTitle.Should().Contain("www.madmox.fr");
            string recipeName = document.QuerySelector(".page-view h1")?.TextContent;
            recipeName.Should().Be(recipe.Name);
        }

        [Test]
        public async Task get_page_with_unknown_recipe_returns_404()
        {
            // Arrange
            this.SetAnonymousUser();

            // Act
            HttpResponseMessage response = await this.GetAsync(
                path: PAGE_URL,
                urlParams: new object[] { Guid.NewGuid(), "unknown" }
            );

            // Assert
            response.StatusCode.Should().Be(HttpStatusCode.NotFound);
        }

        [Test]
        public async Task get_page_with_invalid_slug_returns_301()
        {
            // Arrange
            RecipeCategoryEntity recipeCategory = await RecipeCategoryBuilder.Start()
                .WithName("Plats")
                .Build();
            RecipeEntity recipe = await RecipeBuilder.Start()
                .WithCategory(recipeCategory)
                .Build();
            this.SetAnonymousUser();

            // Act
            HttpResponseMessage response = await this.GetAsync(
                path: PAGE_URL,
                urlParams: new object[] { recipe.Id, "other" }
            );

            // Assert
            response.StatusCode.Should().Be(HttpStatusCode.Moved);
            response.Headers.Location.Should().NotBeNull();
            response.Headers.Location.Should().Be(PAGE_URL.Format(recipe.Id, recipe.Name.Slugify()));
        }
    }
}
