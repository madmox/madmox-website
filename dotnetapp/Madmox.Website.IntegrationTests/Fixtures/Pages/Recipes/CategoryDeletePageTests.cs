﻿using AngleSharp.Html.Dom;
using FluentAssertions;
using Madmox.Website.DataAccess.Database.Entities;
using Madmox.Website.IntegrationTests.Builders;
using Madmox.Website.IntegrationTests.Infrastructure.Assets;
using Madmox.Website.IntegrationTests.Infrastructure.Database;
using Madmox.Website.IntegrationTests.Infrastructure.Web;
using NUnit.Framework;
using System;
using System.Net;
using System.Net.Http;
using System.Threading.Tasks;

namespace Madmox.Website.IntegrationTests.Fixtures.Pages.Recipes
{
    public class CategoryDeletePageTests : BasePageTests
    {
        private const string PAGE_URL = "/recipes/categories/{0}/{1}/delete";

        [Test]
        public async Task get_page_as_admin_user()
        {
            // Arrange
            ApplicationUserEntity user = await ApplicationUserBuilder.Start()
                .IsAdmin(true)
                .Build();
            RecipeCategoryEntity recipeCategory = await RecipeCategoryBuilder.Start()
                .Build();
            this.SetCurrentUser(user);

            // Act
            HttpResponseMessage response = await this.GetAsync(
                path: PAGE_URL,
                urlParams: new object[] { recipeCategory.Id, recipeCategory.Name.Slugify() }
            );

            // Assert
            response.StatusCode.Should().Be(HttpStatusCode.OK);
            IHtmlDocument document = await HtmlHelper.GetDocumentAsync(response);
            string recipeName = document.QuerySelector(".page-view h1")?.TextContent;
            recipeName.Should().Be(recipeCategory.Name);
        }

        [Test]
        public async Task get_page_as_non_admin_user_redirects_to_access_denied()
        {
            // Arrange
            ApplicationUserEntity user = await ApplicationUserBuilder.Start()
                .IsAdmin(false)
                .Build();
            RecipeCategoryEntity recipeCategory = await RecipeCategoryBuilder.Start()
                .Build();
            this.SetCurrentUser(user);

            // Act
            HttpResponseMessage response = await this.GetAsync(
                path: PAGE_URL,
                urlParams: new object[] { recipeCategory.Id, recipeCategory.Name.Slugify() }
            );

            // Assert
            response.StatusCode.Should().Be(HttpStatusCode.Redirect);
            response.Headers.Location.Should().NotBeNull();
            response.Headers.Location.Should().Be("/accessdenied");
        }

        [Test]
        public async Task get_page_when_category_contains_recipes_redirects_to_read_view()
        {
            // Arrange
            ApplicationUserEntity user = await ApplicationUserBuilder.Start()
                .IsAdmin(true)
                .Build();
            RecipeCategoryEntity recipeCategory = await RecipeCategoryBuilder.Start()
                .Build();
            RecipeEntity recipe = await RecipeBuilder.Start()
                .WithCategory(recipeCategory)
                .Build();
            this.SetCurrentUser(user);

            // Act
            HttpResponseMessage response = await this.GetAsync(
                path: PAGE_URL,
                urlParams: new object[] { recipeCategory.Id, recipeCategory.Name.Slugify() }
            );

            // Assert
            response.StatusCode.Should().Be(HttpStatusCode.Redirect);
            response.Headers.Location.Should().NotBeNull();
            response.Headers.Location.OriginalString.Should().Match(x => x.StartsWith($"/recipes/categories/{recipeCategory.Id}"));
        }

        [Test]
        public async Task post_page_as_admin_user_deletes_category()
        {
            // Arrange
            ApplicationUserEntity user = await ApplicationUserBuilder.Start()
                .IsAdmin(true)
                .Build();
            RecipeCategoryEntity recipeCategory = await RecipeCategoryBuilder.Start()
                .Build();
            this.SetCurrentUser(user);
            await AssetsHelper.Instance.Upsert($"images/recipes/categories/{recipeCategory.Id}/photo.png", new byte[] { 0, 1, 2, 3 });

            // Act
            HttpResponseMessage response = await this.PostAsync(
                path: PAGE_URL,
                urlParams: new object[] { recipeCategory.Id, recipeCategory.Name.Slugify() }
            );

            // Assert
            response.StatusCode.Should().Be(HttpStatusCode.Redirect);
            response.Headers.Location.Should().NotBeNull();
            response.Headers.Location.OriginalString.Should().Match(x => x.StartsWith($"/recipes/categories"));
            RecipeCategoryEntity dbRecipeCategory = await RecipeCategoryHelper.Get(recipeCategory.Id);
            dbRecipeCategory.Should().BeNull();
            bool assetExists = await AssetsHelper.Instance.Exists($"images/recipes/categories/{recipeCategory.Id}/photo.png");
            assetExists.Should().BeFalse();
        }

        [Test]
        public async Task post_page_as_non_admin_user_redirects_to_access_denied()
        {
            // Arrange
            ApplicationUserEntity user = await ApplicationUserBuilder.Start()
                .IsAdmin(false)
                .Build();
            RecipeCategoryEntity recipeCategory = await RecipeCategoryBuilder.Start()
                .Build();
            this.SetCurrentUser(user);

            // Act
            HttpResponseMessage response = await this.PostAsync(
                path: PAGE_URL,
                urlParams: new object[] { recipeCategory.Id, recipeCategory.Name.Slugify() }
            );

            // Assert
            response.StatusCode.Should().Be(HttpStatusCode.Redirect);
            response.Headers.Location.Should().NotBeNull();
            response.Headers.Location.Should().Be("/accessdenied");
        }

        [Test]
        public async Task post_page_when_category_contains_recipes_redirects_to_read_view()
        {
            // Arrange
            ApplicationUserEntity user = await ApplicationUserBuilder.Start()
                .IsAdmin(true)
                .Build();
            RecipeCategoryEntity recipeCategory = await RecipeCategoryBuilder.Start()
                .Build();
            RecipeEntity recipe = await RecipeBuilder.Start()
                .WithCategory(recipeCategory)
                .Build();
            this.SetCurrentUser(user);

            // Act
            HttpResponseMessage response = await this.PostAsync(
                path: PAGE_URL,
                urlParams: new object[] { recipeCategory.Id, recipeCategory.Name.Slugify() }
            );

            // Assert
            response.StatusCode.Should().Be(HttpStatusCode.Redirect);
            response.Headers.Location.Should().NotBeNull();
            response.Headers.Location.OriginalString.Should().Match(x => x.StartsWith($"/recipes/categories/{recipeCategory.Id}"));
        }
    }
}
