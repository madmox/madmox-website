﻿using AngleSharp.Html.Dom;
using FluentAssertions;
using Madmox.Website.Core.Types;
using Madmox.Website.DataAccess.Database.Entities;
using Madmox.Website.IntegrationTests.Builders;
using Madmox.Website.IntegrationTests.Infrastructure.Assets;
using Madmox.Website.IntegrationTests.Infrastructure.Database;
using Madmox.Website.IntegrationTests.Infrastructure.Web;
using Madmox.Website.Web.Pages.Recipes;
using NUnit.Framework;
using System;
using System.Collections.Generic;
using System.Net;
using System.Net.Http;
using System.Threading.Tasks;

namespace Madmox.Website.IntegrationTests.Fixtures.Pages.Recipes
{
    public class RecipeNewPageTests : BasePageTests
    {
        private const string PAGE_URL = "/recipes/recipes/new";

        [Test]
        public async Task get_page_as_admin_user()
        {
            // Arrange
            ApplicationUserEntity user = await ApplicationUserBuilder.Start()
                .IsAdmin(true)
                .Build();
            RecipeCategoryEntity recipeCategory = await RecipeCategoryBuilder.Start()
                .Build();
            this.SetCurrentUser(user);

            // Act
            HttpResponseMessage response = await this.GetAsync(
                path: PAGE_URL,
                queryStringParams: new Dictionary<string, object>()
                {
                    { "category_id", recipeCategory.Id },
                }
            );

            // Assert
            response.StatusCode.Should().Be(HttpStatusCode.OK);
            IHtmlDocument document = await HtmlHelper.GetDocumentAsync(response);
            string recipeName = document.QuerySelector(".page-view h1")?.TextContent;
            recipeName.Should().Be("Nouvelle recette");
        }

        [Test]
        public async Task get_page_as_non_admin_user_redirects_to_access_denied()
        {
            // Arrange
            ApplicationUserEntity user = await ApplicationUserBuilder.Start()
                .IsAdmin(false)
                .Build();
            RecipeCategoryEntity recipeCategory = await RecipeCategoryBuilder.Start()
                .Build();
            this.SetCurrentUser(user);

            // Act
            HttpResponseMessage response = await this.GetAsync(
                path: PAGE_URL,
                queryStringParams: new Dictionary<string, object>()
                {
                    { "category_id", recipeCategory.Id },
                }
            );

            // Assert
            response.StatusCode.Should().Be(HttpStatusCode.Redirect);
            response.Headers.Location.Should().NotBeNull();
            response.Headers.Location.Should().Be("/accessdenied");
        }

        [Test]
        public async Task post_page_as_admin_user_creates_recipe()
        {
            // Arrange
            ApplicationUserEntity user = await ApplicationUserBuilder.Start()
                .IsAdmin(true)
                .Build();
            RecipeCategoryEntity recipeCategory = await RecipeCategoryBuilder.Start()
                .Build();
            this.SetCurrentUser(user);
            byte[] photoFile = new byte[] { 0, 1, 2, 3 };

            // Act
            HttpResponseMessage response = await this.PostAsync(
                path: PAGE_URL,
                queryStringParams: new Dictionary<string, object>()
                {
                    { "category_id", recipeCategory.Id },
                },
                formData: new Dictionary<string, string>()
                {
                    { nameof(RecipeNewModel.Name), "Nom 2" },
                    { nameof(RecipeNewModel.NbPersons), "3" },
                    { nameof(RecipeNewModel.PreparationTime), "0:00:30:00" },
                    { nameof(RecipeNewModel.TotalTime), "0:01:00:00" },
                    { nameof(RecipeNewModel.Ingredients), $"Ingrédient 1{Environment.NewLine}Ingrédient 2" },
                    { nameof(RecipeNewModel.Tools), $"Ustensile 1{Environment.NewLine}Ustensile 2" },
                    { nameof(RecipeNewModel.Steps), $"Étape 1{Environment.NewLine}Étape 2" },
                },
                formFiles: new Dictionary<string, FormFile>()
                {
                    { nameof(RecipeNewModel.PhotoFile), new FormFile("photo.png", photoFile) },
                }
            );

            // Assert
            response.StatusCode.Should().Be(HttpStatusCode.Redirect);
            EntitySet<RecipeEntity> dbRecipes = await RecipeHelper.List();
            dbRecipes.Should().HaveCount(1);
            RecipeEntity dbRecipe = dbRecipes[0];
            dbRecipe.Should().NotBeNull();
            dbRecipe.Name.Should().Be("Nom 2");
            response.Headers.Location.Should().NotBeNull();
            response.Headers.Location.OriginalString.Should().Match(x => x.StartsWith($"/recipes/recipes/{dbRecipe.Id}"));
            bool assetExists = await AssetsHelper.Instance.Exists($"images/recipes/recipes/{dbRecipe.Id}/photo.png");
            assetExists.Should().BeTrue();
        }

        [Test]
        public async Task post_page_as_non_admin_user_redirects_to_access_denied()
        {
            // Arrange
            ApplicationUserEntity user = await ApplicationUserBuilder.Start()
                .IsAdmin(false)
                .Build();
            RecipeCategoryEntity recipeCategory = await RecipeCategoryBuilder.Start()
                .Build();
            this.SetCurrentUser(user);
            byte[] photoFile = new byte[] { 0, 1, 2, 3 };

            // Act
            HttpResponseMessage response = await this.PostAsync(
                path: PAGE_URL,
                queryStringParams: new Dictionary<string, object>()
                {
                    { "category_id", recipeCategory.Id },
                },
                formData: new Dictionary<string, string>()
                {
                    { nameof(RecipeNewModel.Name), "Nom 2" },
                    { nameof(RecipeNewModel.NbPersons), "3" },
                    { nameof(RecipeNewModel.PreparationTime), "0:00:30:00" },
                    { nameof(RecipeNewModel.TotalTime), "0:01:00:00" },
                    { nameof(RecipeNewModel.Ingredients), $"Ingrédient 1{Environment.NewLine}Ingrédient 2" },
                    { nameof(RecipeNewModel.Tools), $"Ustensile 1{Environment.NewLine}Ustensile 2" },
                    { nameof(RecipeNewModel.Steps), $"Étape 1{Environment.NewLine}Étape 2" },
                },
                formFiles: new Dictionary<string, FormFile>()
                {
                    { nameof(RecipeNewModel.PhotoFile), new FormFile("photo.png", photoFile) },
                }
            );

            // Assert
            response.StatusCode.Should().Be(HttpStatusCode.Redirect);
            response.Headers.Location.Should().NotBeNull();
            response.Headers.Location.Should().Be("/accessdenied");
        }
    }
}
