﻿using FluentAssertions;
using Madmox.Website.DataAccess.Database.Entities;
using Madmox.Website.IntegrationTests.Builders;
using Madmox.Website.IntegrationTests.Infrastructure.Database;
using Madmox.Website.Web.Pages.Account;
using NUnit.Framework;
using System.Collections.Generic;
using System.Net;
using System.Net.Http;
using System.Threading.Tasks;

namespace Madmox.Website.IntegrationTests.Fixtures.Pages.Account
{
    public class PasswordPageTests : BasePageTests
    {
        private const string PAGE_URL = "/account/password";

        [Test]
        public async Task get_page_when_user_is_authenticated()
        {
            // Arrange
            ApplicationUserEntity user = await ApplicationUserBuilder.Start()
                .Build();
            this.SetCurrentUser(user);

            // Act
            HttpResponseMessage response = await this.GetAsync(
                path: PAGE_URL
            );

            // Assert
            response.StatusCode.Should().Be(HttpStatusCode.OK);
        }

        [Test]
        public async Task get_page_when_user_is_anonymous_redirects_to_signin()
        {
            // Arrange
            this.SetAnonymousUser();

            // Act
            HttpResponseMessage response = await this.GetAsync(
                path: PAGE_URL
            );

            // Assert
            response.StatusCode.Should().Be(HttpStatusCode.Redirect);
            response.Headers.Location.Should().NotBeNull();
            response.Headers.Location.Should().Be("/account/signin");
        }

        [Test]
        public async Task post_page_when_user_is_authenticated_updates_password()
        {
            // Arrange
            ApplicationUserEntity user = await ApplicationUserBuilder.Start()
                .WithClearTextPassword("123AZEqsd!")
                .Build();
            this.SetCurrentUser(user);

            // Act
            HttpResponseMessage response = await this.PostAsync(
                path: PAGE_URL,
                formData: new Dictionary<string, string>()
                {
                    { nameof(PasswordModel.CurrentPassword), "123AZEqsd!" },
                    { nameof(PasswordModel.NewPassword), "456AZEqsd!" },
                    { nameof(PasswordModel.PasswordConfirmation), "456AZEqsd!" },
                }
            );

            // Assert
            response.StatusCode.Should().Be(HttpStatusCode.Redirect);
            response.Headers.Location.Should().NotBeNull();
            response.Headers.Location.OriginalString.Should().Match(x => x.StartsWith(PAGE_URL));
            ApplicationUserEntity dbUser = await ApplicationUserHelper.Get(user.Id);
            dbUser.Should().NotBeNull();
            dbUser.IsPasswordClearText.Should().BeFalse();
            dbUser.Password.Should().NotBe("123AZEqsd!");
        }

        [Test]
        public async Task post_page_when_user_is_anonymous_redirects_to_signin()
        {
            // Arrange
            this.SetAnonymousUser();

            // Act
            HttpResponseMessage response = await this.PostAsync(
                path: PAGE_URL,
                formData: new Dictionary<string, string>()
                {
                    { nameof(PasswordModel.CurrentPassword), "123AZEqsd!" },
                    { nameof(PasswordModel.NewPassword), "456AZEqsd!" },
                    { nameof(PasswordModel.PasswordConfirmation), "456AZEqsd!" },
                }
            );

            // Assert
            response.StatusCode.Should().Be(HttpStatusCode.Redirect);
            response.Headers.Location.Should().NotBeNull();
            response.Headers.Location.Should().Be("/account/signin");
        }
    }
}
