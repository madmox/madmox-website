﻿using FluentAssertions;
using Madmox.Website.DataAccess.Database.Entities;
using Madmox.Website.IntegrationTests.Builders;
using Madmox.Website.Web;
using Microsoft.Net.Http.Headers;
using NUnit.Framework;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Threading.Tasks;

namespace Madmox.Website.IntegrationTests.Fixtures.Pages.Account
{
    public class SignOutPageTests : BasePageTests
    {
        private const string PAGE_URL = "/account/signout";

        [Test]
        public async Task post_page_when_user_is_authenticated_signs_user_out()
        {
            // Arrange
            ApplicationUserEntity user = await ApplicationUserBuilder.Start()
                .Build();
            this.SetCurrentUser(user);

            // Act
            HttpResponseMessage response = await this.PostAsync(
                path: PAGE_URL
            );

            // Assert
            response.StatusCode.Should().Be(HttpStatusCode.Redirect);
            response.Headers.Location.Should().NotBeNull();
            response.Headers.Location.Should().Be("/");
            response.Headers.TryGetValues(HeaderNames.SetCookie, out IEnumerable<string> setCookieHeaders);
            IList<SetCookieHeaderValue> cookies = SetCookieHeaderValue.ParseList(setCookieHeaders?.ToList() ?? new List<string>());
            SetCookieHeaderValue cookie = cookies.FirstOrDefault(x => x.Name.Value == Constants.Security.COOKIE_NAME);
            cookie.Should().NotBeNull();
            cookie.Expires.Should().BeBefore(this.CallEnd.ToDateTimeOffset());
        }

        [Test]
        public async Task post_page_when_user_is_anonymous_redirects_to_signin()
        {
            // Arrange
            this.SetAnonymousUser();

            // Act
            HttpResponseMessage response = await this.PostAsync(
                path: PAGE_URL
            );

            // Assert
            response.StatusCode.Should().Be(HttpStatusCode.Redirect);
            response.Headers.Location.Should().NotBeNull();
            response.Headers.Location.Should().Be("/account/signin");
            response.Headers.TryGetValues(HeaderNames.SetCookie, out IEnumerable<string> setCookieHeaders);
            IList<SetCookieHeaderValue> cookies = SetCookieHeaderValue.ParseList(setCookieHeaders?.ToList() ?? new List<string>());
            SetCookieHeaderValue cookie = cookies.FirstOrDefault(x => x.Name.Value == Constants.Security.COOKIE_NAME);
            cookie.Should().BeNull();
        }
    }
}
