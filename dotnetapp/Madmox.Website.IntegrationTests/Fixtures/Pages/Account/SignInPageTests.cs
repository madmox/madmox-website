﻿using FluentAssertions;
using Madmox.Website.DataAccess.Database.Entities;
using Madmox.Website.IntegrationTests.Builders;
using Madmox.Website.Web;
using Madmox.Website.Web.Pages.Account;
using Microsoft.Net.Http.Headers;
using NUnit.Framework;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Threading.Tasks;

namespace Madmox.Website.IntegrationTests.Fixtures.Pages.Account
{
    public class SignInPageTests : BasePageTests
    {
        private const string PAGE_URL = "/account/signin";

        [Test]
        public async Task get_page_when_user_is_anonymous()
        {
            // Arrange
            this.SetAnonymousUser();

            // Act
            HttpResponseMessage response = await this.GetAsync(
                path: PAGE_URL
            );

            // Assert
            response.StatusCode.Should().Be(HttpStatusCode.OK);
        }

        [Test]
        public async Task get_page_when_user_is_authenticated_redirects_to_index()
        {
            // Arrange
            ApplicationUserEntity user = await ApplicationUserBuilder.Start()
                .Build();
            this.SetCurrentUser(user);

            // Act
            HttpResponseMessage response = await this.GetAsync(
                path: PAGE_URL
            );

            // Assert
            response.StatusCode.Should().Be(HttpStatusCode.Redirect);
            response.Headers.Location.Should().NotBeNull();
            response.Headers.Location.Should().Be("/");
        }

        [Test]
        public async Task post_page_with_valid_credentials_signs_user_in()
        {
            // Arrange
            ApplicationUserEntity user = await ApplicationUserBuilder.Start()
                .WithEmail("user@example.com")
                .WithClearTextPassword("123AZEqsd!")
                .Build();
            this.SetAnonymousUser();

            // Act
            HttpResponseMessage response = await this.PostAsync(
                path: PAGE_URL,
                formData: new Dictionary<string, string>()
                {
                    { nameof(SignInModel.Email), user.Email },
                    { nameof(SignInModel.Password), user.Password },
                }
            );

            // Assert
            response.StatusCode.Should().Be(HttpStatusCode.Redirect);
            response.Headers.Location.Should().NotBeNull();
            response.Headers.Location.Should().Be("/");
            response.Headers.TryGetValues(HeaderNames.SetCookie, out IEnumerable<string> setCookieHeaders);
            IList<SetCookieHeaderValue> cookies = SetCookieHeaderValue.ParseList(setCookieHeaders?.ToList() ?? new List<string>());
            SetCookieHeaderValue cookie = cookies.FirstOrDefault(x => x.Name.Value == Constants.Security.COOKIE_NAME);
            cookie.Should().NotBeNull();
        }

        [Test]
        public async Task post_page_with_invalid_credentials_displays_error()
        {
            // Arrange
            ApplicationUserEntity user = await ApplicationUserBuilder.Start()
                .WithEmail("user@example.com")
                .WithClearTextPassword("123AZEqsd!")
                .Build();
            this.SetAnonymousUser();

            // Act
            HttpResponseMessage response = await this.PostAsync(
                path: PAGE_URL,
                formData: new Dictionary<string, string>()
                {
                    { nameof(SignInModel.Email), user.Email },
                    { nameof(SignInModel.Password), "invalid" },
                }
            );

            // Assert
            response.StatusCode.Should().Be(HttpStatusCode.OK);
            response.Headers.TryGetValues(HeaderNames.SetCookie, out IEnumerable<string> setCookieHeaders);
            IList<SetCookieHeaderValue> cookies = SetCookieHeaderValue.ParseList(setCookieHeaders?.ToList() ?? new List<string>());
            SetCookieHeaderValue cookie = cookies.FirstOrDefault(x => x.Name.Value == Constants.Security.COOKIE_NAME);
            cookie.Should().BeNull();
        }
    }
}
