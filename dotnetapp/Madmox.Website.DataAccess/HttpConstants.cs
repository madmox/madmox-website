﻿using NodaTime;
using Polly;
using Polly.Extensions.Http;
using System;
using System.Net.Http;

namespace Madmox.Website.DataAccess
{
    public static class HttpConstants
    {
        public static IAsyncPolicy<HttpResponseMessage> GetDefaultRetryPolicy()
        {
            return HttpPolicyExtensions
                .HandleTransientHttpError()
                .WaitAndRetryAsync(retryCount: 3, sleepDurationProvider: retryAttempt => Duration.FromSeconds(Math.Pow(2, retryAttempt)).ToTimeSpan());
        }

        public static class Clients
        {
            public const string DEFAULT = "default";
        }
    }
}
