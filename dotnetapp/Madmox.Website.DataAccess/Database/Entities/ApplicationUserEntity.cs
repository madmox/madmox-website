﻿using Madmox.Website.DataAccess.Database.Infrastructure;
using NodaTime;
using System;

namespace Madmox.Website.DataAccess.Database.Entities
{
    [SqlTable("application_user")]
    public class ApplicationUserEntity
    {
        [SqlColumn("id")]
        public Guid Id { get; set; }

        [SqlColumn("email")]
        public string Email { get => this._email?.ToLowerInvariant(); set => this._email = value?.ToLowerInvariant(); }
        private string _email;

        [SqlColumn("is_password_clear_text")]
        public bool IsPasswordClearText { get; set; }

        [SqlColumn("password")]
        public string Password { get; set; }

        [SqlColumn("is_admin")]
        public bool IsAdmin { get; set; }

        [SqlColumn("last_name")]
        public string LastName { get; set; }

        [SqlColumn("first_name")]
        public string FirstName { get; set; }

        [SqlColumn("created_at")]
        public Instant CreatedAt { get => this._createdAt; set => this._createdAt = value.TruncateToMillisecond(); }
        private Instant _createdAt;

        [SqlColumn("updated_at")]
        public Instant UpdatedAt { get => this._updatedAt; set => this._updatedAt = value.TruncateToMillisecond(); }
        private Instant _updatedAt;

        public ApplicationUserEntity()
        {
        }
    }
}
