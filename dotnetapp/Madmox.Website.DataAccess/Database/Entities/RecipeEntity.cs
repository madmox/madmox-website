﻿using Madmox.Website.DataAccess.Database.Infrastructure;
using NodaTime;
using System;
using System.Collections.Generic;

namespace Madmox.Website.DataAccess.Database.Entities
{
    [SqlTable("recipe")]
    public class RecipeEntity
    {
        [SqlColumn("id")]
        public Guid Id { get; set; }

        [SqlColumn("category_id")]
        public Guid CategoryId { get; set; }

        [SqlColumn("name")]
        public string Name { get; set; }

        [SqlColumn("nb_persons")]
        public int NbPersons { get; set; }

        [SqlColumn("preparation_time")]
        public Duration PreparationTime { get; set; }

        [SqlColumn("total_time")]
        public Duration TotalTime { get; set; }

        [SqlColumn("ingredients")]
        public List<string> Ingredients { get; set; }

        [SqlColumn("tools")]
        public List<string> Tools { get; set; }

        [SqlColumn("steps")]
        public List<string> Steps { get; set; }

        [SqlColumn("created_at")]
        public Instant CreatedAt { get => this._createdAt; set => this._createdAt = value.TruncateToMillisecond(); }
        private Instant _createdAt;

        [SqlColumn("updated_at")]
        public Instant UpdatedAt { get => this._updatedAt; set => this._updatedAt = value.TruncateToMillisecond(); }
        private Instant _updatedAt;

        public RecipeEntity()
        {
        }
    }
}
