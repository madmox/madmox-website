﻿using Madmox.Website.DataAccess.Database.Infrastructure;
using NodaTime;
using System;

namespace Madmox.Website.DataAccess.Database.Entities
{
    [SqlTable("recipe_category")]
    public class RecipeCategoryEntity
    {
        [SqlColumn("id")]
        public Guid Id { get; set; }

        [SqlColumn("name")]
        public string Name { get; set; }

        [SqlColumn("position")]
        public int Position { get; set; }

        [SqlColumn("created_at")]
        public Instant CreatedAt { get => this._createdAt; set => this._createdAt = value.TruncateToMillisecond(); }
        private Instant _createdAt;

        [SqlColumn("updated_at")]
        public Instant UpdatedAt { get => this._updatedAt; set => this._updatedAt = value.TruncateToMillisecond(); }
        private Instant _updatedAt;

        public RecipeCategoryEntity()
        {
        }
    }
}
