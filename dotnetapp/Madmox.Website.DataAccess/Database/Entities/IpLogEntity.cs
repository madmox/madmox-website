﻿using Madmox.Website.Core.Types;
using Madmox.Website.DataAccess.Database.Infrastructure;
using NodaTime;
using System;

namespace Madmox.Website.DataAccess.Database.Entities
{
    [SqlTable("ip_log")]
    public class IpLogEntity
    {
        [SqlColumn("id")]
        public Guid Id { get; set; }

        [SqlColumn("ip_address")]
        public string IpAddress { get; set; }

        [SqlColumn("action")]
        public EnumWrapper<IpLogAction> Action { get; set; }

        [SqlColumn("context")]
        public string Context { get; set; }

        [SqlColumn("timestamp")]
        public Instant? Timestamp { get => this._timestamp; set => this._timestamp = value?.TruncateToMillisecond(); }
        private Instant? _timestamp;

        [SqlColumn("created_at")]
        public Instant CreatedAt { get => this._createdAt; set => this._createdAt = value.TruncateToMillisecond(); }
        private Instant _createdAt;

        [SqlColumn("updated_at")]
        public Instant UpdatedAt { get => this._updatedAt; set => this._updatedAt = value.TruncateToMillisecond(); }
        private Instant _updatedAt;

        public IpLogEntity()
        {
        }
    }
}
