﻿using Dapper;
using NodaTime;
using Npgsql;
using NpgsqlTypes;
using System.Data;

namespace Madmox.Website.DataAccess.Database.Handlers
{
    public class DateTimeZoneHandler : SqlMapper.TypeHandler<DateTimeZone>
    {
        public override DateTimeZone Parse(object value)
        {
            string typedValue = (string)value;
            return DateTimeZoneProviders.Tzdb.GetZoneOrNull(typedValue);
        }

        public override void SetValue(IDbDataParameter parameter, DateTimeZone value)
        {
            parameter.Value = value?.Id;
            ((NpgsqlParameter)parameter).NpgsqlDbType = NpgsqlDbType.Text;
        }
    }
}
