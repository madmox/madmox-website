﻿using Dapper;
using NodaTime;
using Npgsql;
using NpgsqlTypes;
using System.Data;

namespace Madmox.Website.DataAccess.Database.Handlers
{
    public class DurationHandler : SqlMapper.TypeHandler<Duration>
    {
        public override Duration Parse(object value)
        {
            var typedValue = (Period)value;
            return typedValue.ToDuration();
        }

        public override void SetValue(IDbDataParameter parameter, Duration value)
        {
            parameter.Value = value;
            ((NpgsqlParameter)parameter).NpgsqlDbType = NpgsqlDbType.Interval;
        }
    }
}
