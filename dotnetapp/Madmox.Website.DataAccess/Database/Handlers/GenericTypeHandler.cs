﻿using Dapper;
using Npgsql;
using NpgsqlTypes;
using System.Data;

namespace Madmox.Website.DataAccess.Database.Handlers
{
    public class GenericTypeHandler<T> : SqlMapper.TypeHandler<T>
    {
        private readonly NpgsqlDbType _dbType;

        public GenericTypeHandler(NpgsqlDbType dbType)
        {
            this._dbType = dbType;
        }

        public override T Parse(object value)
        {
            var typedValue = (T)value;
            return typedValue;
        }

        public override void SetValue(IDbDataParameter parameter, T value)
        {
            parameter.Value = value;
            ((NpgsqlParameter)parameter).NpgsqlDbType = this._dbType;
        }
    }
}
