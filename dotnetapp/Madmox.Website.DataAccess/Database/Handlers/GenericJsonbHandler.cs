﻿using Dapper;
using Madmox.Website.Core.Json;
using Npgsql;
using NpgsqlTypes;
using System.Data;
using System.Text.Json;

namespace Madmox.Website.DataAccess.Database.Handlers
{
    public class GenericJsonbHandler<T> : SqlMapper.TypeHandler<T>
    {
        public override T Parse(object value)
        {
            if (value is string json)
            {
                T typedValue = JsonSerializer.Deserialize<T>(json, JsonSettings.DEFAULT_SERIALIZER_OPTIONS);
                return typedValue;
            }
            else
            {
                return default;
            }
        }

        public override void SetValue(IDbDataParameter parameter, T value)
        {
            parameter.Value = JsonSerializer.Serialize(value, JsonSettings.DEFAULT_SERIALIZER_OPTIONS);
            ((NpgsqlParameter)parameter).NpgsqlDbType = NpgsqlDbType.Jsonb;
        }
    }
}
