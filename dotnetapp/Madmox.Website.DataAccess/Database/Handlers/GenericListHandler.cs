﻿using Dapper;
using Npgsql;
using NpgsqlTypes;
using System.Collections.Generic;
using System.Data;
using System.Linq;

namespace Madmox.Website.DataAccess.Database.Handlers
{
    public class GenericListHandler<T> : SqlMapper.TypeHandler<List<T>>
    {
        private readonly NpgsqlDbType _itemDbType;

        public GenericListHandler(NpgsqlDbType itemDbType)
        {
            this._itemDbType = itemDbType;
        }

        public override List<T> Parse(object value)
        {
            var typedValue = ((IEnumerable<T>)value).ToList();
            return typedValue;
        }

        public override void SetValue(IDbDataParameter parameter, List<T> value)
        {
            parameter.Value = value;
            ((NpgsqlParameter)parameter).NpgsqlDbType = NpgsqlDbType.Array | this._itemDbType;
        }
    }
}
