﻿using Dapper;
using Madmox.Website.Core.Helpers;
using Madmox.Website.Core.Types;
using Npgsql;
using NpgsqlTypes;
using System;
using System.Data;

namespace Madmox.Website.DataAccess.Database.Handlers
{
    public class EnumWrapperHandler<T> : SqlMapper.TypeHandler<EnumWrapper<T>> where T : Enum
    {
        public override EnumWrapper<T> Parse(object value)
        {
            string typedValue = (string)value;
            return EnumHelper.Parse<T>(typedValue);
        }

        public override void SetValue(IDbDataParameter parameter, EnumWrapper<T> value)
        {
            parameter.Value = value.ToString();
            ((NpgsqlParameter)parameter).NpgsqlDbType = NpgsqlDbType.Text;
        }
    }
}
