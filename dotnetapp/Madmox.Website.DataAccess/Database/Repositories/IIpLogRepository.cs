﻿using Madmox.Website.Core.Types;
using Madmox.Website.DataAccess.Database.Entities;
using NodaTime;
using System.Threading.Tasks;

namespace Madmox.Website.DataAccess.Database.Repositories
{
    public interface IIpLogRepository
    {
        // Read

        Task<int> CountByIpAddressSinceTimestamp(IpLogAction action, string ipAddress, Instant timestamp);

        // Write

        Task<int> Insert(IpLogEntity entity);
        Task<int> DeleteWhereOlderThanTimestamp(Instant timestamp);
    }
}
