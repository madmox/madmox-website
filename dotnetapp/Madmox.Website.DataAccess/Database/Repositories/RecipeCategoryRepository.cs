﻿using Madmox.Website.Core.Types;
using Madmox.Website.DataAccess.Database.Entities;
using Madmox.Website.DataAccess.Database.Infrastructure;
using NodaTime;
using System;
using System.Threading.Tasks;

namespace Madmox.Website.DataAccess.Database.Repositories
{
    public class RecipeCategoryRepository : IRecipeCategoryRepository
    {
        private readonly IDatabaseDriver _databaseDriver;

        public RecipeCategoryRepository(
            IDatabaseDriver databaseDriver
        )
        {
            this._databaseDriver = databaseDriver;
        }

        // Read

        async Task<RecipeCategoryEntity> IRecipeCategoryRepository.Get(Guid id)
        {
            var p = new
            {
                Id = id
            };
            string sql = $@"
SELECT
    {SqlHelper<RecipeCategoryEntity>.SelectedColumns}
FROM {SqlHelper<RecipeCategoryEntity>.Table}
WHERE
    {SqlHelper<RecipeCategoryEntity>.PrefixedColumn(x => x.Id)} = @{nameof(p.Id)}
;
";

            return await this._databaseDriver.QuerySingleOrDefaultAsync<RecipeCategoryEntity>(sql, p);
        }

        async Task<EntitySet<RecipeCategoryEntity>> IRecipeCategoryRepository.List()
        {
            var p = new
            {
            };
            string sql = $@"
SELECT
    {SqlHelper<RecipeCategoryEntity>.SelectedColumns}
FROM {SqlHelper<RecipeCategoryEntity>.Table}
;
";

            return await this._databaseDriver.QueryAsync<RecipeCategoryEntity>(sql, p);
        }

        // Write

        async Task<int> IRecipeCategoryRepository.Insert(RecipeCategoryEntity entity)
        {
            Instant now = SystemClock.Instance.GetCurrentInstant();
            entity.CreatedAt = now;
            entity.UpdatedAt = now;

            string sql = $@"
INSERT INTO {SqlHelper<RecipeCategoryEntity>.Table} (
    {SqlHelper<RecipeCategoryEntity>.Columns}
) VALUES (
    {SqlHelper<RecipeCategoryEntity>.Parameters}
)
;
";
            return await this._databaseDriver.ExecuteAsync(sql, entity);
        }

        async Task<int> IRecipeCategoryRepository.Update(RecipeCategoryEntity entity)
        {
            Instant now = SystemClock.Instance.GetCurrentInstant();
            entity.UpdatedAt = now;

            string sql = $@"
UPDATE {SqlHelper<RecipeCategoryEntity>.Table}
SET
    {SqlHelper<RecipeCategoryEntity>.Setters}
WHERE
    {SqlHelper<RecipeCategoryEntity>.PrefixedColumn(x => x.Id)} = @{nameof(entity.Id)}
;
";
            return await this._databaseDriver.ExecuteAsync(sql, entity);
        }

        async Task<int> IRecipeCategoryRepository.Delete(Guid id)
        {
            var p = new
            {
                Id = id
            };
            string sql = $@"
DELETE FROM {SqlHelper<RecipeCategoryEntity>.Table}
WHERE
    {SqlHelper<RecipeCategoryEntity>.PrefixedColumn(x => x.Id)} = @{nameof(p.Id)}
;
";
            return await this._databaseDriver.ExecuteAsync(sql, p);
        }
    }
}
