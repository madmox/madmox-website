﻿using Madmox.Website.Core.Types;
using Madmox.Website.DataAccess.Database.Entities;
using System;
using System.Threading.Tasks;

namespace Madmox.Website.DataAccess.Database.Repositories
{
    public interface IRecipeCategoryRepository
    {
        // Read

        Task<RecipeCategoryEntity> Get(Guid id);
        Task<EntitySet<RecipeCategoryEntity>> List();

        // Write

        Task<int> Insert(RecipeCategoryEntity entity);
        Task<int> Update(RecipeCategoryEntity entity);
        Task<int> Delete(Guid id);
    }
}
