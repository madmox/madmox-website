﻿using Madmox.Website.Core.Types;
using Madmox.Website.DataAccess.Database.Entities;
using Madmox.Website.DataAccess.Database.Infrastructure;
using NodaTime;
using System;
using System.Threading.Tasks;

namespace Madmox.Website.DataAccess.Database.Repositories
{
    public class RecipeRepository : IRecipeRepository
    {
        private readonly IDatabaseDriver _databaseDriver;

        public RecipeRepository(
            IDatabaseDriver databaseDriver
        )
        {
            this._databaseDriver = databaseDriver;
        }

        // Read

        async Task<RecipeEntity> IRecipeRepository.Get(Guid id)
        {
            var p = new
            {
                Id = id
            };
            string sql = $@"
SELECT
    {SqlHelper<RecipeEntity>.SelectedColumns}
FROM {SqlHelper<RecipeEntity>.Table}
WHERE
    {SqlHelper<RecipeEntity>.PrefixedColumn(x => x.Id)} = @{nameof(p.Id)}
;
";

            return await this._databaseDriver.QuerySingleOrDefaultAsync<RecipeEntity>(sql, p);
        }

        async Task<EntitySet<RecipeEntity>> IRecipeRepository.ListByCategoryId(Guid categoryId)
        {
            var p = new
            {
                CategoryId = categoryId
            };
            string sql = $@"
SELECT
    {SqlHelper<RecipeEntity>.SelectedColumns}
FROM {SqlHelper<RecipeEntity>.Table}
WHERE
    {SqlHelper<RecipeEntity>.PrefixedColumn(x => x.CategoryId)} = @{nameof(p.CategoryId)}
;
";

            return await this._databaseDriver.QueryAsync<RecipeEntity>(sql, p);
        }

        // Write

        async Task<int> IRecipeRepository.Insert(RecipeEntity entity)
        {
            Instant now = SystemClock.Instance.GetCurrentInstant();
            entity.CreatedAt = now;
            entity.UpdatedAt = now;

            string sql = $@"
INSERT INTO {SqlHelper<RecipeEntity>.Table} (
    {SqlHelper<RecipeEntity>.Columns}
) VALUES (
    {SqlHelper<RecipeEntity>.Parameters}
)
;
";
            return await this._databaseDriver.ExecuteAsync(sql, entity);
        }

        async Task<int> IRecipeRepository.Update(RecipeEntity entity)
        {
            Instant now = SystemClock.Instance.GetCurrentInstant();
            entity.UpdatedAt = now;

            string sql = $@"
UPDATE {SqlHelper<RecipeEntity>.Table}
SET
    {SqlHelper<RecipeEntity>.Setters}
WHERE
    {SqlHelper<RecipeEntity>.PrefixedColumn(x => x.Id)} = @{nameof(entity.Id)}
;
";
            return await this._databaseDriver.ExecuteAsync(sql, entity);
        }

        async Task<int> IRecipeRepository.Delete(Guid id)
        {
            var p = new
            {
                Id = id
            };
            string sql = $@"
DELETE FROM {SqlHelper<RecipeEntity>.Table}
WHERE
    {SqlHelper<RecipeEntity>.PrefixedColumn(x => x.Id)} = @{nameof(p.Id)}
;
";
            return await this._databaseDriver.ExecuteAsync(sql, p);
        }
    }
}
