﻿using Madmox.Website.Core.Types;
using Madmox.Website.DataAccess.Database.Entities;
using Madmox.Website.DataAccess.Database.Infrastructure;
using NodaTime;
using System;
using System.Threading.Tasks;

namespace Madmox.Website.DataAccess.Database.Repositories
{
    public class FailedSigninAttemptRepository : IIpLogRepository
    {
        private readonly IDatabaseDriver _databaseDriver;

        public FailedSigninAttemptRepository(
            IDatabaseDriver databaseDriver
        )
        {
            this._databaseDriver = databaseDriver;
        }

        // Read

        async Task<int> IIpLogRepository.CountByIpAddressSinceTimestamp(IpLogAction action, string ipAddress, Instant timestamp)
        {
            var p = new
            {
                Action = action.ToEnumString(),
                IpAddress = ipAddress,
                Timestamp = timestamp
            };
            string sql = $@"
SELECT COUNT({SqlHelper<IpLogEntity>.PrefixedColumn(x => x.Id)})
FROM {SqlHelper<IpLogEntity>.Table}
WHERE
    {SqlHelper<IpLogEntity>.PrefixedColumn(x => x.Action)} = @{nameof(p.Action)} AND
    {SqlHelper<IpLogEntity>.PrefixedColumn(x => x.IpAddress)} = @{nameof(p.IpAddress)} AND
    {SqlHelper<IpLogEntity>.PrefixedColumn(x => x.Timestamp)} >= @{nameof(p.Timestamp)}
;
";

            return await this._databaseDriver.QuerySingleAsync<int>(sql, p);
        }

        // Write

        async Task<int> IIpLogRepository.Insert(IpLogEntity entity)
        {
            Instant now = SystemClock.Instance.GetCurrentInstant();
            entity.CreatedAt = now;
            entity.UpdatedAt = now;

            string sql = $@"
INSERT INTO {SqlHelper<IpLogEntity>.Table} (
    {SqlHelper<IpLogEntity>.Columns}
) VALUES (
    {SqlHelper<IpLogEntity>.Parameters}
)
;
";
            return await this._databaseDriver.ExecuteAsync(sql, entity);
        }

        async Task<int> IIpLogRepository.DeleteWhereOlderThanTimestamp(Instant timestamp)
        {
            var p = new
            {
                Timestamp = timestamp
            };
            string sql = $@"
DELETE FROM {SqlHelper<IpLogEntity>.Table}
WHERE
    {SqlHelper<IpLogEntity>.PrefixedColumn(x => x.Timestamp)} < @{nameof(p.Timestamp)}
;
";
            return await this._databaseDriver.ExecuteAsync(sql, p);
        }
    }
}
