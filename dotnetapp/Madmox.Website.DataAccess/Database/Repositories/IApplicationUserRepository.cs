﻿using Madmox.Website.DataAccess.Database.Entities;
using System;
using System.Threading.Tasks;

namespace Madmox.Website.DataAccess.Database.Repositories
{
    public interface IApplicationUserRepository
    {
        // Read

        Task<ApplicationUserEntity> Get(Guid id);
        Task<ApplicationUserEntity> GetByEmail(string email);

        // Write

        Task<int> Insert(ApplicationUserEntity entity);
        Task<int> Update(ApplicationUserEntity entity);
        Task<int> Delete(Guid id);
    }
}
