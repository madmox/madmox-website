﻿using Madmox.Website.DataAccess.Database.Entities;
using Madmox.Website.DataAccess.Database.Infrastructure;
using NodaTime;
using System;
using System.Threading.Tasks;

namespace Madmox.Website.DataAccess.Database.Repositories
{
    public class ApplicationUserRepository : IApplicationUserRepository
    {
        private readonly IDatabaseDriver _databaseDriver;

        public ApplicationUserRepository(
            IDatabaseDriver databaseDriver
        )
        {
            this._databaseDriver = databaseDriver;
        }

        // Read

        async Task<ApplicationUserEntity> IApplicationUserRepository.Get(Guid id)
        {
            var p = new { Id = id };
            string sql = $@"
SELECT
    {SqlHelper<ApplicationUserEntity>.SelectedColumns}
FROM {SqlHelper<ApplicationUserEntity>.Table}
WHERE
    {SqlHelper<ApplicationUserEntity>.PrefixedColumn(x => x.Id)} = @{nameof(p.Id)}
;
";

            return await this._databaseDriver.QuerySingleOrDefaultAsync<ApplicationUserEntity>(sql, p);
        }

        async Task<ApplicationUserEntity> IApplicationUserRepository.GetByEmail(string email)
        {
            var p = new { Email = email.ToLowerInvariant() };
            string sql = $@"
SELECT
    {SqlHelper<ApplicationUserEntity>.SelectedColumns}
FROM {SqlHelper<ApplicationUserEntity>.Table}
WHERE
    {SqlHelper<ApplicationUserEntity>.PrefixedColumn(x => x.Email)} = @{nameof(p.Email)}
;
";

            return await this._databaseDriver.QuerySingleOrDefaultAsync<ApplicationUserEntity>(sql, p);
        }

        // Write

        async Task<int> IApplicationUserRepository.Insert(ApplicationUserEntity entity)
        {
            Instant now = SystemClock.Instance.GetCurrentInstant();
            entity.CreatedAt = now;
            entity.UpdatedAt = now;

            string sql = $@"
INSERT INTO {SqlHelper<ApplicationUserEntity>.Table} (
    {SqlHelper<ApplicationUserEntity>.Columns}
) VALUES (
    {SqlHelper<ApplicationUserEntity>.Parameters}
)
;
";
            return await this._databaseDriver.ExecuteAsync(sql, entity);
        }

        async Task<int> IApplicationUserRepository.Update(ApplicationUserEntity entity)
        {
            Instant now = SystemClock.Instance.GetCurrentInstant();
            entity.UpdatedAt = now;

            string sql = $@"
UPDATE {SqlHelper<ApplicationUserEntity>.Table}
SET
    {SqlHelper<ApplicationUserEntity>.Setters}
WHERE
    {SqlHelper<ApplicationUserEntity>.PrefixedColumn(x => x.Id)} = @{nameof(entity.Id)}
;
";
            return await this._databaseDriver.ExecuteAsync(sql, entity);
        }

        async Task<int> IApplicationUserRepository.Delete(Guid id)
        {
            var p = new
            {
                Id = id
            };
            string sql = $@"
DELETE FROM {SqlHelper<ApplicationUserEntity>.Table}
WHERE
    {SqlHelper<ApplicationUserEntity>.PrefixedColumn(x => x.Id)} = @{nameof(p.Id)}
;
";
            return await this._databaseDriver.ExecuteAsync(sql, p);
        }
    }
}
