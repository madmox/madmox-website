﻿using Madmox.Website.Core.Types;
using Madmox.Website.DataAccess.Database.Entities;
using System;
using System.Threading.Tasks;

namespace Madmox.Website.DataAccess.Database.Repositories
{
    public interface IRecipeRepository
    {
        // Read

        Task<RecipeEntity> Get(Guid id);
        Task<EntitySet<RecipeEntity>> ListByCategoryId(Guid categoryId);

        // Write

        Task<int> Insert(RecipeEntity entity);
        Task<int> Update(RecipeEntity entity);
        Task<int> Delete(Guid id);
    }
}
