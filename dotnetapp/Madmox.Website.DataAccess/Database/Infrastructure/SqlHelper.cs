﻿using System;
using System.Linq.Expressions;
using System.Reflection;
using System.Text;

namespace Madmox.Website.DataAccess.Database.Infrastructure
{
    public static class SqlHelper<TEntity>
    {
        public static string Table { get; private set; }
        public static string Columns { get; private set; }
        public static string SelectedColumns { get; private set; }
        public static string Parameters { get; private set; }
        public static string Setters { get; private set; }

        static SqlHelper()
        {
            Type t = typeof(TEntity);

            SqlTableAttribute sqlTableAtt = t.GetCustomAttribute<SqlTableAttribute>() ??
                throw new ArgumentException($"Could not find SqlTableAttribute on type {t.FullName}.");
            string tableName = sqlTableAtt.TableName;

            var columnsSb = new StringBuilder();
            var selectedColumnsSb = new StringBuilder();
            var parametersSb = new StringBuilder();
            var settersSb = new StringBuilder();
            bool first = true;
            PropertyInfo[] properties = typeof(TEntity).GetProperties(BindingFlags.Instance | BindingFlags.Public);
            foreach (PropertyInfo property in properties)
            {
                SqlColumnAttribute sqlColumnAtt = property.GetCustomAttribute<SqlColumnAttribute>();
                if (sqlColumnAtt != null)
                {
                    string columnName = sqlColumnAtt.ColumnName;
                    string parameterName = string.IsNullOrEmpty(sqlColumnAtt.SqlType) ?
                        $"@{property.Name}" :
                        $"@{property.Name}::{sqlColumnAtt.SqlType}";

                    if (!first)
                    {
                        columnsSb.Append(", ");
                        selectedColumnsSb.Append(", ");
                        parametersSb.Append(", ");
                        settersSb.Append(", ");
                    }
                    else
                    {
                        first = false;
                    }
                    columnsSb.Append(columnName);
                    selectedColumnsSb.Append($"{tableName}.{columnName} AS {property.Name}");
                    parametersSb.Append(parameterName);
                    settersSb.Append($"{columnName} = {parameterName}");
                }
            }

            Table = tableName;
            Columns = columnsSb.ToString();
            SelectedColumns = selectedColumnsSb.ToString();
            Parameters = parametersSb.ToString();
            Setters = settersSb.ToString();
        }

        public static string Column<TProperty>(Expression<Func<TEntity, TProperty>> propertySelector)
        {
            if (propertySelector.Body is not MemberExpression member)
            {
                throw new ArgumentException($"Expression '{propertySelector}' refers to a method, not a property.");
            }

            if (member.Member is not PropertyInfo property)
            {
                throw new ArgumentException($"Expression '{propertySelector}' refers to a field, not a property.");
            }

            SqlColumnAttribute sqlColumnAtt = property.GetCustomAttribute<SqlColumnAttribute>();
            if (sqlColumnAtt == null)
            {
                throw new ArgumentException($"Expression '{propertySelector}' does not refer to a property with SqlColumnAttribute.");
            }

            return sqlColumnAtt.ColumnName;
        }

        public static string PrefixedColumn<TProperty>(Expression<Func<TEntity, TProperty>> propertySelector)
        {
            if (propertySelector.Body is not MemberExpression member)
            {
                throw new ArgumentException($"Expression '{propertySelector}' refers to a method, not a property.");
            }

            if (member.Member is not PropertyInfo property)
            {
                throw new ArgumentException($"Expression '{propertySelector}' refers to a field, not a property.");
            }

            SqlColumnAttribute sqlColumnAtt = property.GetCustomAttribute<SqlColumnAttribute>();
            if (sqlColumnAtt == null)
            {
                throw new ArgumentException($"Expression '{propertySelector}' does not refer to a property with SqlColumnAttribute.");
            }

            return $"{Table}.{sqlColumnAtt.ColumnName}";
        }
    }
}
