﻿using System;

namespace Madmox.Website.DataAccess.Database.Infrastructure
{
    [AttributeUsage(AttributeTargets.Class, AllowMultiple = false)]
    public class SqlTableAttribute : Attribute
    {
        public string TableName { get; }

        public SqlTableAttribute(string tableName)
        {
            this.TableName = tableName;
        }
    }
}
