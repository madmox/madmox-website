﻿using System;

namespace Madmox.Website.DataAccess.Database.Infrastructure
{
    [AttributeUsage(AttributeTargets.Property, AllowMultiple = false)]
    public class SqlColumnAttribute : Attribute
    {
        public string ColumnName { get; }
        public string SqlType { get; set; }

        public SqlColumnAttribute(string columnName)
        {
            this.ColumnName = columnName;
        }
    }
}
