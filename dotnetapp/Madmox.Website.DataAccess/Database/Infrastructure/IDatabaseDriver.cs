﻿using Madmox.Website.Core.Types;
using System;
using System.Threading.Tasks;

namespace Madmox.Website.DataAccess.Database.Infrastructure
{
    public interface IDatabaseDriver
    {
        Task<T> QuerySingleAsync<T>(string sql, object param = null);
        Task<T> QuerySingleOrDefaultAsync<T>(string sql, object param = null);
        Task<EntitySet<T>> QueryAsync<T>(string sql, object param = null) where T : class;
        Task<EntitySet<TReturn>> QueryAsync<TFirst, TSecond, TReturn>(string sql, Func<TFirst, TSecond, TReturn> map, object param = null) where TReturn : class;
        Task<EntitySet<TReturn>> QueryAsync<TFirst, TSecond, TThird, TReturn>(string sql, Func<TFirst, TSecond, TThird, TReturn> map, object param = null) where TReturn : class;
        Task<EntitySet<TReturn>> QueryAsync<TFirst, TSecond, TThird, TFourth, TReturn>(string sql, Func<TFirst, TSecond, TThird, TFourth, TReturn> map, object param = null) where TReturn : class;
        Task<EntitySet<TReturn>> QueryAsync<TFirst, TSecond, TThird, TFourth, TFifth, TReturn>(string sql, Func<TFirst, TSecond, TThird, TFourth, TFifth, TReturn> map, object param = null) where TReturn : class;
        Task<int> ExecuteAsync(string sql, object param = null);
        Task Rollback();
        Task Commit();
    }
}
