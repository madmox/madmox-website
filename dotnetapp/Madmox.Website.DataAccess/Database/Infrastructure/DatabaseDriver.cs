﻿using Dapper;
using Madmox.Website.Core.Json;
using Madmox.Website.Core.Types;
using Microsoft.Extensions.Logging;
using Microsoft.Extensions.Options;
using Npgsql;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Common;
using System.Linq;
using System.Text.Json;
using System.Threading.Tasks;

namespace Madmox.Website.DataAccess.Database.Infrastructure
{
    public class DatabaseDriver : IDatabaseDriver
    {
        /// <summary>
        /// The Serializable isolation level emulates serial transaction execution for all committed transactions;
        /// as if transactions had been executed one after another, serially, rather than concurrently.
        /// 
        /// Like the Repeatable Read isolation level, it only sees data committed before the transaction began;
        /// it never sees either uncommitted data or changes committed during transaction execution by concurrent
        /// transactions.
        /// 
        /// See https://www.postgresql.org/docs/current/transaction-iso.html
        /// </summary>
        private const IsolationLevel TRANSACTION_ISOLATION_LEVEL = IsolationLevel.Serializable;

        private readonly string _connectionString;
        private DbConnection _connection;
        private DbTransaction _transaction;

        private readonly Dictionary<string, object> _cache = new Dictionary<string, object>();
        private readonly ILogger<DatabaseDriver> _logger;

        public DatabaseDriver(
            ILogger<DatabaseDriver> logger,
            IOptions<DatabaseConfiguration> options
        )
        {
            DatabaseConfiguration databaseConfig = options.Value;
            var builder = new NpgsqlConnectionStringBuilder()
            {
                Host = databaseConfig.Host,
                Port = databaseConfig.Port,
                Database = databaseConfig.Name,
                Username = databaseConfig.Username,
                Password = databaseConfig.Password,
                SslMode = SslMode.Disable
            };
            this._connectionString = builder.ConnectionString;
            this._logger = logger;
        }

        async Task<T> IDatabaseDriver.QuerySingleAsync<T>(string sql, object param)
        {
            this._logger.LogInformation("{sql}", sql);

            IDbTransaction transaction = await this.GetCurrentTransaction();
            return await this.QueryWithCache(
                sql: sql,
                param: param,
                factory: async () =>
                {
                    T result = await transaction.Connection.QuerySingleAsync<T>(sql, param, transaction);
                    return result;
                }
            );
        }

        async Task<T> IDatabaseDriver.QuerySingleOrDefaultAsync<T>(string sql, object param)
        {
            this._logger.LogInformation("{sql}", sql);

            IDbTransaction transaction = await this.GetCurrentTransaction();
            return await this.QueryWithCache(
                sql: sql,
                param: param,
                factory: async () =>
                {
                    T result = await transaction.Connection.QuerySingleOrDefaultAsync<T>(sql, param, transaction);
                    return result;
                }
            );
        }

        async Task<EntitySet<T>> IDatabaseDriver.QueryAsync<T>(string sql, object param)
        {
            this._logger.LogInformation("{sql}", sql);

            IDbTransaction transaction = await this.GetCurrentTransaction();
            return await this.QueryWithCache(
                sql: sql,
                param: param,
                factory: async () =>
                {
                    IEnumerable<T> result = await transaction.Connection.QueryAsync<T>(sql, param, transaction);
                    return result.ToEntitySet();
                }
            );
        }

        async Task<EntitySet<TReturn>> IDatabaseDriver.QueryAsync<TFirst, TSecond, TReturn>(string sql, Func<TFirst, TSecond, TReturn> map, object param)
        {
            this._logger.LogInformation("{sql}", sql);

            IDbTransaction transaction = await this.GetCurrentTransaction();
            return await this.QueryWithCache(
                sql: sql,
                param: param,
                factory: async () =>
                {
                    IEnumerable<TReturn> result = await transaction.Connection.QueryAsync(sql, map, param, transaction);
                    return result.Distinct().ToEntitySet();
                }
            );
        }

        async Task<EntitySet<TReturn>> IDatabaseDriver.QueryAsync<TFirst, TSecond, TThird, TReturn>(string sql, Func<TFirst, TSecond, TThird, TReturn> map, object param)
        {
            this._logger.LogInformation("{sql}", sql);

            IDbTransaction transaction = await this.GetCurrentTransaction();
            return await this.QueryWithCache(
                sql: sql,
                param: param,
                factory: async () =>
                {
                    IEnumerable<TReturn> result = await transaction.Connection.QueryAsync(sql, map, param, transaction);
                    return result.Distinct().ToEntitySet();
                }
            );
        }

        async Task<EntitySet<TReturn>> IDatabaseDriver.QueryAsync<TFirst, TSecond, TThird, TFourth, TReturn>(string sql, Func<TFirst, TSecond, TThird, TFourth, TReturn> map, object param)
        {
            this._logger.LogInformation("{sql}", sql);

            IDbTransaction transaction = await this.GetCurrentTransaction();
            return await this.QueryWithCache(
                sql: sql,
                param: param,
                factory: async () =>
                {
                    IEnumerable<TReturn> result = await transaction.Connection.QueryAsync(sql, map, param, transaction);
                    return result.Distinct().ToEntitySet();
                }
            );
        }

        async Task<EntitySet<TReturn>> IDatabaseDriver.QueryAsync<TFirst, TSecond, TThird, TFourth, TFifth, TReturn>(string sql, Func<TFirst, TSecond, TThird, TFourth, TFifth, TReturn> map, object param)
        {
            this._logger.LogInformation("{sql}", sql);

            IDbTransaction transaction = await this.GetCurrentTransaction();
            return await this.QueryWithCache(
                sql: sql,
                param: param,
                factory: async () =>
                {
                    IEnumerable<TReturn> result = await transaction.Connection.QueryAsync(sql, map, param, transaction);
                    return result.Distinct().ToEntitySet();
                }
            );
        }

        async Task<int> IDatabaseDriver.ExecuteAsync(string sql, object param)
        {
            this._logger.LogInformation("{sql}", sql);

            IDbTransaction transaction = await this.GetCurrentTransaction();
            return await transaction.Connection.ExecuteAsync(sql, param);
        }

        async Task IDatabaseDriver.Rollback()
        {
            try
            {
                if (this._transaction != null && this._connection?.State == ConnectionState.Open)
                {
                    this._logger.LogInformation("Rollback current SQL transaction.");
                    await this._transaction.RollbackAsync();
                }
            }
            finally
            {
                await this.CleanUp();
            }
        }

        async Task IDatabaseDriver.Commit()
        {
            try
            {
                if (this._transaction != null && this._connection?.State == ConnectionState.Open)
                {
                    this._logger.LogInformation("Commit current SQL transaction.");
                    await this._transaction.CommitAsync();
                }
            }
            finally
            {
                await this.CleanUp();
            }
        }

        private async Task<IDbTransaction> GetCurrentTransaction()
        {
            if (this._transaction == null)
            {
                this._connection = new NpgsqlConnection(this._connectionString);
                await this._connection.OpenAsync();
                this._transaction = this._connection.BeginTransaction(TRANSACTION_ISOLATION_LEVEL);
            }
            return this._transaction;
        }

        private async Task<T> QueryWithCache<T>(string sql, object param, Func<Task<T>> factory)
        {
            string cacheKey = ComputeCacheKey(sql, param);
            if (this._cache.ContainsKey(cacheKey))
            {
                this._logger.LogInformation($"SQL query is cached.");
                return (T)this._cache[cacheKey];
            }
            else
            {
                this._logger.LogInformation($"SQL query is not cached.");
                T value = await factory();
                this._cache[cacheKey] = value;
                return value;
            }
        }

        private static string ComputeCacheKey(string sql, object param)
        {
            object queryObject = new
            {
                Sql = sql,
                Parameters = param
            };
            string serializedQuery = JsonSerializer.Serialize(queryObject, JsonSettings.DEFAULT_SERIALIZER_OPTIONS);
            return serializedQuery.Sha256();
        }

        private async Task CleanUp()
        {
            if (this._transaction != null)
            {
                await this._transaction.DisposeAsync();
                this._transaction = null;
            }
            if (this._connection != null)
            {
                await this._connection.DisposeAsync();
                this._connection = null;
            }
            this._cache.Clear();
        }
    }
}
