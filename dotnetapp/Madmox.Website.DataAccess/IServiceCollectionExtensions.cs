﻿using Madmox.Website.Core.Types;
using Madmox.Website.DataAccess;
using Madmox.Website.DataAccess.Assets;
using Madmox.Website.DataAccess.Database;
using Madmox.Website.DataAccess.Database.Handlers;
using Madmox.Website.DataAccess.Database.Infrastructure;
using Madmox.Website.DataAccess.Database.Repositories;
using Microsoft.Extensions.Configuration;
using NodaTime;
using Npgsql;
using NpgsqlTypes;
using System;
using System.Collections.Generic;
using System.Text.Json;

namespace Microsoft.Extensions.DependencyInjection
{
    public static class IServiceCollectionExtensions
    {
        public static IServiceCollection SetupDataAccess(this IServiceCollection @this)
        {
            // ***
            // Setup Npgsql
            // ***
            // Note: Npgsql.NpgsqlConnection.GlobalTypeMapper.DefaultNameTranslator is Npgsql.NameTranslation.NpgsqlSnakeCaseNameTranslator

            // NodaTime types
            NpgsqlConnection.GlobalTypeMapper.UseNodaTime();

            // ***
            // Setup Dapper
            // ***

            // .NET builtins
            Dapper.SqlMapper.AddTypeHandler(new GenericListHandler<Guid>(NpgsqlDbType.Uuid));
            Dapper.SqlMapper.AddTypeHandler(new GenericListHandler<string>(NpgsqlDbType.Text));
            Dapper.SqlMapper.AddTypeHandler(new GenericJsonbHandler<JsonElement>());
            Dapper.SqlMapper.AddTypeHandler(new GenericJsonbHandler<Dictionary<string, string>>());

            // Custom types
            // Enums
            Dapper.SqlMapper.AddTypeHandler(new EnumWrapperHandler<IpLogAction>());

            // NodaTime types
            Dapper.SqlMapper.AddTypeHandler(new DateTimeZoneHandler());
            Dapper.SqlMapper.AddTypeHandler(new DurationHandler());
            Dapper.SqlMapper.AddTypeHandler(new GenericTypeHandler<Instant>(NpgsqlDbType.TimestampTz));
            Dapper.SqlMapper.AddTypeHandler(new GenericTypeHandler<Interval>(NpgsqlDbType.Range | NpgsqlDbType.TimestampTz));
            Dapper.SqlMapper.AddTypeHandler(new GenericTypeHandler<LocalDate>(NpgsqlDbType.Date));
            Dapper.SqlMapper.AddTypeHandler(new GenericTypeHandler<LocalDateTime>(NpgsqlDbType.Timestamp));
            Dapper.SqlMapper.AddTypeHandler(new GenericTypeHandler<LocalTime>(NpgsqlDbType.Time));
            Dapper.SqlMapper.AddTypeHandler(new GenericTypeHandler<Period>(NpgsqlDbType.Interval));

            return @this;
        }

        public static IServiceCollection AddApplicationDataAccessServices(this IServiceCollection @this, IConfiguration config)
        {
            // Configuration
            @this.Configure<AssetsConfiguration>(config.GetSection("Assets"));
            @this.Configure<DatabaseConfiguration>(config.GetSection("Database"));

            // .NET
            @this.AddHttpClient(HttpConstants.Clients.DEFAULT)
                .AddPolicyHandler(HttpConstants.GetDefaultRetryPolicy());

            // Assets
            @this.AddScoped<IAssetsDriver, AssetsDriver>();

            // Database
            @this.AddScoped<IDatabaseDriver, DatabaseDriver>();
            @this.AddScoped<IApplicationUserRepository, ApplicationUserRepository>();
            @this.AddScoped<IIpLogRepository, FailedSigninAttemptRepository>();
            @this.AddScoped<IRecipeCategoryRepository, RecipeCategoryRepository>();
            @this.AddScoped<IRecipeRepository, RecipeRepository>();

            return @this;
        }
    }
}
