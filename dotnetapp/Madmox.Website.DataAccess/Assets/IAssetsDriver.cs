﻿using System.IO;
using System.Threading.Tasks;

namespace Madmox.Website.DataAccess.Assets
{
    public interface IAssetsDriver
    {
        /// <summary>
        /// Uploads the specified asset.
        /// </summary>
        /// <param name="path">The asset's path, relative to the assets root directory.</param>
        /// <param name="content">The asset's content.</param>
        /// <returns>true if the asset was created, false if it was replaced.</returns>
        Task<bool> UploadAsset(string path, Stream content);

        /// <summary>
        /// Deletes the specified asset and any empty parent directory.
        /// </summary>
        /// <param name="path">The asset's path, relative to the assets root directory.</param>
        /// <returns>true if the asset was actually deleted.</returns>
        Task<bool> DeleteAsset(string path);
    }
}
