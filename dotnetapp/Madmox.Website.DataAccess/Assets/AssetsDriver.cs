﻿using Microsoft.Extensions.Options;
using System;
using System.IO;
using System.Threading.Tasks;

namespace Madmox.Website.DataAccess.Assets
{
    public class AssetsDriver : IAssetsDriver
    {
        private readonly AssetsConfiguration _assetsConfig;

        public AssetsDriver(
            IOptions<AssetsConfiguration> assetsOptions
        )
        {
            this._assetsConfig = assetsOptions.Value;
        }

        async Task<bool> IAssetsDriver.UploadAsset(string path, Stream content)
        {
            string assetsFolder = this.ValidateAssetsFolder();
            string assetFullPath = Path.GetFullPath(Path.Combine(assetsFolder, path));

            // Create missing intermediate directories
            string directoryName = Path.GetDirectoryName(assetFullPath);
            Directory.CreateDirectory(directoryName);

            // Create or replace file
            bool exists = File.Exists(assetFullPath);
            using FileStream fs = File.OpenWrite(assetFullPath);
            content.Seek(0, SeekOrigin.Begin);
            await content.CopyToAsync(fs);

            return !exists;
        }

        Task<bool> IAssetsDriver.DeleteAsset(string path)
        {
            string assetsFolder = this.ValidateAssetsFolder();
            string assetFullPath = Path.Join(assetsFolder, path);

            // Delete file if it exists
            bool exists = File.Exists(assetFullPath);
            if (exists)
            {
                File.Delete(assetFullPath);
            }

            // Purge all empty subfolders in the assets folder
            this.PurgeEmptySubFolders(assetsFolder);

            return Task.FromResult(exists);
        }

        private string ValidateAssetsFolder()
        {
            if (string.IsNullOrEmpty(this._assetsConfig.AssetsFolder))
            {
                throw new InvalidOperationException($"Assets folder is not defined.");
            }

            string assetsFolder = Path.GetFullPath(this._assetsConfig.AssetsFolder);
            if (!Directory.Exists(assetsFolder))
            {
                throw new InvalidOperationException($"Assets folder '{assetsFolder}' does not exist.");
            }

            return assetsFolder;
        }

        private void PurgeEmptySubFolders(string rootFolder)
        {
            foreach (string folder in Directory.GetDirectories(rootFolder))
            {
                this.PurgeEmptySubFolders(folder);
                if (Directory.GetFiles(folder).Length == 0 && Directory.GetDirectories(folder).Length == 0)
                {
                    Directory.Delete(folder, recursive: false);
                }
            }
        }
    }
}
