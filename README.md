# Madmox's website

This is the source code of my personal website. It contains various tools I developed in my free time to discover new technologies, some of them I use on a daily basis.

## Development

### Prerequisites

With these prerequisites, you will be able to run the application locally.

#### .NET Core

Install the [.NET 6.0 SDK](https://dotnet.microsoft.com/download/dotnet-core) (included with Visual Studio 2022 if you are using it).

#### PostgreSQL

Install [PostgreSQL](https://www.postgresql.org/download/) 13.x, and create 2 distinct databases from the pgAdmin query tool:

* one database for local development named `madmox` ;
* one database for the integration tests named `madmox_test`.

Initialize the local development database by executing the SQL scripts found in the `infrastructure/postgresql/initialization` folder from the pgAdmin query tool, in order. This step is not necessary for the test database.

#### Application settings

Make sure you set all the required application settings in the following projects:

* `Madmox.Website.Web`: create a file named `appsettings.local.json` in this folder. Use the `samples/appsettings.local.json` template and edit the values that need to be.

Don't edit the `appsettings.json` files directly, as these files are versioned. The settings in the new file will override any value present in `appsettings.json`.

### Build

Run the following command from the `dotnetapp` folder to build the solution:
```
dotnet build
```

Alternatively, open the solution in Visual Studio and build it.

### Run locally

To run the service locally with the Kestrel server, you just need to execute the script `scripts/run_kestrel.cmd` or `scripts/run_kestrel.sh` (depending on your OS). This is enough for development and testing scenarios. If you need a more production-ready form of hosting, see the [run in production](#running-in-production) section.

### Running unit tests

Run the following command from the `dotnetapp` folder to execute the unit tests:
```
dotnet test ".\Madmox.Website.UnitTests"
```

Alternatively, open the solution in Visual Studio and run the tests by right-clicking on the `Madmox.Website.UnitTests` project and selecting the `Run Tests` menu entry.

### Running integration tests

Run the following commands from the `dotnetapp` folder to execute the integration tests:
```
dotnet test ".\Madmox.Website.IntegrationTests"
```

Alternatively, open the solution in Visual Studio and run the tests by right-clicking on the test projects and selecting the `Run Tests` menu entry (or use the tests explorer).

## Running in production

This documentation currently only supports hosting the application on a [Debian "buster"](https://www.debian.org/releases/buster/) system. It assumes you have cloned the current repository on the production server and cd into it.

### Prerequisites

#### DNS

Make sure you have the DNS records `madmox.fr` and `www.madmox.fr` point to your web server's IP.

#### .NET Core

From [this guide](https://docs.microsoft.com/en-us/dotnet/core/install/linux-package-manager-debian10).

Add Microsoft repository key and feed:
```bash
wget -O - https://packages.microsoft.com/keys/microsoft.asc | gpg --dearmor > microsoft.asc.gpg
sudo mv microsoft.asc.gpg /etc/apt/trusted.gpg.d/
sudo chown root:root /etc/apt/trusted.gpg.d/microsoft.asc.gpg
sudo chmod 644 /etc/apt/trusted.gpg.d/microsoft.asc.gpg

wget https://packages.microsoft.com/config/debian/10/prod.list
sudo mv prod.list /etc/apt/sources.list.d/microsoft-prod.list
sudo chown root:root /etc/apt/sources.list.d/microsoft-prod.list
sudo chmod 644 /etc/apt/sources.list.d/microsoft-prod.list
```

Install the ASP.NET Core runtime:
```bash
sudo apt update
sudo apt install aspnetcore-runtime-6.0
```

If you plan on compiling the application on the production server, install the dotnet Core SDK instead of the ASP.NET Core runtime:
```bash
sudo apt update
sudo apt-get install dotnet-sdk-6.0
```

#### Install nginx

Install nginx:
```bash
sudo apt install nginx
```

Check that the service is started, or start it:
```bash
sudo service nginx status
sudo service nginx start
```

#### Let's Encrypt

From [this guide](https://www.nginx.com/blog/using-free-ssltls-certificates-from-lets-encrypt-with-nginx/).

Install Let's Encrypt certbot's nginx plugin:
```bash
sudo apt install python-certbot-nginx
```

Use certbot to generate new HTTPS certificates for the website's domains:
```bash
sudo certbot certonly --nginx -d madmox.fr -d www.madmox.fr
```
Enter a valid email address, and answer the other questions when prompted to do so.

Add a cron job to renew Let's Encypt certificates automatically within 30 days of expiration:
```bash
sudo crontab -e
```
The CRON job should look like this:
```
0 12 * * * /usr/bin/certbot renew --quiet
```

#### Configure nginx

Generate a self-signed certificate to handle SSL connections on invalid hosts:
```bash
sudo mkdir /etc/nginx/ssl
sudo openssl req -x509 -nodes -days 7300 -newkey rsa:2048 -keyout /etc/nginx/ssl/nginx.key -out /etc/nginx/ssl/nginx.crt
```

Copy the configuration files for nginx (or update your existing configuration file, if you already have sites running):
```bash
sudo cp ./infrastructure/nginx/conf.d/server.conf /etc/nginx/conf.d/server.conf
sudo chown root:root /etc/nginx/conf.d/server.conf
sudo chmod 644 /etc/nginx/conf.d/server.conf

sudo cp ./infrastructure/nginx/sites-available/default /etc/nginx/sites-available/default
sudo chown root:root /etc/nginx/sites-available/default
sudo chmod 644 /etc/nginx/sites-available/default
```

Check your nginx configuration and reload it:
```bash
sudo nginx -t
sudo service nginx reload
```

Check that your web server correctly handles HTTPS connections by opening https://www.madmox.fr/ in a web browser. It should display a 502 error code page (not a SSL connection error). It should also redirect both http://madmox.fr/ and http://www.madmox.fr/ to https://www.madmox.fr/.

#### PostgreSQL

Install `postgresql-13` by following the [documentation](https://www.postgresql.org/download/), and create a user named `madmox` and a database also named `madmox` in the cluster you are going to use:
```bash
sudo -u postgres pg_ctlcluster 13 main start
sudo -u postgres createuser --pwprompt madmox
sudo -u postgres createdb --owner=madmox madmox
```

If you wish, you can configure the server for remote access. This involves editing the files `postgresql.conf` (to listen on the server's public IP address) and `pg_hba.conf` (to allow access from the remote IP address). Note that securing the database is outside the scope of this documentation, but should be done properly if you are planning to run the website in production.

Then run all the scripts contained in the `infrastructure/sql/initialization` folder to initialize the database, either by using the remote client of your choice, or by running the following command locally:
```bash
psql -a -d madmox -f infrastructure/sql/initialization/<script name>
```

#### Kestrel application

Create the files and directories that will be used to host the dotnet application:
```bash
sudo mkdir /var/www/madmox-website
sudo chown www-data:www-data /var/www/madmox-website
sudo chmod 770 /var/www/madmox-website

sudo mkdir /var/www/madmox-website-appdata
sudo cp -r ./infrastructure/appdata/* /var/www/madmox-website-appdata/
sudo chown -R www-data:www-data /var/www/madmox-website-appdata
sudo chmod 770 /var/www/madmox-website-appdata

sudo mkdir /var/www/madmox-website-assets
sudo chown www-data:www-data /var/www/madmox-website-assets
sudo chmod 770 /var/www/madmox-website-assets
```

Create a new certificate for data protection:
```bash
sudo openssl req -x509 -newkey rsa:2048 -keyout /var/www/madmox-website-appdata/dpapi.key -out /var/www/madmox-website-appdata/dpapi.crt -days 36500
sudo openssl pkcs12 -export -in /var/www/madmox-website-appdata/dpapi.crt -inkey /var/www/madmox-website-appdata/dpapi.key -out /var/www/madmox-website-appdata/dpapi.pfx
# You will need the PFX password in the next step, so don't lose it
sudo rm /var/www/madmox-website-appdata/dpapi.crt
sudo rm /var/www/madmox-website-appdata/dpapi.key
sudo chown www-data:www-data /var/www/madmox-website-appdata/dpapi.pfx
```

Edit the environment file and fill-in your environment specific configuration (database connection string, etc.):
```bash
sudo nano /var/www/madmox-website-appdata/environment
```

nginx does not manage the Kestrel process, so we use systemd to start the service automatically:
```bash
sudo cp ./infrastructure/systemd/kestrel-madmox-website.service /etc/systemd/system/kestrel-madmox-website.service
sudo chown root:root /etc/systemd/system/kestrel-madmox-website.service
sudo chmod 644 /etc/systemd/system/kestrel-madmox-website.service
sudo systemctl enable kestrel-madmox-website.service
```

### Publish the application

#### If the production server has .NET Core SDK installed
You just need to execute the following script:
```bash
sudo bash ./deployment/deploy.sh
```

#### If the production server does not have .NET Core SDK installed
Create the publish package from a machine with the .NET Core SDK installed:
```bash
# Run this command from the build machine!
dotnet publish ./dotnetapp/Madmox.Website.Web --configuration "Release" --output ./deployment/publish
```

Stop the Kestrel service:
```bash
sudo service kestrel-madmox-website stop
```

Upload the package manually to `/var/www/madmox-website` (remove all existing content).

Start the Kestrel service:
```bash
sudo service kestrel-madmox-website start
```

#### Check the application is running

Check the service has been correctly started:
```bash
sudo service kestrel-madmox-website status
```

If an error occurs, you can check the systemd logs:
```bash
sudo journalctl -fu kestrel-madmox-website.service
```

Check that the website is accessible at https://www.madmox.fr/. The 502 error page should be replaced by a working version of the website. Hooray!

## CI/CD

This project uses BitBucket's CI/CD tool, [BitBucket Pipelines](https://bitbucket.org/product/features/pipelines).

### Continuous Integration

Any commit pushed on the `develop` branch is automatically submitted to the CI pipeline. Build, unit tests and integration tests are run on BitBucket.

### Continuous Deployment
Continuous deployment is not enabled for this project.
