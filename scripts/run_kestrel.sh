#!/bin/bash
SCRIPT_ROOT=$(dirname "$(realpath -s "${BASH_SOURCE[0]}")")
cd "$SCRIPT_ROOT/../dotnetapp"

dotnet run --configuration "Debug" --project "Madmox.Website.Web" --launch-profile "Kestrel"
