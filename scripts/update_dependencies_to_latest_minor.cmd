@ECHO OFF
SET SCRIPT_ROOT=%~dp0
cd "%SCRIPT_ROOT%..\dotnetapp"

REM Install this tool with the following command:
REM dotnet tool install --global dotnet-outdated-tool
dotnet outdated --version-lock major --upgrade
pause
