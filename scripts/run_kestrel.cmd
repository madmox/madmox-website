@ECHO OFF
SET SCRIPT_ROOT=%~dp0
cd "%SCRIPT_ROOT%..\dotnetapp"

dotnet run --configuration "Debug" --project "Madmox.Website.Web" --launch-profile "Kestrel"
